//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 09 Apr 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package structs

import (
	"time"
)

///////////////////////////////////////////////////////////////////////////////
// SLA structs used to create / receive SLAs (violations, penalties etc.) from
// the SLA Frameqork tool:

// VIOLATIONS
// ViolationInfo2
type ViolationInfo2 struct {
	Agreement_id string
	Status       string
	Client_id    string
	Client_name  string
	Guarantee    string
}

/*
ViolationInfo ...
*/
type ViolationInfo struct {
	AgreementID 	string
	Status      	string
	ClientID    	string
	ClientName  	string
	Guarantee   	string
	// NEW
	AppID 			string     `json:"appId,omitempty"`		
	ViolationName 	string     `json:"violationName,omitempty"`
	ViolationType 	string     `json:"violationType,omitempty"`
}

/*
SLADetailsGuarantee ...
*/
type SLADetailsGuarantee struct {
	Name       string       	   `json:"name"`
	Constraint string       	   `json:"constraint"`
	Penalties  []SLAPenaltyDef 	   `json:"penalties,omitempty"`
	Importance []SLAGuaranteeType  `json:"importance"`   		
	Actions    []SLAActionsDef 	   `json:"actions,omitempty"`	
}

// For tracking reasons, we add the type of violation
// to define "mild", "serious", "catastrophic", etc. violations
type SLAGuaranteeType struct {
	Name       string   `json:"name"`
	Constraint string   `json:"constraint"`
}

// SLAPenaltyDef is the struct that represents a penalty in case of an SLO violation
type SLAPenaltyDef struct {
	Type  string `json:"type"`
	Value string `json:"value"`
	Unit  string `json:"unit"`
}

// SLAActionsDef is the struct that represents an action in case of an SLO violation
type SLAActionsDef struct {
	Type  string `json:"type"`
	Value string `json:"value"`
	Unit  string `json:"unit"`
}

/*
SLADetailsSt ...
*/
type SLADetailsSt struct {
	ID   string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

/*
SLADetails ...
*/
type SLADetails struct {
	ID         string                `json:"id,omitempty"`
	Type       string                `json:"type,omitempty"`
	Name       string                `json:"name,omitempty"`
	Provider   SLADetailsSt          `json:"provider,omitempty"`
	Client     SLADetailsSt          `json:"client,omitempty"`
	Creation   time.Time             `json:"creation,omitempty"`
	Expiration time.Time             `json:"expiration,omitempty"`
	Guarantees []SLADetailsGuarantee `json:"guarantees,omitempty"`
}

/*
SLA ...
*/
type SLA struct {
	ID       		string     `json:"id,omitempty"`
	Name     		string     `json:"name,omitempty"`
	Description 	string     `json:"description,omitempty"`	// NEW
	AppID 			string     `json:"appId,omitempty"`			// NEW
	State    		string     `json:"state,omitempty"`
	Location 		string     `json:"location,omitempty"`
	Details  		SLADetails `json:"details,omitempty"`
}