//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 09 Apr 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package structs

///////////////////////////////////////////////////////////////////////////////
// IME: Orchestrator / cluster definitions

/*
E2cOrchestrator represents an orchestrator and all the  host / edge device / cluster access point information
*/
type E2cOrchestrator struct {
	ID               	string 	`json:"id,omitempty"`
	Name             	string 	`json:"name,omitempty"`
	Description      	string 	`json:"description,omitempty"`
	Type            	string `json:"type,omitempty"`             // EDGE: 'microk8s', 'k3s'; CLUSTER: 'openshift', 'kubernetes'
	SO               	string `json:"so,omitempty"`               // S.O.: 'ubuntu18', 'ubuntu16', 'centos'
	DefaultNamespace 	string `json:"defaultnamespace,omitempty"` // 'class', 'default'
	IP               	string `json:"ip,omitempty"`
	Port             	int    `json:"hostPort,omitempty"`
	User             	string `json:"user,omitempty"`
	Password         	string `json:"password,omitempty"`
	KeyFile          	string `json:"keyFile,omitempty"`
	ConnectionToken  	string `json:"connectionToken,omitempty"`
	RESTAPIEndPoint  	string `json:"restAPIEndPoint,omitempty"`
	Location         	string `json:"location,omitempty"`
	// secondary tools
	SLALiteEndPoint               string `json:"slaLiteEndPoint,omitempty"`
	PrometheusPushgatewayEndPoint string `json:"prometheusPushgatewayEndPoint,omitempty"`
	PrometheusEndPoint            string `json:"prometheusEndPoint,omitempty"`
	// Client Certificates
	CACertData     	string `json:"caCertData,omitempty"` // DER with base64 encode (PEM without line break and headers)
	ClientCertData 	string `json:"clientCertData,omitempty"`
	KeyCertData    	string `json:"keyCertData,omitempty"`
	CACertFile     	string `json:"caCertFile,omitempty"` // PEM file path
	ClientCertFile 	string `json:"clientCertFile,omitempty"`
	KeyCertFile    	string `json:"keyCertFile,omitempty"`
}

/*
E2cOrchestrator represents an orchestrator and all the applications deployed on it.
*/
type E2cOrchestratorApps struct {
	ID                            string             `json:"id,omitempty"`
	Name                          string             `json:"name,omitempty"`
	Description                   string             `json:"description,omitempty"`
	Type                          string             `json:"type,omitempty"`             // EDGE: 'microk8s', 'k3s'; CLUSTER: 'openshift', 'kubernetes'
	SO                            string             `json:"so,omitempty"`               // S.O.: 'ubuntu18', 'ubuntu16', 'centos'
	DefaultNamespace              string             `json:"defaultnamespace,omitempty"` // 'class', 'default'
	IP                            string             `json:"ip,omitempty"`
	Port                          int                `json:"hostPort,omitempty"`
	User                          string             `json:"user,omitempty"`
	Password                      string             `json:"password,omitempty"`
	KeyFile                       string             `json:"keyFile,omitempty"`
	ConnectionToken               string             `json:"connectionToken,omitempty"`
	RESTAPIEndPoint               string             `json:"restAPIEndPoint,omitempty"`
	SLALiteEndPoint               string             `json:"slaLiteEndPoint,omitempty"`
	PrometheusPushgatewayEndPoint string             `json:"prometheusPushgatewayEndPoint,omitempty"`
	PrometheusEndPoint            string             `json:"prometheusEndPoint,omitempty"`
	Apps                          []E2coSubApp 		 `json:"apps,omitempty"`
}

type E2coSoeObject struct {
	IDE2cOrchestrator 	string  	`json:"idE2cOrchestrator,omitempty"`
	UserID 				string 		`json:"user_id,omitempty"`
	ComputeID 			string  	`json:"compute_id,omitempty"`
	Name 				string  	`json:"name,omitempty"`
	Description 			string  	`json:"description,omitempty"`
	ChunkIDs  			[]string  	`json:"chunk_ids,omitempty"`
	Slic3ID				string  	`json:"slic3Id,omitempty"`
	Requirements  	struct {
		Ram  	struct {
			Required 	int `json:"required,omitempty"`
			Limits  	int `json:"limits,omitempty"`
			Units  		string `json:"units,omitempty"`
		} `json:"ram,omitempty"`
		Cpus 	struct {
			Required 	int `json:"required,omitempty"`
			Limits  	int `json:"limits,omitempty"`
			Units  		string `json:"units,omitempty"`
		} `json:"cpus,omitempty"`
		Storage     struct {
			Required 	int `json:"required,omitempty"`
			Limits  	int `json:"limits,omitempty"`
			Units  		string `json:"units,omitempty"`
		} `json:"storage,omitempty"`
	} `json:"requirements,omitempty"`
}
