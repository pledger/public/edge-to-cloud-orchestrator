//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 16 Sep 2021
// Updated on 16 Sep 2021
//
// @author: ATOS
//
package parsers

import (
	"errors"
	"strings"
	"bytes"
	"net/http"
	"io"
	"encoding/json"

	"atos.pledger/e2c-orchestrator/common"
	"atos.pledger/e2c-orchestrator/common/globals"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"

	"github.com/gin-gonic/gin"
)

/*
ParseE2coApplicationStructV2 parses incomming JSON to an E2coTaskExtended struct
*/
func ParseE2coApplicationStructV2(c *gin.Context) (structs.E2coApplication, error) {
	log.Trace(pathLOG + "[ParseE2coApplicationStructV2] Parsing body content (json definition) to [E2coApplication] ...")

	var e2coApplication structs.E2coApplication

	err := c.ShouldBindJSON(&e2coApplication) // E2coApplication?
	if err != nil {
		return structs.E2coApplication{}, errors.New("JSON (body) is not a E2coApplication object: " + err.Error())
	}

	// check JSON fields
	if e2coApplication.Apps == nil {
		return structs.E2coApplication{}, errors.New("JSON is not a E2coApplication object: No application or function was defined")
	}

	// set unset values to default
	e2coApplication.ID = strings.ToLower(e2coApplication.ID) 	// id to lower case
	SetDefaultE2coApplicationValues(&e2coApplication)

	strApp, _ := common.StructToString(e2coApplication)
	log.Debug(pathLOG + "[parseE2coApplicationStructV2] e2coApplication: " + strApp)

	return e2coApplication, nil
}

/*
parseE2cOrchestratorStructV2 parses incomming JSON to an E2cOrchestrator struct
*/
func ParseE2cOrchestratorStructV2(c *gin.Context) (structs.E2cOrchestrator, error) {
	log.Trace(pathLOG + "[ParseE2cOrchestratorStructV2] Parsing body content (json definition) to [E2cOrchestrator] ...")
	
	var e2coOrchestrator structs.E2cOrchestrator

	err := c.ShouldBindJSON(&e2coOrchestrator) // E2cOrchestrator?
	if err != nil {
		return structs.E2cOrchestrator{}, errors.New("JSON (body) is not a E2cOrchestrator object: " + err.Error())
	}

	// format values (lowercase, urls ...):
	// ID
	e2coOrchestrator.ID = strings.ToLower(e2coOrchestrator.ID)
	// TYPE
	e2coOrchestrator.Type = strings.ToLower(e2coOrchestrator.Type)
	if e2coOrchestrator.Type == "k8s" {
		e2coOrchestrator.Type = globals.KUBERNETES
	}
	// Endpoints
	e2coOrchestrator.RESTAPIEndPoint, err = parseURL(e2coOrchestrator.RESTAPIEndPoint)
	if err != nil {
		return structs.E2cOrchestrator{}, errors.New("RESTAPIEndPoint value is not correct: " + err.Error())
	}	

	strApp, _ := common.StructToString(e2coOrchestrator)
	log.Debug(pathLOG + "[ParseE2cOrchestratorStructV2] e2coOrchestrator: " + strApp)
	
	return e2coOrchestrator, nil
}

/*
ParseViolationInfoStructV2 parses incomming JSON to an ViolationInfo struct
*/
func ParseViolationInfoStructV2(c *gin.Context) (structs.ViolationInfo2, error) {
	log.Trace(pathLOG + "[ParseViolationInfoStructV2] Parsing default json definition [ViolationInfo] ...")

	var vinfo structs.ViolationInfo2

	err := c.ShouldBindJSON(&vinfo) // ViolationInfo2?
	if err != nil {
		return structs.ViolationInfo2{}, errors.New("JSON (body) is not a ViolationInfo object: " + err.Error())
	}

	strApp, _ := common.StructToString(vinfo)
	log.Debug(pathLOG + "[ParseViolationInfoStructV2] vinfo: " + strApp)

	return vinfo, nil
}

/*
ParseTaskOperationStructV2 parses incomming JSON to an TaskOperation struct
*/
func ParseTaskOperationStructV2(c *gin.Context) (structs.TaskOperation, error) {
	log.Trace(pathLOG + "[ParseTaskOperationStructV2] Parsing default json definition [TaskOperation] ...")

	var taskOperation structs.TaskOperation

	err := c.ShouldBindJSON(&taskOperation) // TaskOperation?
	if err != nil {
		return structs.TaskOperation{}, errors.New("JSON (body) is not a TaskOperation object: " + err.Error())
	}

	strApp, _ := common.StructToString(taskOperation)
	log.Debug(pathLOG + "[ParseTaskOperationStructV2] taskOperation: " + strApp)

	return taskOperation, nil
}

// DEPRECATED: used by API v1

/*
ParseE2coApplicationStruct parses incomming JSON to an E2coTaskExtended struct
*/
func ParseE2coApplicationStruct(r *http.Request) (structs.E2coApplication, error) {
	log.Trace(pathLOG + "[ParseE2coApplicationStruct] 'Duplicating' r.Body to use it multiple times with decoders ...")
	rBody := r.Body
	var buf bytes.Buffer
	tee := io.TeeReader(rBody, &buf)

	log.Debug(pathLOG + "[ParseE2coApplicationStruct] Parsing default json definition [E2coApplication] ...")
	decoder := json.NewDecoder(tee)
	decoder.DisallowUnknownFields()

	// E2coApplication?
	e2coApplication, err := getE2coApplication(decoder)
	if err != nil {
		return structs.E2coApplication{}, errors.New("JSON is not a E2coApplication object: " + err.Error())
	}

	// check JSON fields
	if e2coApplication.Apps == nil {
		return structs.E2coApplication{}, errors.New("JSON is not a E2coApplication object: No application or function was defined")
	}

	// set unset values to default
	SetDefaultE2coApplicationValues(e2coApplication)

	strApp, _ := common.StructToString(*e2coApplication)
	log.Debug(pathLOG + "[ParseE2coApplicationStruct] e2coApplication: " + strApp)

	return *e2coApplication, nil
}

/*
ParseE2cOrchestratorStruct parses incomming JSON to an E2coTaskExtended struct
*/
func ParseE2cOrchestratorStruct(r *http.Request) (*structs.E2cOrchestrator, error) {
	log.Trace(pathLOG + "[ParseE2cOrchestratorStruct] Parsing default json definition [E2cOrchestrator] ...")
	decoder := json.NewDecoder(r.Body)

	// JSON is an E2cOrchestrator struct?
	e2coOrchestrator, err := getE2cOrchestrator(decoder)
	if err != nil {
		return nil, errors.New("JSON is not a E2cOrchestrator object")
	}

	// format values (lowercase, urls ...):
	
	// ID
	e2coOrchestrator.ID = strings.ToLower(e2coOrchestrator.ID)

	// TYPE
	e2coOrchestrator.Type = strings.ToLower(e2coOrchestrator.Type)
	if e2coOrchestrator.Type == "k8s" {
		e2coOrchestrator.Type = globals.KUBERNETES
	}

	// Endpoints
	e2coOrchestrator.RESTAPIEndPoint, err = parseURL(e2coOrchestrator.RESTAPIEndPoint)
	if err != nil {
		return nil, errors.New("RESTAPIEndPoint value is not correct: " + err.Error())
	}	
	
	return e2coOrchestrator, nil
}

/*
ParseTaskOperationStruct parses incomming JSON to an TaskOperation struct
*/
func ParseTaskOperationStruct(r *http.Request) (*structs.TaskOperation, error) {
	log.Trace(pathLOG + "[ParseTaskOperationStruct] Parsing default json definition [TaskOperation] ...")
	decoder := json.NewDecoder(r.Body)

	// TaskOperation?
	taskOperation, err := getTaskOperation(decoder)
	if err != nil {
		return nil, errors.New("JSON is not a TaskOperation object: " + err.Error())
	}

	return taskOperation, nil
}