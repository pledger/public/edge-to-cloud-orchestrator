//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 26 Mar 2021
// Updated on 26 Mar 2021
//
// @author: ATOS
//
package parsers

import (
	"encoding/json"
	"errors"
	"strconv"
	"strings"
	"time"
	"net/url"

	"atos.pledger/e2c-orchestrator/common"
	"atos.pledger/e2c-orchestrator/common/cfg"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
	"atos.pledger/e2c-orchestrator/common/globals"

	"github.com/lithammer/shortuuid"
)

// path used in logs
const pathLOG string = "E2CO > Data > Parsers "

/*
getE2cOrchestrator Checks if E2cOrchestrator struct is valid (from json)
*/
func getE2cOrchestrator(decoder *json.Decoder) (*structs.E2cOrchestrator, error) {
	log.Trace(pathLOG + "[getE2cOrchestrator] Checking json object ...")

	var t structs.E2cOrchestrator
	err := decoder.Decode(&t)
	if err != nil {
		return nil, errors.New("Error decoding object: " + err.Error())
	}

	tStr, err := common.StructToString(t)
	log.Trace(pathLOG + "[getE2cOrchestrator] Parsed object (string): " + tStr)
	log.Trace(pathLOG + "[getE2cOrchestrator] Sending parsed object ...")

	// id to lower case
	t.ID = strings.ToLower(t.ID) 

	return &t, err
}

/*
getE2coApplication Checks if E2coApplication struct is valid (from json)
*/
func getE2coApplication(decoder *json.Decoder) (*structs.E2coApplication, error) {
	log.Trace(pathLOG + "[getE2coApplication] Checking json object ...")

	var t structs.E2coApplication
	err := decoder.Decode(&t)
	if err != nil {
		return nil, errors.New("Error decoding object: " + err.Error())
	}

	tStr, err := common.StructToString(t)
	log.Trace(pathLOG + "[getE2coApplication] Parsed object (string): " + tStr)
	log.Trace(pathLOG + "[getE2coApplication] Sending parsed object ...")

	// id to lower case
	t.ID = strings.ToLower(t.ID) 

	return &t, err
}

/*
getTaskOperation Checks if TaskOperation struct is valid (from json)
*/
func getTaskOperation(decoder *json.Decoder) (*structs.TaskOperation, error) {
	log.Trace(pathLOG + "[getTaskOperation] Checking json object ...")

	var t structs.TaskOperation
	err := decoder.Decode(&t)
	if err != nil {
		return nil, errors.New("Error decoding object: " + err.Error())
	}

	tStr, err := common.StructToString(t)
	log.Trace(pathLOG + "[getTaskOperation] Parsed object (string): " + tStr)
	log.Trace(pathLOG + "[getTaskOperation] Sending parsed object ...")

	return &t, err
}

/*
generateID generates an Identifier based on a random string and the current time in nanoseconds
*/
func generateID() string {
	// 1. ID generation
	id := shortuuid.NewWithAlphabet("0123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwx")
	id = strings.ToLower(id) // Kubernetes doesnt allow uppercase names for deployments

	// 2. time in nanoseconds is appended to ID
	now := time.Now()      // current local time
	nsec := now.UnixNano() // number of nanoseconds since January 1, 1970 UTC
	id = id + strconv.FormatInt(nsec, 10)

	return id
}

/*
Returns the default orchestrator: [0] or "maincluster"
*/
func getDefaultOrchestrator() (structs.E2cOrchestrator, error) {
	for _, o := range cfg.Config.Orchestrators {
		if strings.ToLower(o.ID) == "maincluster" {
			return o.E2cOrchestrator, nil
		}
	}

	if len(cfg.Config.Orchestrators) > 0 {
		return cfg.Config.Orchestrators[0].E2cOrchestrator, nil
	}

	return structs.E2cOrchestrator{}, errors.New("No orchestrators found")
}

// remove the very last character of a string
func trimSuffix(s, suffix string) string {
    if strings.HasSuffix(s, suffix) {
        s = s[:len(s)-len(suffix)]
    }
    return s
}

// check URL
func parseURL(u string) (string, error) {
	_, err := url.ParseRequestURI(u)
	if err != nil {
		return u, errors.New("[" + u + "] is not a valid URL: " + err.Error())
	}

	if strings.HasSuffix(u, "/") {
		u = trimSuffix(u, "/")
	}

	return u, nil
}

///////////////////////////////////////////////////////////////////////////////

/*
SetDefaultE2coApplicationValues
*/
func SetDefaultE2coApplicationValues(e2coApplication *structs.E2coApplication) {
	// Main ID
	if len(e2coApplication.ID) == 0 {
		e2coApplication.ID = generateID()
	}

	// Created & Updated
	t := time.Now().String()
	e2coApplication.Created = t
	e2coApplication.Updated = t

	// Apps
	for index, app := range e2coApplication.Apps {
		log.Debug(pathLOG + "[SetDefaultE2coApplicationValues] Setting default values in app [" + app.Name + "] ...")

		// app ID
		if len(app.ID) == 0 {
			e2coApplication.Apps[index].ID = generateID()
		}

		// parent ID = Main ID
		if len(app.ParentID) == 0 {
			e2coApplication.Apps[index].ParentID = e2coApplication.ID
		}

		// Created & Updated
		e2coApplication.Apps[index].Created = t
		e2coApplication.Apps[index].Updated = t

		// DEFAULT VALUES:
		// locations
		// app locations: we have to look first in specific locations defined for the app (if they exist)
		// default task locations: if no specific locations are defined we take the default locations
		if len(app.Locations) == 0 {
			e2coApplication.Apps[index].Locations = e2coApplication.Locations
			e2coApplication.Apps[index].LocationsType = globals.V_GLOBAL
		} else {
			e2coApplication.Apps[index].LocationsType = globals.V_SPECIFIC
		}

		for j, location := range e2coApplication.Apps[index].Locations {
			if len(location.NameSpace) == 0 {
				e2coApplication.Apps[index].Locations[j].NameSpace = structs.DEFAULT_APP_LOCATION_NAMESPACE
			} else {
				e2coApplication.Apps[index].Locations[j].NameSpace = strings.ToLower(location.NameSpace)
			}
			e2coApplication.Apps[index].Locations[j].IDE2cOrchestrator = strings.ToLower(location.IDE2cOrchestrator)
		}
		
		// type
		if len(app.Type) == 0 {
			e2coApplication.Apps[index].Type = structs.DEFAULT_APP_TYPE
		} else {
			e2coApplication.Apps[index].Type = strings.ToLower(app.Type) 
		}

		// environment
		if len(app.Environment) == 0 {
			e2coApplication.Apps[index].Environment = e2coApplication.Environment
		}

		// Replicas
		if app.Replicas == 0 && e2coApplication.Replicas == 0{
			e2coApplication.Apps[index].Replicas = structs.DEFAULT_APP_REPLICAS
		} else if app.Replicas == 0 {
			e2coApplication.Apps[index].Replicas = e2coApplication.Replicas
		}

		// MaxReplicas
		if app.MaxReplicas == 0 {
			e2coApplication.Apps[index].MaxReplicas = e2coApplication.MaxReplicas
		}

		// Qos
		if len(app.Qos) == 0 {
			e2coApplication.Apps[index].Qos = e2coApplication.Qos
		}

		// exposed
		//if app.Service == (structs.E2coAppService{}) {
		//	e2coApplication.Apps[index].Service.Expose = false
		//}
	}
}
