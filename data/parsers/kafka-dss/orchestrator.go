//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 16 Sep 2021
// Updated on 16 Sep 2021
//
// @author: ATOS
//
package kafkadss

import (
	"encoding/json"
	"errors"
	"strings"

	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
	"atos.pledger/e2c-orchestrator/common/globals"
)

/*
ToE2cOrchestrator 
*/
func ToE2cOrchestrator(id string, infraInfoMap map[string]interface{}) (structs.E2cOrchestrator, error) {
	log.Debug(pathLOG + "[ToE2cOrchestrator] Setting E2cOrchestrator values from app info ...")

	var err error

	// Set Orchestrator values <= infraInfo
	var e2cOrchestrator structs.E2cOrchestrator
	e2cOrchestrator.ID = id
	e2cOrchestrator.Name = infraInfoMap["name"].(string)
	e2cOrchestrator.Type = strings.ToLower(infraInfoMap["type"].(string))

	// credentials
	if infraInfoMap["projectSets"] != nil {
		projectSets := infraInfoMap["projectSets"].([]interface{})
		for key := range projectSets {
			value := projectSets[key].(map[string]interface{})
			e2cOrchestrator.ConnectionToken = value["credentials"].(string)
			break
		}
	} else if infraInfoMap["credentials"] != nil {
		e2cOrchestrator.ConnectionToken = infraInfoMap["credentials"].(string)
	}

	// check if infrastructure is of SOE type
	api_endpoint := ""
	// field: "monitoring_plugin"
	// content: {'kubeconfig': '/var/kubeconfig_i2cat/pledger-i2cat.kubeconfig', 'goldpinger_endpoint': 'http://172.16.10.10:30080/', 
	//			 'soe_endpoint': 'http://172.16.10.6:30989/api/1.0', 'monitoring_type': 'metrics-server', 'monitoring_endpoint': 'http://172.16.10.10:30090',
	//			 'prometheus_endpoint': 'http://172.16.10.10:30090'}
	log.Debug(pathLOG + "[ToE2cOrchestrator] Checking 'monitoringPlugin' field content ...")
	if infraInfoMap["monitoringPlugin"] != nil {
		if _, ok := infraInfoMap["monitoringPlugin"].(interface{}); ok {
			var monitoring_plugin_properties map[string]interface{}
			aux := infraInfoMap["monitoringPlugin"].(string)
			aux = strings.ReplaceAll(aux, "'", "\"")
			err = json.Unmarshal([]byte(aux), &monitoring_plugin_properties)
			if err != nil || monitoring_plugin_properties == nil {
				log.Error(pathLOG+"[ToE2cOrchestrator] Error unmarshalling JSON 'monitoring_plugin_properties' info: ", err)
				return structs.E2cOrchestrator{}, errors.New("Error unmarshalling JSON 'monitoring_plugin_properties' info: " + err.Error())
			}
			
			if monitoring_plugin_properties["soe_endpoint"] != nil && monitoring_plugin_properties["soe_endpoint"].(string) != "" {
				log.Debug(pathLOG + "[ToE2cOrchestrator] Infrastructure is of SOE type")
				e2cOrchestrator.Type = globals.SOE

				api_endpoint = monitoring_plugin_properties["soe_endpoint"].(string)
			}

			if monitoring_plugin_properties["prometheus_endpoint"] != nil && monitoring_plugin_properties["prometheus_endpoint"].(string) != "" {
				log.Debug(pathLOG + "[ToE2cOrchestrator] Infrastructure defines a Prometheus endpoint")
				e2cOrchestrator.PrometheusEndPoint = monitoring_plugin_properties["prometheus_endpoint"].(string)
			}			
		} else {
			log.Warn(pathLOG+"[ToE2cOrchestrator] 'monitoringPlugin' is not of expected type: ", err)
		}
	} else {
		log.Warn(pathLOG+"[ToE2cOrchestrator] 'monitoringPlugin' field not found")
	}

	// endpoint
	if len(api_endpoint) > 0 {
		e2cOrchestrator.RESTAPIEndPoint = api_endpoint
	} else {
		e2cOrchestrator.RESTAPIEndPoint = infraInfoMap["endpoint"].(string)
	}
	e2cOrchestrator.RESTAPIEndPoint = trimSuffix(e2cOrchestrator.RESTAPIEndPoint, "/")

	// Properties
	if infraInfoMap["properties"] != "" {
		var properties map[string]interface{}
		aux := infraInfoMap["properties"].(string)
		aux = strings.ReplaceAll(aux, "'", "\"")
		err = json.Unmarshal([]byte(aux), &properties)
		if err != nil || properties == nil {
			log.Error(pathLOG+"[ToE2cOrchestrator] Error unmarshalling JSON properties info: ", err)
			return structs.E2cOrchestrator{}, errors.New("Error unmarshalling JSON properties info: " + err.Error())
		}
		e2cOrchestrator.Location = properties["infrastructure_location"].(string)
	}

	return e2cOrchestrator, nil
}

// trimSuffix
func trimSuffix(s, suffix string) string {
    if strings.HasSuffix(s, suffix) {
        s = s[:len(s)-len(suffix)]
    }
    return s
}