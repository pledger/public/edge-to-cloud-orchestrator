//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 26 Mar 2021
//
// @author: ATOS
//
package memory

import (
	"context"
	"errors"

	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
	cfg "atos.pledger/e2c-orchestrator/common/cfg"
	"atos.pledger/e2c-orchestrator/rest-api/sec/models"

	"go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/bson"
)

// path used in logs
const pathLOG string = "E2CO > Data > DB > MongoDB "

/*
Adapter Adapter
*/
type Adapter struct{}

// MongoDB
var client mongo.Client

// const
const MONGODB_URI string = "mongodb://localhost:27017"
const MONGODB_DATABASE string = "E2CO"
const MONGODB_COLLECTION_ORCHESTRATORS string = "ORCHESTRATORS"
const MONGODB_COLLECTION_APPLICATIONS string = "APPLICATIONS"
const MONGODB_COLLECTION_SUBAPPLICATIONS string = "SUBAPPLICATIONS"
const MONGODB_COLLECTION_USERS string = "USERS"
const MONGODB_COLLECTION_LOGS string = "LOGS"
const MONGODB_COLLECTION_SOE string = "SOE"


/*
Initialize initialization
*/
func (a Adapter) Initialize() error {
	log.Info(pathLOG + "[Initialize] Initializating connection to Database ...")

	connUrl := MONGODB_URI
	if len(cfg.Config.DatabaseConnection) > 0 {
		connUrl = cfg.Config.DatabaseConnection
	}

	c, err := setClient(connUrl)
	if err != nil {
		log.Error(pathLOG+"[Initialize] Could not establish a connection with the mongo database: [connUrl=" + connUrl + "] ")
	} else {
		client = *c
	}

	return err
}

/*
setClient  initializes connection to mongodb and sets the client
*/
func setClient(connUrl string) (*mongo.Client, error) {
	// Set client options
	clientOptions := options.Client().ApplyURI(connUrl)

	// Connect to MongoDB
	c, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Error(pathLOG+"[setClient] Error connecting to database [" + connUrl + "]: ", err)
		return nil, err
	}

	// Check the connection
	log.Info(pathLOG + "[setClient] Ping to database ...")
	err = c.Ping(context.TODO(), nil)
	if err != nil {
		log.Error(pathLOG+"[setClient] Cannot ping to database [" + connUrl + "]: ", err)
		return nil, err
	}

	log.Info(pathLOG + "[setClient] Connection established with " + connUrl)
	return c, nil
}

/*
setValue insert / update operation
id string, 
*/
func setValue(data interface{}, table string) error {
	collection := client.Database(MONGODB_DATABASE).Collection(table)

	insertResult, err := collection.InsertOne(context.TODO(), data)
	if err != nil {
		log.Error(pathLOG+"[setValue] Error inserting to database [" + MONGODB_DATABASE + "], table [" + table + "]: ", err)
		return err
	}

	log.Debug(pathLOG + "[setValue] Inserted a single document in [" + table + "]: ", insertResult.InsertedID)
	return nil
}

/*
updateValue update operation
*/
func updateValue(id string, data interface{}, table string) error {
	filter := bson.D{{"id", id}}
	collection := client.Database(MONGODB_DATABASE).Collection(table)
	update := bson.M{
        "$set": data,
    }

	updateResult, err := collection.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		log.Error(pathLOG+"[updateValue] Error updating value in database [" + MONGODB_DATABASE + "], table [" + table + "], id [" + id + "]: ", err)
		return err
	}

	log.Debug(pathLOG + "[updateValue] Update operation in [" + table + ", " + id + "]: Matched %v documents and updated %v documents: ", updateResult.MatchedCount, updateResult.ModifiedCount)
	return nil
}

/*
deleteValue delete operation
*/
func deleteValue(id string, table string) (string, error) {
	filter := bson.D{{"id", id}}
	collection := client.Database(MONGODB_DATABASE).Collection(table)

	res, err := collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		log.Error(pathLOG+"[deleteValue] Error deleting value in database [" + MONGODB_DATABASE + "], table [" + table + "], id [" + id + "]: ", err)
		return id, err
	}

	log.Debug(pathLOG + "[deleteValue] Deleted operation in [" + table + ", " + id + "]: Deleted %v documents: ", res.DeletedCount)
	return id, nil
}

/*
CloseDB Close the connection once no longer needed
*/
func (a Adapter) CloseConnection() {
	err := client.Disconnect(context.TODO())
	if err != nil {
		log.Error(pathLOG+"[CloseConnection] Error closing connection: ", err)
	} else {
		log.Info(pathLOG + "[CloseConnection] Connection to MongoDB closed.")
	}
}

///////////////////////////////////////////////////////////////////////////////
// E2CO Users - Security

/*
Signup creates a user in db
*/
func (a Adapter) Signup(u models.User) error {
	user, _ := a.GetUser(u.Email)
	if user == (models.User{}) {
		return setValue(u, MONGODB_COLLECTION_USERS)
	}

	return errors.New("User (email) already exists")
}

/*
Login logs users in
*/
func (a Adapter) Login(u models.LoginPayload) (models.User, error) {
	dbUser := models.User{}

	collection := client.Database(MONGODB_DATABASE).Collection(MONGODB_COLLECTION_USERS)

	filter := bson.D{{"email", u.Email}}

	err := collection.FindOne(context.TODO(), filter).Decode(&dbUser)
	if err != nil {
		log.Warn(pathLOG+"[Login] Warning: User not found in DB: ", err)
		return dbUser, err
	}

	log.Trace(pathLOG + "[Login] Found a single document:  %+v", dbUser)

	return dbUser, nil
}

/*
GetUsers get the list of users
*/
func (a Adapter) GetUsers() ([]models.User, error) {
	log.Trace(pathLOG + "[GetUsers] Getting users ...")
	var dbUsers []models.User

	collection := client.Database(MONGODB_DATABASE).Collection(MONGODB_COLLECTION_USERS)

	findOptions := options.Find()
	//findOptions.SetLimit(10)

	// Finding multiple documents returns a cursor
	cur, err := collection.Find(context.TODO(), bson.D{{}}, findOptions)
	if err != nil {
		log.Error(pathLOG+"[GetUsers] Error in 'collection.Find' call: ", err)
		return dbUsers, err
	}

	// Iterate through the cursor
	for cur.Next(context.TODO()) {
		var elem models.User
		err := cur.Decode(&elem)
		if err != nil {
			log.Error(pathLOG+"[GetUsers] Error decoding element: ", err)
		} else {
			elem.Password = "******"
			dbUsers = append(dbUsers, elem)
		}
	}

	if err := cur.Err(); err != nil {
		log.Error(pathLOG+"[GetUsers] Error in 'cur.Err' call: ", err)
	} else {
		cur.Close(context.TODO())	// Close the cursor once finished
	}

	log.Trace(pathLOG + "[GetUsers] Found multiple documents (array of pointers): %+v ", dbUsers)

	return dbUsers, nil
}


/*
GetUsers get a user
*/
func (a Adapter) GetUser(id string) (models.User, error) {
	dbUser := models.User{}

	collection := client.Database(MONGODB_DATABASE).Collection(MONGODB_COLLECTION_USERS)

	filter := bson.D{{"email", id}}

	err := collection.FindOne(context.TODO(), filter).Decode(&dbUser)
	if err != nil {
		log.Warn(pathLOG+"[GetUser] Warning: user not found in DB: ", err)
		return dbUser, err
	}

	log.Trace(pathLOG + "[GetUser] Found a single document:  %+v", dbUser)

	if err == nil {
		dbUser.Password = "******"
	}

	return dbUser, nil
}


///////////////////////////////////////////////////////////////////////////////
// Applications

// E2coApplication

/*
SetE2coApplication adds a anew application / updates application
*/
func (a Adapter) SetE2coApplication(id string, dbtask structs.E2coApplication) error {
	return setValue(dbtask, MONGODB_COLLECTION_APPLICATIONS)
}

/*
UpdateE2coApplication adds a anew application / updates application
*/
func (a Adapter) UpdateE2coApplication(id string, dbtask structs.E2coApplication) error {
	return updateValue(id, dbtask, MONGODB_COLLECTION_APPLICATIONS)
}

/*
ReadE2coApplicationValue gets an application from database
*/
func (a Adapter) ReadE2coApplication(id string) (structs.E2coApplication, error) {
	dbtask := structs.E2coApplication{}

	collection := client.Database(MONGODB_DATABASE).Collection(MONGODB_COLLECTION_APPLICATIONS)

	filter := bson.D{{"id", id}}

	err := collection.FindOne(context.TODO(), filter).Decode(&dbtask)
	if err != nil {
		log.Warn(pathLOG+"[ReadE2coApplication] Warning: Application not found in DB: ", err)
		return dbtask, err
	}

	log.Trace(pathLOG + "[ReadE2coApplication] Found a single document:  %+v", dbtask)

	return dbtask, nil
}

/*
DeleteE2coApplication removes an application from database
*/
func (a Adapter) DeleteE2coApplication(id string) (string, error) {
	return deleteValue(id, MONGODB_COLLECTION_APPLICATIONS)
}

/*
ReadAllApplications Return all applications from database
*/
func (a Adapter) ReadAllApplications() ([]structs.E2coApplication, error) {
	log.Trace(pathLOG + "[ReadAllApplications] Getting All apps ...")
	var dbtasks []structs.E2coApplication

	collection := client.Database(MONGODB_DATABASE).Collection(MONGODB_COLLECTION_APPLICATIONS)

	findOptions := options.Find()
	//findOptions.SetLimit(10)

	// Finding multiple documents returns a cursor
	cur, err := collection.Find(context.TODO(), bson.D{{}}, findOptions)
	if err != nil {
		log.Error(pathLOG+"[ReadAllApplications] Error in 'collection.Find' call: ", err)
		return dbtasks, err
	}

	// Iterate through the cursor
	for cur.Next(context.TODO()) {
		var elem structs.E2coApplication
		err := cur.Decode(&elem)
		if err != nil {
			log.Error(pathLOG+"[ReadE2coApplication] Error decoding element: ", err)
		} else {
			dbtasks = append(dbtasks, elem)
		}
	}

	if err := cur.Err(); err != nil {
		log.Error(pathLOG+"[ReadAllApplications] Error in 'cur.Err' call: ", err)
	} else {
		cur.Close(context.TODO())	// Close the cursor once finished
	}

	log.Trace(pathLOG + "[ReadAllApplications] Found multiple documents (array of pointers): %+v ", dbtasks)

	return dbtasks, nil
}

///////////////////////////////////////////////////////////////////////////////
// Sub Applications

// E2coApp

/*
SetE2coSubApp adds a anew application
*/
func (a Adapter) SetE2coSubApp(id string, subapp structs.E2coSubApp) error {
	return setValue(subapp, MONGODB_COLLECTION_SUBAPPLICATIONS)
}

/*
UpdateE2coSubApp updates application
*/
func (a Adapter) UpdateE2coSubApp(id string, subapp structs.E2coSubApp) error {
	return updateValue(id, subapp, MONGODB_COLLECTION_SUBAPPLICATIONS)
}

/*
ReadE2coSubApp gets an application from database
*/
func (a Adapter) ReadE2coSubApp(id string) (structs.E2coSubApp, error) {
	dbtask := structs.E2coSubApp{}

	collection := client.Database(MONGODB_DATABASE).Collection(MONGODB_COLLECTION_SUBAPPLICATIONS)

	filter := bson.D{{"id", id}}

	err := collection.FindOne(context.TODO(), filter).Decode(&dbtask)
	if err != nil {
		log.Warn(pathLOG+"[ReadE2coSubApp] Warning: Sub Application not found in DB: ", err)
		return dbtask, err
	}

	log.Trace(pathLOG + "[ReadE2coSubApp] Found a single document:  %+v", dbtask)

	return dbtask, nil
}

/*
DeleteE2coSubApp removes an application from database
*/
func (a Adapter) DeleteE2coSubApp(id string) (string, error) {
	return deleteValue(id, MONGODB_COLLECTION_SUBAPPLICATIONS)
}

/*
ReadAllSubApplications Return all sub applications from database
*/
func (a Adapter) ReadAllSubApplications() ([]structs.E2coSubApp, error) {
	log.Trace(pathLOG + "[ReadAllSubApplications] Getting All sub apps ...")
	var dbtasks []structs.E2coSubApp

	collection := client.Database(MONGODB_DATABASE).Collection(MONGODB_COLLECTION_SUBAPPLICATIONS)

	findOptions := options.Find()
	//findOptions.SetLimit(10)

	// Finding multiple documents returns a cursor
	cur, err := collection.Find(context.TODO(), bson.D{{}}, findOptions)
	if err != nil {
		log.Error(pathLOG+"[ReadAllSubApplications] Error in 'collection.Find' call: ", err)
		return dbtasks, err
	}

	// Iterate through the cursor
	for cur.Next(context.TODO()) {
		var elem structs.E2coSubApp
		err := cur.Decode(&elem)
		if err != nil {
			log.Error(pathLOG+"[ReadAllSubApplications] Error decoding element: ", err)
		} else {
			dbtasks = append(dbtasks, elem)
		}		
	}

	if err := cur.Err(); err != nil {
		log.Error(pathLOG+"[ReadAllSubApplications] Error in 'cur.Err' call: ", err)
	} else {
		cur.Close(context.TODO())	// Close the cursor once finished
	}

	log.Trace(pathLOG + "[ReadAllSubApplications] Found multiple documents (array of pointers): %+v ", dbtasks)

	return dbtasks, nil
}

///////////////////////////////////////////////////////////////////////////////
// Orchestrators

/*
SetOrchestrator adds a new orchestrator
*/
func (a Adapter) SetOrchestrator(id string, dbOrch structs.E2cOrchestrator) error {
	return setValue(dbOrch, MONGODB_COLLECTION_ORCHESTRATORS)
}

/*
UpdateOrchestrator updates value
*/
func (a Adapter) UpdateOrchestrator(id string, dbOrch structs.E2cOrchestrator) error {
	return updateValue(id, dbOrch, MONGODB_COLLECTION_ORCHESTRATORS)
}

/*
ReadOrchestrator gets an orchestrator from database
*/
func (a Adapter) ReadOrchestrator(id string) (structs.E2cOrchestrator, error) {
	dbOrch := structs.E2cOrchestrator{}

	collection := client.Database(MONGODB_DATABASE).Collection(MONGODB_COLLECTION_ORCHESTRATORS)

	filter := bson.D{{"id", id}}

	err := collection.FindOne(context.TODO(), filter).Decode(&dbOrch)
	if err != nil {
		log.Warn(pathLOG+"[ReadE2coSubApp] Warning: Orchestrator not found in DB: ", err)
		return dbOrch, err
	}

	log.Trace(pathLOG + "[ReadE2coSubApp] Found a single document:  %+v", dbOrch)

	return dbOrch, nil
}

/*
DeleteOrchestrator removes an orchestrator from database
*/
func (a Adapter) DeleteOrchestrator(id string) (string, error) {
	return deleteValue(id, MONGODB_COLLECTION_ORCHESTRATORS)
}

/*
ReadAllOrchestrators Return all orchestrators from database
*/
func (a Adapter) ReadAllOrchestrators() ([]structs.E2cOrchestrator, error) {
	log.Trace(pathLOG + "[ReadAllOrchestrators] Getting All orchestrators ...")
	var dbOrchs []structs.E2cOrchestrator

	collection := client.Database(MONGODB_DATABASE).Collection(MONGODB_COLLECTION_ORCHESTRATORS)

	// Finding multiple documents returns a cursor
	cur, err := collection.Find(context.TODO(), bson.D{{}})
	if err != nil {
		log.Error(pathLOG+"[ReadAllOrchestrators] Error in 'collection.Find' call: ", err)
		return dbOrchs, err
	}

	// Iterate through the cursor
	for cur.Next(context.TODO()) {
		var elem structs.E2cOrchestrator
		err := cur.Decode(&elem)
		if err != nil {
			log.Error(pathLOG+"[ReadAllOrchestrators] Error decoding element: ", err)
		} else {
			dbOrchs = append(dbOrchs, elem)
		}
	}

	if err := cur.Err(); err != nil {
		log.Error(pathLOG+"[ReadAllOrchestrators] Error in 'cur.Err' call: ", err)
	} else {
		cur.Close(context.TODO())	// Close the cursor once finished
	}

	log.Trace(pathLOG + "[ReadAllOrchestrators] Found multiple documents (array of pointers): %+v ", dbOrchs)

	return dbOrchs, nil
}

/*
ReadAllOrchestratorsApps Return all orchestrators from database
*/
func (a Adapter) ReadAllOrchestratorsApps() ([]structs.E2cOrchestratorApps, error) {
	log.Trace(pathLOG + "[ReadAllOrchestratorsApps] Getting All orchestrators (apps version) ...")
	var dbOrchs []structs.E2cOrchestratorApps

	return dbOrchs, nil
}

///////////////////////////////////////////////////////////////////////////////
// Logs

/*
SetLog store log
*/
func (a Adapter) SetLog(s string) error {
	return setValue(s, MONGODB_COLLECTION_LOGS)
}

/*
ReadLogs Return last n logs
*/
func (a Adapter) ReadLogs(n int) ([]string, error) {
	log.Trace(pathLOG + "[ReadLogs] Getting logs ...")
	var dbLogs []string

	collection := client.Database(MONGODB_DATABASE).Collection(MONGODB_COLLECTION_LOGS)

	// Finding multiple documents returns a cursor
	cur, err := collection.Find(context.TODO(), bson.D{{}})
	if err != nil {
		log.Error(pathLOG+"[ReadLogs] Error in 'collection.Find' call: ", err)
		return dbLogs, err
	}

	// Iterate through the cursor
	for cur.Next(context.TODO()) {
		var elem string
		err := cur.Decode(&elem)
		if err != nil {
			log.Error(pathLOG+"[ReadLogs] Error decoding element: ", err)
		} else {
			dbLogs = append(dbLogs, elem)
		}
	}

	if err := cur.Err(); err != nil {
		log.Error(pathLOG+"[ReadLogs] Error in 'cur.Err' call: ", err)
	} else {
		cur.Close(context.TODO())	// Close the cursor once finished
	}

	log.Trace(pathLOG + "[ReadLogs] Found multiple documents (array of pointers): %+v ", dbLogs)

	return dbLogs, nil
}

///////////////////////////////////////////////////////////////////////////////
// Orchestrators

/*
SetSOEObj adds a new SOEObj
*/
func (a Adapter) SetSOEObj(id string, dbOrch structs.E2coSoeObject) error {
	return setValue(dbOrch, MONGODB_COLLECTION_SOE)
}

/*
UpdateSOEObj updates value
*/
func (a Adapter) UpdateSOEObj(id string, dbOrch structs.E2coSoeObject) error {
	return updateValue(id, dbOrch, MONGODB_COLLECTION_SOE)
}

/*
ReadSOEObj gets a SOEObj from database
*/
func (a Adapter) ReadSOEObj(id string) (structs.E2coSoeObject, error) {
	dbOrch := structs.E2coSoeObject{}

	collection := client.Database(MONGODB_DATABASE).Collection(MONGODB_COLLECTION_SOE)

	filter := bson.D{{"id", id}}

	err := collection.FindOne(context.TODO(), filter).Decode(&dbOrch)
	if err != nil {
		log.Warn(pathLOG+"[ReadSOEObj] Warning: Orchestrator not found in DB: ", err)
		return dbOrch, err
	}

	log.Trace(pathLOG + "[ReadSOEObj] Found a single document:  %+v", dbOrch)

	return dbOrch, nil
}

/*
DeleteSOEObj removes a SOEObj from database
*/
func (a Adapter) DeleteSOEObj(id string) (string, error) {
	return deleteValue(id, MONGODB_COLLECTION_SOE)
}

/*
ReadAllSOEObjs Return all SOEObjs from database
*/
func (a Adapter) ReadAllSOEObjs() ([]structs.E2coSoeObject, error) {
	log.Trace(pathLOG + "[ReadAllSOEObjs] Getting All SOEObjs ...")
	var dbOrchs []structs.E2coSoeObject

	collection := client.Database(MONGODB_DATABASE).Collection(MONGODB_COLLECTION_SOE)

	// Finding multiple documents returns a cursor
	cur, err := collection.Find(context.TODO(), bson.D{{}})
	if err != nil {
		log.Error(pathLOG+"[ReadAllSOEObjs] Error in 'collection.Find' call: ", err)
		return dbOrchs, err
	}

	// Iterate through the cursor
	for cur.Next(context.TODO()) {
		var elem structs.E2coSoeObject
		err := cur.Decode(&elem)
		if err != nil {
			log.Error(pathLOG+"[ReadAllSOEObjs] Error decoding element: ", err)
		} else {
			dbOrchs = append(dbOrchs, elem)
		}
	}

	if err := cur.Err(); err != nil {
		log.Error(pathLOG+"[ReadAllSOEObjs] Error in 'cur.Err' call: ", err)
	} else {
		cur.Close(context.TODO())	// Close the cursor once finished
	}

	log.Trace(pathLOG + "[ReadAllSOEObjs] Found multiple documents (array of pointers): %+v ", dbOrchs)

	return dbOrchs, nil
}
