//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 26 Mar 2021
//
// @author: ATOS
//
package db

import (
	"strings"

	"atos.pledger/e2c-orchestrator/data/structs"
	mem "atos.pledger/e2c-orchestrator/data/db/memory"
	mongodb "atos.pledger/e2c-orchestrator/data/db/mongodb"
	log "atos.pledger/e2c-orchestrator/common/logs"
	cfg "atos.pledger/e2c-orchestrator/common/cfg"
	"atos.pledger/e2c-orchestrator/rest-api/sec/models"
	"atos.pledger/e2c-orchestrator/common/globals"
)

// adapter
var db DBdAdapter


// path used in logs
const pathLOG string = "E2CO > Data > DB "

/*
Initialize
*/
func InitializeDB() error {
	log.Info(pathLOG + "[InitializeDB] Setting database connection [" + strings.ToLower(cfg.Config.Database) + "] ...")

	if strings.ToLower(cfg.Config.Database) == globals.DB_MONGO {
		db = mongodb.Adapter{}
	} else {
		db = mem.Adapter{}
	}

	log.Info(pathLOG + "[InitializeDB] Initializing " + strings.ToLower(cfg.Config.Database) + " database adapter ...")
	err := db.Initialize()
	if err != nil && strings.ToLower(cfg.Config.Database) == globals.DB_MONGO {
		log.Error(pathLOG + "[InitializeDB] Error initializing mongodb database. Trying with default Database ...")
		db = mem.Adapter{}
		err = db.Initialize()
	} 
	
	if err != nil {
		log.Error(pathLOG + "[InitializeDB] Database adapter not initialized.")
	} else {
		log.Info(pathLOG + "[InitializeDB] Database adapter initialized.")
	}

	return err
}


/*
CloseDB Closes database
*/
func CloseConnection() {
	db.CloseConnection()
}

///////////////////////////////////////////////////////////////////////////////
// E2CO Users - Security

/*
Signup creates a user in db
*/
func Signup(u models.User) error {
	return db.Signup(u)
}

/*
Login logs users in
*/
func Login(u models.LoginPayload) (models.User, error) {
	return db.Login(u)
}

/*
GetUsers get the list of users
*/
func GetUsers() ([]models.User, error) {
	return db.GetUsers()
}

/*
GetUsers get a user
*/
func GetUser(id string) (models.User, error) {
	return db.GetUser(id)
}

///////////////////////////////////////////////////////////////////////////////
// E2cOrchestrator

/*
SetOrchestrator adds a new orchestrator
*/
func SetOrchestrator(id string, dbOrch structs.E2cOrchestrator) error {
	return db.SetOrchestrator(id, dbOrch)
}

/*
UpdateOrchestrator updates value
*/
func UpdateOrchestrator(id string, dbOrch structs.E2cOrchestrator) error {
	return db.UpdateOrchestrator(id, dbOrch)
}

/*
ReadOrchestrator gets an orchestrator from database
*/
func ReadOrchestrator(id string) (structs.E2cOrchestrator, error) {
	return db.ReadOrchestrator(id)
}

/*
DeleteOrchestrator removes an orchestrator from database
*/
func DeleteOrchestrator(id string) (string, error) {
	return db.DeleteOrchestrator(id)
}

/*
ReadAllOrchestrators Return all orchestrators from database
*/
func ReadAllOrchestrators() ([]structs.E2cOrchestrator, error) {
	return db.ReadAllOrchestrators()
}

/*
ReadAllOrchestratorsApps Return all orchestrators from database
*/
func ReadAllOrchestratorsApps() ([]structs.E2cOrchestratorApps, error) {
	return db.ReadAllOrchestratorsApps()
}

///////////////////////////////////////////////////////////////////////////////
// E2coApplication

/*
SetE2coApplication adds a anew application
*/
func SetE2coApplication(id string, dbtask structs.E2coApplication) error {
	return db.SetE2coApplication(id, dbtask)
}

/*
UpdateE2coApplication updates application
*/
func UpdateE2coApplication(id string, dbtask structs.E2coApplication) error {
	return db.UpdateE2coApplication(id, dbtask)
}

/*
ReadE2coApplication gets an application from database
*/
func ReadE2coApplication(id string) (structs.E2coApplication, error) {
	return db.ReadE2coApplication(id)
}

/*
DeleteE2coApplication removes an application from database
*/
func DeleteE2coApplication(id string) (string, error) {
	return db.DeleteE2coApplication(id)
}

/*
ReadAllApplications Return all applications from database
*/
func ReadAllApplications() ([]structs.E2coApplication, error) {
	return db.ReadAllApplications()
}

///////////////////////////////////////////////////////////////////////////////
// E2coApp

/*
SetE2coSubApp adds a new sub application
*/
func SetE2coSubApp(id string, dbtask structs.E2coSubApp) error {
	return db.SetE2coSubApp(id, dbtask)
}

/*
UpdateE2coSubApp updates sub application
*/
func UpdateE2coSubApp(id string, dbtask structs.E2coSubApp) error {
	return db.UpdateE2coSubApp(id, dbtask)
}

/*
ReadE2coSubApp gets an application from database
*/
func ReadE2coSubApp(id string) (structs.E2coSubApp, error) {
	return db.ReadE2coSubApp(id)
}

/*
DeleteE2coSubApp removes an application from database
*/
func DeleteE2coSubApp(id string) (string, error) {
	return db.DeleteE2coSubApp(id)
}

/*
ReadAllSubApplications Return all sub applications from database
*/
func ReadAllSubApplications() ([]structs.E2coSubApp, error) {
	return db.ReadAllSubApplications()
}

///////////////////////////////////////////////////////////////////////////////
// Logs

/*
SetLog store log
*/
func SetLog(s string) error {
	return db.SetLog(s)
}

/*
ReadLogs Return last n logs
*/
func ReadLogs(n int) ([]string, error) {
	return db.ReadLogs(n)
}

///////////////////////////////////////////////////////////////////////////////
// SOE

/*
SetSOEObj adds a new SOEObj
*/
func SetSOEObj(id string, dbOrch structs.E2coSoeObject) error {
	return db.SetSOEObj(id, dbOrch)
}

/*
UpdateSOEObj updates value
*/
func UpdateSOEObj(id string, dbOrch structs.E2coSoeObject) error {
	return db.UpdateSOEObj(id, dbOrch)
}

/*
ReadSOEObj gets a SOEObj from database
*/
func ReadSOEObj(id string) (structs.E2coSoeObject, error) {
	return db.ReadSOEObj(id)
}

/*
DeleteSOEObj removes a SOEObj from database
*/
func DeleteSOEObj(id string) (string, error) {
	return db.DeleteSOEObj(id)
}

/*
ReadAllSOEObjs Return all SOEObjs from database
*/
func ReadAllSOEObjs() ([]structs.E2coSoeObject, error) {
	return db.ReadAllSOEObjs()
}
