//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 26 Mar 2021
//
// @author: ATOS
//
package db

import (
	"atos.pledger/e2c-orchestrator/data/structs"
	"atos.pledger/e2c-orchestrator/rest-api/sec/models"
)

/*
DBdAdapter Data Base Adapter
*/
type DBdAdapter interface {
	// db
	Initialize() error
	CloseConnection()

	// users - sec
	Signup(u models.User) error
	Login(u models.LoginPayload) (models.User, error)
	GetUsers() ([]models.User, error)
	GetUser(id string) (models.User, error)
	// orchestrators
	SetOrchestrator(id string, dbOrch structs.E2cOrchestrator) error
	UpdateOrchestrator(id string, dbOrch structs.E2cOrchestrator) error
	ReadOrchestrator(id string) (structs.E2cOrchestrator, error)
	DeleteOrchestrator(id string) (string, error)
	ReadAllOrchestrators() ([]structs.E2cOrchestrator, error)
	ReadAllOrchestratorsApps() ([]structs.E2cOrchestratorApps, error)
	// applications
	SetE2coApplication(id string, dbtask structs.E2coApplication) error
	UpdateE2coApplication(id string, dbtask structs.E2coApplication) error
	ReadE2coApplication(id string) (structs.E2coApplication, error)
	DeleteE2coApplication(id string) (string, error)
	ReadAllApplications() ([]structs.E2coApplication, error)
	// sub applications
	SetE2coSubApp(id string, dbtask structs.E2coSubApp) error
	UpdateE2coSubApp(id string, dbtask structs.E2coSubApp) error
	ReadE2coSubApp(id string) (structs.E2coSubApp, error)
	DeleteE2coSubApp(id string) (string, error)
	ReadAllSubApplications() ([]structs.E2coSubApp, error)
	// logs
	SetLog(s string) error
	ReadLogs(n int) ([]string, error)
	// SOE
	SetSOEObj(id string, dbOrch structs.E2coSoeObject) error
	UpdateSOEObj(id string, dbOrch structs.E2coSoeObject) error
	ReadSOEObj(id string) (structs.E2coSoeObject, error)
	DeleteSOEObj(id string) (string, error)
	ReadAllSOEObjs() ([]structs.E2coSoeObject, error)
}

