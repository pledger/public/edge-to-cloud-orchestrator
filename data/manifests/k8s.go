//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package manifests

// Spec
type Spec struct {
	Replicas string   `yaml:"replicas"`
	Selector Selector `yaml:"selector"`
	Strategy Strategy `yaml:"strategy"`
	Template Template `yaml:"template"`
}

// Template
type Template struct {
	Metadata TemplateMetadata `yaml:"metadata"`
	Spec     TemplateSpec     `yaml:"spec"`
}

// TemplateSpec
type TemplateSpec struct {
	NodeSelector     map[string]string `yaml:"nodeSelector"`
	Containers       []Containers      `yaml:"containers"`
	//ImagePullSecrets map[string]string `yaml:"imagePullSecrets"`
	//ImagePullSecret  []string 		   `yaml:"imagePullSecret"`
	Volumes          []Volumes         `yaml:"volumes"`
	NodeName         string            `yaml:"nodeName"`
}

// Volumes
type Volumes struct {
	Name string `yaml:"name"`
}

// K8S Deployment
type K8SDeployment struct {
	ApiVersion string   `yaml:"apiVersion"`
	Kind       string   `yaml:"kind"`
	Metadata   Metadata `yaml:"metadata"`
	Spec       Spec     `yaml:"spec"`
	Status     Status   `yaml:"status"`
}

// Metadata
type Metadata struct {
	//CreationTimestamp interface{} `yaml:"creationTimestamp"`
	Name      string            `yaml:"name"`
	Namespace string            `yaml:"namespace"`
	Labels    map[string]string `yaml:"labels"`
}

// Containers
type Containers struct {
	Name            	string         		`yaml:"name"`
	Image           	string         		`yaml:"image"`
	ImagePullPolicy 	string         		`yaml:"imagePullPolicy"`
	ImagePullSecrets 	[]map[string]string `yaml:"imagePullSecrets"`
	ImagePullSecret  	[]string 		   	`yaml:"imagePullSecret"`
	ImageRepoName       string         		`yaml:"repo_user"`
	ImageRepoPassword	string              `yaml:"repo_password"`
	NetworkName			string              `yaml:"network_name"`
	Ports           	[]Ports        		`yaml:"ports"`
	Env             	[]Env          		`yaml:"env"`
	EnvDocker       	[]Env          		`yaml:"env_docker"`
	Command         	[]string       		`yaml:"command"`
	Args            	[]string       		`yaml:"args"`
	Resources       	Resources      		`yaml:"resources"`
	VolumeMounts    	[]VolumeMounts 		`yaml:"volumeMounts"`
}

// VolumeMounts
type VolumeMounts struct {
	Name      string `yaml:"name"`
	MountPath string `yaml:"mountPath"`
	ReadOnly  bool   `yaml:"readOnly"`
}

// Ports
type Ports struct {
	ContainerPort int    `yaml:"containerPort"`
	HostPort      int    `yaml:"hostPort"`
	Protocol      string `yaml:"protocol"`
}

// Selector
type Selector struct {
	MatchLabels map[string]string `yaml:"matchLabels"`
}

// TemplateMetadata
type TemplateMetadata struct {
	Labels map[string]string `yaml:"labels"`
}

// Env
type Env struct {
	Name  string `yaml:"name"`
	Value string `yaml:"value"`
}

// Resources
type Resources struct {
	Requests Requests `yaml:"requests"`
	Limits   Requests `yaml:"limits"`
}

// Requests
type Requests struct {
	Cpu    string `yaml:"cpu"`
	Memory string `yaml:"memory"`
}

// Status
type Status struct {
}

// Strategy
type Strategy struct {
}
