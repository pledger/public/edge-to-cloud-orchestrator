# E2CO UI

GUI for the E2C-Orchestrator component.

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

&copy; Atos Spain S.A. 2020

The E2C-Orchestrator GUI is a component of the European Project [Pledger](http://www.pledger-project.eu/).


-----------------------

[Description](#description)

[Installation Guide](#installation-guide)

[Usage Guide](#usage-guide)

[Available Scripts](#available-scripts)

[Credits](#credits)

[LICENSE](#license)

-----------------------

## Description




## Installation Guide



## Usage Guide



## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

```
npm install -g serve
serve -s build
```

## Credits

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### LICENSE

The Lifecycle application is licensed under [Apache License, version 2](LICENSE.TXT).
