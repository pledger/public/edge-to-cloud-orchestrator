//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 03 Mar 2021
//
// @author: ATOS
//
import React, { Component } from "react";
import { Button, Card, CardColumns, InputGroup, FormControl, ListGroup } from 'react-bootstrap';
import request from "request";


/**
 * Config
 */
class Config extends Component {

  /**
   * CONSTRUCTOR
   */
  constructor(props, context) {
    super(props, context);

    this.state = {
      url_rest_api_lm: global.rest_api_lm,
      E2COVersion: "",
      E2COPort: "",
      DefaultRESTAPIEndPoint: "",
      E2COSLALiteEndPoint: "",
      E2COPrometheusPushgatewayEndPoint: "",
      E2COPrometheusEndPoint: "",
      KafkaEndPoint: ""
    }

    // functions binding
    this.handleChange_rest_api_lm = this.handleChange_rest_api_lm.bind(this);
    this.handleChange_rest_api_um = this.handleChange_rest_api_um.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleRestore = this.handleRestore.bind(this);
    this.viewReport = this.viewReport.bind(this);
  }


  /**
   * after components are ready: load graph
   */
  componentDidMount() {
    this.viewReport(null);
  }


  /**
   * get all configuration info: call to rest api
   */
  viewReport(event) {
    console.log("Getting configuration report ...");
    try {
      var that = this;
      request.get({url: global.rest_api_lm + "config"}, function(err, resp, body) {
        if (err) {
          console.error(err);
          that.setState({ show_alert: true, msg: "GET /api/v1/config", msg_content: err.toString() });
          that.setState({ E2COVersion: "",
                          E2COPort: "",
                          DefaultRESTAPIEndPoint: "",
                          E2COSLALiteEndPoint: "",
                          E2COPrometheusPushgatewayEndPoint: "",
                          E2COPrometheusEndPoint: "",
                          KafkaEndPoint: ""});
        }
        else {
          console.log('Getting report ... ok');
          try {
            console.log(body);
            var conf = JSON.parse(body);

            if (conf.object != null) {
              that.setState({ E2COVersion: conf.object["E2COVersion"],
                              E2COPort: conf.object["E2COPort"],
                              DefaultRESTAPIEndPoint: conf.object["DefaultRESTAPIEndPoint"],
                              E2COSLALiteEndPoint: conf.object["E2COSLALiteEndPoint"],
                              E2COPrometheusPushgatewayEndPoint: conf.object["E2COPrometheusPushgatewayEndPoint"],
                              E2COPrometheusEndPoint: conf.object["E2COPrometheusEndPoint"],
                              KafkaEndPoint: conf.object["KafkaEndPoint"]});
            }
          }
          catch(err) {
            console.error(err);
            that.setState({ orchestrators: []});
          }
        }
      });
    }
    catch(err) {
      console.error(err);
      this.setState({ show_alert: true, msg: "GET /api/v1/ime/e2co", msg_content: err.toString() });
      that.setState({ orchestrators: []});
    }
  }


  handleChange_rest_api_lm(event) {
    this.setState({url_rest_api_lm: event.target.value});
  }


  handleChange_rest_api_um(event) {
    this.setState({url_rest_api_um: event.target.value});
  }


  handleSave(event) {
    global.rest_api_lm = this.state.url_rest_api_lm;
  }


  handleRestore(event) {
    global.rest_api_lm = global.const_rest_api_lm;

    this.setState({url_rest_api_lm: global.rest_api_lm});
  }


  render() {
    var swaggerui = this.state.url_rest_api_lm.replace("api/v1/", "swaggerui/");

    return (
      <div style={{margin: "0px 0px 0px 0px"}}>
          <div className="form-group row">
            <div className="col-sm-7">
              <i class="fa fa-wrench" aria-hidden="true"></i>
              &nbsp;
              UI and REST API configuration
            </div>
          </div>

        <CardColumns style={{display: 'flex', flexDirection: 'row'}}>

          <Card bg="info" style={{ width: '21rem' }}>
            <Card.Header style={{color: "white"}}><b>Edge-To-Cloud Orchestrator</b></Card.Header>
            <ListGroup variant="flush">
              <ListGroup.Item><b>E2CO Version</b>&nbsp;<i>{this.state.E2COVersion}</i></ListGroup.Item>
              <ListGroup.Item><b>E2CO Application Port</b>&nbsp;<i>{this.state.E2COPort}</i></ListGroup.Item>
            </ListGroup>
            <Card.Body>
              <Card.Title style={{color: "black"}}>Default REST API</Card.Title>
              <Card.Subtitle><i>{this.state.DefaultRESTAPIEndPoint}</i></Card.Subtitle>
              <hr />
              <Card.Title style={{color: "black"}}>SLA Framework</Card.Title>
              <Card.Subtitle><i>{this.state.E2COSLALiteEndPoint}</i></Card.Subtitle>
              <hr />
              <Card.Title style={{color: "black"}}>Pushgateway</Card.Title>
              <Card.Subtitle><i>{this.state.E2COPrometheusPushgatewayEndPoint}</i></Card.Subtitle>
              <hr />
              <Card.Title style={{color: "black"}}>Prometheus</Card.Title>
              <Card.Subtitle><i>{this.state.E2COPrometheusEndPoint}</i></Card.Subtitle>
              <hr />
              <Card.Title style={{color: "black"}}>Kafka</Card.Title>
              <Card.Subtitle><i>{this.state.KafkaEndPoint}</i></Card.Subtitle>
              <hr />
              <Card.Title style={{color: "black"}}>E2CO Swagger UI</Card.Title>
              <Card.Subtitle><i>{swaggerui}</i></Card.Subtitle>
            </Card.Body>
          </Card>

          <Card bg="dark" text="white" style={{ width: '21rem', height: '250px' }}>
            <Card.Header><b>E2CO REST API Configuration</b></Card.Header>
            <Card.Body>
              <Card.Text>
                <InputGroup size="sm" className="mb-3">
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" value={this.state.url_rest_api_lm}
                    onChange={this.handleChange_rest_api_lm} style={{ backgroundColor: "#E0F2F7" }}/>
                </InputGroup>
              </Card.Text>

              <Button variant="outline-success" size="sm" onClick={this.handleSave}><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save</Button>
              &nbsp;
              <Button variant="outline-light" size="sm" onClick={this.handleRestore}><i class="fa fa-eraser" aria-hidden="true"></i>&nbsp;Restore default</Button>
            </Card.Body>
          </Card>

          <Card bg="primary" style={{ width: '21rem', height: '400px' }}>
            <Card.Header><b>E2CO CONNECTORS</b></Card.Header>
            <Card.Body>
              <Card.Title>SLA Framework</Card.Title>
              <Card.Text>
                <InputGroup size="sm" className="mb-3">
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" value={this.state.E2COSLALiteEndPoint}
                    onChange={this.handleChange_rest_api_lm} style={{ backgroundColor: "#E0F2F7" }} disabled/>
                </InputGroup>
                <Button variant="outline-light" size="sm" onClick={()=> window.open(this.state.E2COSLALiteEndPoint+"/swaggerui/", "_blank")} >
                  SLA Framework UI
                </Button>
                &nbsp;
                <Button variant="outline-light" size="sm" onClick={()=> window.open(this.state.E2COSLALiteEndPoint, "_blank")} >
                  REST API
                </Button>
                &nbsp;
                <Button variant="outline-success" size="sm" onClick={this.handleSave} disabled>
                  <i class="fa fa-floppy-o" aria-hidden="true" ></i>&nbsp;Save
                </Button>
              </Card.Text>
              <Card.Title>Kafka</Card.Title>
              <Card.Text>
                <InputGroup size="sm" className="mb-3">
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" value={this.state.KafkaEndPoint}
                    onChange={this.handleChange_rest_api_um} style={{ backgroundColor: "#E0F2F7" }} disabled/>
                </InputGroup>
                &nbsp;
                <Button variant="outline-success" size="sm" onClick={this.handleSave} disabled>
                  <i class="fa fa-floppy-o" aria-hidden="true" ></i>&nbsp;Save
                </Button>
              </Card.Text>
            </Card.Body>
          </Card>

        </CardColumns>

      </div>
    );
  }
}

export default Config;
