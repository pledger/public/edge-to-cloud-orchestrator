//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 19 Mar 2021
// Updated on 19 Mar 2021
//
// @author: ATOS
//
import React, { Component } from "react";
import { Card, CardColumns } from 'react-bootstrap';
import request from "request";


/**
 * Init
 */
class Init extends Component {

  /**
   * CONSTRUCTOR
   */
  constructor(props, context) {
    super(props, context);
  }


  render() {
    return (
      <div style={{margin: "15px 0px 0px 0px"}}>
        <CardColumns style={{display: 'flex', flexDirection: 'row'}}>

          <Card bg="light" style={{flex: 1, color: '#fff', background: 'url(img/bg22.jpg) 100% no-repeat'}}>
            <Card.Img variant="top" src="img/logo122.png"  style={{width: "195px"}} />
            <Card.Body>
              <Card.Text>
              The Pledger project aims at delivering a new architectural paradigm and a toolset that will pave the way for next generation edge computing infrastructures, tackling the modern challenges faced today and coupling the benefits of low latencies on the edge, with the robustness and resilience of cloud infrastructures.
              </Card.Text>
            </Card.Body>
          </Card>

          <Card bg="primary" text="black" style={{ flex: 1 }}>
            <Card.Body>
              <Card.Title><b>Edge-To-Cloud-Orchestrator</b></Card.Title>
              <Card.Subtitle><font color="white"><i>version 0.1.4</i></font></Card.Subtitle>
              <Card.Text>
                This module is responsible for managing the deployment and execution of applications in the Pledger environment.
              </Card.Text>
              <Card.Text>
                The E2C-Orchestrator can deploy applications in devices / clusters running Kubernetes, Openshift, Docker and Docker Swarm.
              </Card.Text>
            </Card.Body>
            <Card.Img src="vendor/img/docker-logo.png" style={{width: "90px"}} />
            <Card.Img src="vendor/img/microk8s.png" style={{width: "90px"}} />
            <Card.Img src="vendor/img/docker-swarm-logo.png" style={{width: "90px"}} />
            <Card.Img src="vendor/img/kubernetes.png" style={{width: "90px"}} />
          </Card>

          <Card bg="light" text="black" style={{flex: 1}}>
            <Card.Img variant="top" src="vendor/img/e2co.png" fluid/>
            <Card.Body>
              <Card.Title><b>Edge-To-Cloud-Orchestrator architecture</b></Card.Title>
            </Card.Body>
          </Card>

        </CardColumns>
      </div>
    );
  }
}

export default Init;
