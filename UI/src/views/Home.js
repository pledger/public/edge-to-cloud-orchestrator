//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 03 Mar 2021
//
// @author: ATOS
//
import React, { Component } from "react";
import { Card, CardColumns } from 'react-bootstrap';
import request from "request";


/**
 * Home
 */
class Home extends Component {

  /**
   * CONSTRUCTOR
   */
  constructor(props, context) {
    super(props, context);

    this.state = {
      url_rest_api_um: global.rest_api_um,
      url_rest_api_lm: global.rest_api_lm,
      orchestrators: [],
      apps: []
    }

    // functions
    this.viewReportOrchs = this.viewReportOrchs.bind(this);
    this.viewReportApps = this.viewReportApps.bind(this);
    this.handleChange_rest_api_lm = this.handleChange_rest_api_lm.bind(this);
    this.handleChange_rest_api_um = this.handleChange_rest_api_um.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleRestore = this.handleRestore.bind(this);
  }

  /**
   *componentDidMount: after component is mount tasks
   */
  async componentDidMount() {
    this.viewReportOrchs(null);
    this.viewReportApps(null);
  }


  /**
   * get all orchestrators: call to rest api
   */
  viewReportOrchs(event) {
    console.log("Getting orchestrators report ...");
    try {
      var that = this;
      request.get({url: global.rest_api_lm + "ime/e2co"}, function(err, resp, body) {
        if (err) {
          console.error(err);
          that.setState({ show_alert: true, msg: "GET /api/v1/ime/e2co", msg_content: err.toString() });
          that.setState({ orchestrators: []});
        }
        else {
          console.log('Getting report ... ok');
          try {
            console.log(body);
            if (JSON.parse(body).list == null) {
              that.setState({ orchestrators: []});
            } else {
              that.setState({ orchestrators: JSON.parse(body).list});
            }
          }
          catch(err) {
            console.error(err);
            that.setState({ orchestrators: []});
          }
        }
      });
    }
    catch(err) {
      console.error(err);
      this.setState({ show_alert: true, msg: "GET /api/v1/ime/e2co", msg_content: err.toString() });
      that.setState({ orchestrators: []});
    }
  }


  /**
   * get all applications: call to rest api
   */
  viewReportApps(event) {
    console.log("Getting applications report ...");
    try {
      var that = this;
      request.get({url: global.rest_api_lm + "apps"}, function(err, resp, body) {
        if (err) {
          console.error(err);
          that.setState({ show_alert: true, msg: "GET /api/v1/apps", msg_content: err.toString() });
          that.setState({ apps: []});
        }
        else {
          console.log('Getting report ... ok');
          try {
            console.log(body);
            if (JSON.parse(body).list == null) {
              that.setState({ apps: []});
            } else {
              that.setState({ apps: JSON.parse(body).list});
            }
          }
          catch(err) {
            console.error(err);
            that.setState({ apps: []});
          }
        }
      });
    }
    catch(err) {
      console.error(err);
      this.setState({ show_alert: true, msg: "GET /api/v1/apps", msg_content: err.toString() });
      that.setState({ apps: []});
    }
  }


  handleChange_rest_api_lm(event) {
    this.setState({url_rest_api_lm: event.target.value});
  }


  handleChange_rest_api_um(event) {
    this.setState({url_rest_api_um: event.target.value});
  }


  handleSave(event) {
    global.rest_api_um = this.state.url_rest_api_um;
    global.rest_api_lm = this.state.url_rest_api_lm;
  }


  handleRestore(event) {
    global.rest_api_um = global.const_rest_api_um;
    global.rest_api_lm = global.const_rest_api_lm;

    this.setState({url_rest_api_lm: global.rest_api_lm});
    this.setState({url_rest_api_um: global.rest_api_um});
  }


  render() {
    return (
      <div style={{margin: "15px 0px 0px 0px"}}>
        <h3><b>Summary</b></h3>

        <h5><i class="fa fa-cloud" aria-hidden="true"></i>&nbsp;&nbsp;<b>List of clusters</b></h5>
        <table className="table table-sm table-bordered table-hover">
          <thead class="table-dark">
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Description</th>
              <th>Location</th>
              <th>Type</th>
              <th>IP</th>
            </tr>
          </thead>
          <tbody>
            {this.state.orchestrators.map(item => (
              <tr key={item.id}>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.description}</td>
                <td><i>-location-</i></td>
                <td>{item.type}</td>
                <td>{item.ip}</td>
              </tr>
            ))}
          </tbody>
        </table>

        <br />

        <h5><i class="fa fa-cogs" aria-hidden="true">&nbsp;&nbsp;</i><b>List of applications</b></h5>
        <table className="table table-sm table-bordered table-hover">
          <thead class="table-primary">
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Descriptoin </th>
              <th>Cluster</th>
              <th>Replicas</th>
              <th>Created</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {this.state.apps.map(item => (
              <tr key={item.id}>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td><i>-description-</i></td>
                <td>{item.idE2cOrchestrator}</td>
                <td>{item.replicas}</td>
                <td>{item.created}</td>
                <td>{item.status}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Home;
