//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 24 Mar 2021
//
// @author: ATOS
//
import React, { Component } from "react";
import request from "request";
import { Alert, Button, Badge, Form, Row, Col, Spinner, Modal, Tooltip, OverlayTrigger, Dropdown } from 'react-bootstrap';
import vis from "vis-network";


/**
 * Orchestrators
 */
class Orchestrators extends Component {


  /**
   * CONSTRUCTOR
   */
  constructor(props, context) {
    super(props, context);

    this.state = {
      sel_service_instance: "",
      isLoading: false,
      delete_si_button: false,
      isDeleting: false,
      msg: "",
      msg_content: "",
      show_alert: false,
      show_info: false,
      sel_orch_id_1: "",
      total_service_instances_1: 0,
      img_icon_service_type: "vendor/img/apps_mini.png",

      notCreatingNew: true,
      notSelected: true,

      // orchestrator
      orch_id: "",
      orch_ip: "",
      orch_type: "",
      orch_description: "",
      orch_name: "",
      orch_so: "",
      orch_restAPIEndPoint: "",
      orch_slaLiteEndPoint: "",
      orch_defaultnamespace: "",
      orch_connectionToken: "",
      orch_location: "",

      // Modal
      show: false,
      orch_json: "{}"
    };

    this.handleView2 = this.handleView2.bind(this);
    this.newCluster = this.newCluster.bind(this);
    this.clear = this.clear.bind(this);
    this.cancel = this.cancel.bind(this);
    this.cancel2 = this.cancel2.bind(this);
    this.save = this.save.bind(this);
    this.update = this.update.bind(this);

    this.handleChangeId = this.handleChangeId.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeDescription = this.handleChangeDescription.bind(this);
    this.handleChangeIP = this.handleChangeIP.bind(this);
    this.handleChangeType = this.handleChangeType.bind(this);
    this.handleChangeSO = this.handleChangeSO.bind(this);
    this.handleChangeRestAPIEndPoint = this.handleChangeRestAPIEndPoint.bind(this);
    this.handleChangeDefaultnamespace = this.handleChangeDefaultnamespace.bind(this);
    this.handleChangeConnectionToken = this.handleChangeConnectionToken.bind(this);
    this.handleChangeLocation = this.handleChangeLocation.bind(this);
    this.handleSelectType = this.handleSelectType.bind(this);

    this.handleDelete = this.handleDelete.bind(this);
    this.handleShow = this.handleShow.bind(this);
  }

  /**
   * after components are ready: load graph
   */
   componentDidMount() {
    this.handleView2(null);
  }

  // 
  newCluster() {
    this.setState({ notCreatingNew: false, notSelected: true});
    this.clear();
  }

  // 
  clear() {
    this.setState({orch_id: "", orch_ip: "", orch_type: "", orch_description: "", orch_location: "",
                  orch_name: "", orch_so: "", orch_restAPIEndPoint: "", orch_defaultnamespace: "",
                  orch_connectionToken: "", orch_json: "{}"});
  }

  // 
  cancel() {
    this.setState({ notCreatingNew: true, sel_orch_id_1: ""});
    this.clear();
  }

  // 
  cancel2() {
    this.setState({ notSelected: true, notCreatingNew: true, sel_orch_id_1: ""});
    this.clear();
  }

  // save cluster: call to api
  save(event) {
    this.setState({isLoading: true});
    var that = this;

    if (this.state.orch_id.trim() == "" || this.state.orch_name.trim() == "" 
          || this.state.orch_type.trim() == "" || this.state.orch_restAPIEndPoint.trim() == "") {
      that.setState({ isLoading: false, show_alert: true, msg: "Values are required", msg_content: "Id, Name, Type, EndPoint" });
    } else {
      // POST body
      var jsonObj = {
        "id": this.state.orch_id,
        "ip": this.state.orch_ip,
        "type": this.state.orch_type,
        "description": this.state.orch_description,
        "name": this.state.orch_name,
        "so": this.state.orch_so,
        "restAPIEndPoint": this.state.orch_restAPIEndPoint,
        "defaultNamespace": this.state.orch_defaultnamespace,
        "connectionToken": this.state.orch_connectionToken,
        "location": this.state.orch_location
      }

      console.log("Adding new orchestrator ...");
      try {
        request.post({url: global.rest_api_lm + 'ime/e2co', json: jsonObj}, function(err, resp, body) {
          if (err) {
            console.error(err);
            that.setState({ isLoading: false, show_alert: true, msg: "POST /api/v1/ime/e2co", msg_content: err.toString() });
          }
          else {
            console.log('Adding new orchestrator ... ok');
            if ((resp.statusCode == 200) && (body["resp"] != null) && (body["resp"] == "error")) {
              that.setState({ show_alert: true, msg: "Error adding a new cluster. Response: ", msg_content: JSON.stringify(body) });
            }
            else if (resp.statusCode == 200) {
              that.setState({ show_info: true, msg: "Cluster added. Response: ", msg_content: JSON.stringify(body) });
              that.sleep(1000);
              that.handleView2(null);
              that.setState({ notCreatingNew: true, sel_orch_id_1: ""});
              that.clear();
            }
            else {
              that.setState({ show_alert: true, msg: "POST /api/v1/ime/e2co => " + resp.statusCode + " / Undefined response obtained: ", msg_content: JSON.stringify(body) });
            }
            
            that.setState({orch_id: "", orch_ip: "", orch_type: "", orch_description: "", orch_name: "", orch_so: "",
                          orch_restAPIEndPoint: "", orch_defaultnamespace: "", orch_connectionToken: "", orch_location: "",
                          img_icon_service_type: "img/apps_mini.png", orch_json: "{}"});
          }

          that.setState({isLoading: false});
          that.cancel();
        });
      }
      catch(err) {
        console.error(err);
        this.setState({ isLoading: false, show_alert: true, msg: "POST /api/v1/ime/e2co", msg_content: err.toString(), isDeleting: false});
      }
    }
  }

  // Updates cluster information: call to api
  update(event) {
    this.setState({isLoading: true});
    var that = this;

    if (this.state.orch_id.trim() == "" || this.state.orch_name.trim() == "" 
          || this.state.orch_type.trim() == "" || this.state.orch_restAPIEndPoint.trim() == "") {
      that.setState({ isLoading: false, show_alert: true, msg: "Values are required", msg_content: "Id, Name, Type, EndPoint" });
    } else {
      // POST body
      var jsonObj = {
        "id": this.state.orch_id,
        "ip": this.state.orch_ip,
        "type": this.state.orch_type,
        "description": this.state.orch_description,
        "name": this.state.orch_name,
        "so": this.state.orch_so,
        "restAPIEndPoint": this.state.orch_restAPIEndPoint,
        "defaultNamespace": this.state.orch_defaultnamespace,
        "connectionToken": this.state.orch_connectionToken,
        "location": this.state.orch_location
      }

      console.log("Updating orchestrator [" + this.state.sel_orch_id_1 + "] ...");
      try {
        request.put({url: global.rest_api_lm + 'ime/e2co/' + this.state.sel_orch_id_1, json: jsonObj}, function(err, resp, body) {
          if (err) {
            console.error(err);
            that.setState({ isLoading: false, show_alert: true, msg: "PUT /api/v1/ime/e2co/" + that.state.sel_orch_id_1, msg_content: err.toString() });
          }
          else {
            console.log('Updating orchestrator ... ok');
            that.setState({ show_info: true, msg: "Cluster updated. Response: ", msg_content: JSON.stringify(body) });
            that.sleep(500); // wait 1/2 second
            that.handleView2(null); // reload clusters
            that.setState({orch_id: "", orch_ip: "", orch_type: "", orch_description: "", orch_location: "",
                          orch_name: "", orch_so: "", orch_restAPIEndPoint: "", orch_defaultnamespace: "", orch_connectionToken: "",
                          img_icon_service_type: "img/apps_mini.png", orch_json: "{}", sel_orch_id_1: "", notSelected: true});
          }

          that.setState({ isLoading: false});
        });
      }
      catch(err) {
        console.error(err);
        this.setState({ isLoading: false, show_alert: true, msg: "PUT /api/v1/ime/e2co/{id}", msg_content: err.toString(), isDeleting: false});
      }
    }
  }

  // delete cluster: call to api
  handleDelete(event) {
    this.setState({isDeleting: true});
    console.log("Removing orchestrator [" + this.state.sel_orch_id_1 + "] ...");
    var that = this;
    try {
      request.delete({url: global.rest_api_lm + 'ime/e2co/' + this.state.sel_orch_id_1}, function(err, resp, body) {
        if (err) {
          console.error(err);
          that.setState({ show_alert: true, msg: "DELETE /api/v1/ime/e2co/" + that.state.sel_orch_id_1, msg_content: err.toString() });
        }
        else {
          console.log('Removing orchestrator ... ok');
          that.setState({ show_info: true, msg: "DELETE /api/v1/ime/e2co/" + that.state.sel_orch_id_1 + " => " + resp.statusCode, msg_content: "Service instance deleted: response: " + body });
          that.sleep(1000);
          that.handleView2(null);
          that.setState({orch_id: "", orch_ip: "", orch_type: "", orch_description: "", orch_name: "", orch_so: "",
                         orch_restAPIEndPoint: "", orch_defaultnamespace: "", orch_connectionToken: "", orch_location: "",
                         img_icon_service_type: "img/apps_mini.png", orch_json: "{}"});
        }

        that.setState({isDeleting: false});
      });
    }
    catch(err) {
      console.error(err);
      this.setState({ show_alert: true, msg: "DELETE /api/v1/ime/e2co/{id}", msg_content: err.toString(), isDeleting: false});
    }
  }


  /**
   * Display selected cluster / orchestrator: call to REST API
   */
  displayCluster() {
    try {
      console.log("Getting orchestrators / call to REST API ...");
      var that = this;
      request.get({url: global.rest_api_lm + 'ime/e2co/' + that.state.sel_orch_id_1}, function(err, resp, body) {
        if (err) {
          console.error(err);
          that.setState({ show_alert: true, msg: "GET /api/v1/ime/e2co/{id}", msg_content: err.toString(), notSelected: true });
        }
        else {
          if (resp.statusCode == 200) {
            console.log('Getting orchestrators ... ok');

            body = JSON.parse(body);
            console.log(body);

            ////////////////////////////////////////////////////////////////////////////
            // FORM
            if (body['object'] != null) {
              // custer
              that.setState({orch_id: body['object']['id'],
                             orch_ip: body['object']['ip'],
                             orch_type: body['object']['type'],
                             orch_description: body['object']['description'],
                             orch_name: body['object']['name'],
                             orch_so: body['object']['so'],
                             orch_restAPIEndPoint: body['object']['restAPIEndPoint'],
                             orch_defaultnamespace: body['object']['defaultnamespace'],
                             orch_location: body['object']['location'],
                             orch_connectionToken: body['object']['connectionToken']});

              that.setState({notSelected: false});

              if (body['object']['type'] != null) {
                if (body['object']['type'] == "microK8s") {
                  that.setState({img_icon_service_type: "vendor/img/K8s.png"});
                } else if (body['object']['type'] == "docker-swarm") {
                  that.setState({img_icon_service_type: "img/docker-swarm-logo.png"});
                } else if (body['object']['type'] == "compss") {
                  that.setState({img_icon_service_type: "img/K8s.png.png"});
                } else if (body['object']['type'] == "docker-compose") {
                  that.setState({img_icon_service_type: "img/docker-compose-logo.png"});
                } else {
                  that.setState({img_icon_service_type: "img/K8s.png.png"});
                }
              }
            } else {
              that.setState({ sel_orch_id_1: "", notSelected: true});
              that.clear();
            }
            ////////////////////////////////////////////////////////////////////////////
          }
          else {
            that.setState({ show_alert: true, msg: "GET /api/v1/lm/service-instances/all",
                            msg_content: JSON.stringify(body) + " => " + resp.statusCode, orch_json: "{}" });
          }
        }
      });
    }
    catch(err) {
      console.error(err);
      this.setState({ show_alert: true, msg: "GET /api/v1/lm/service-instances/all", msg_content: err.toString() });
    }
  }

  /**
   * Load initial graph with all orchestrators managed by E2CO
   */
  handleView2(event) {
    this.clear();

    this.setState({isLoading: true});
    console.log('Getting orchestrators ...');
    // call to api
    try {
      var that = this;
      request.get({url: global.rest_api_lm + 'ime/e2co'}, function(err, resp, body) {
        if (err) {
          console.error(err);
          that.setState({ show_alert: true, msg: "GET /api/v1/ime/e2co", msg_content: err.toString() });
        }
        else {
          if (resp.statusCode == 200) {
            console.log('Getting orchestrators ... ok');

            body = JSON.parse(body);
            console.log(body);

            ////////////////////////////////////////////////////////////////////////////
            if (body['list'] != null && body['list'].length > 0) {
              // create an array with nodes
              var nodes2 = new vis.DataSet([
                {id: 'ag_1', label: "<b>E2C-Orchestrator</b>", image: './vendor/img/logo2.png', shape: 'image', title: "E2CO application",
                 font: {size:13, multi: true, color: "black", strokeWidth:2, strokeColor: "#dddddd"}, level: 0}
              ]);

              var edges2 = new vis.DataSet([]);

              body['list'].forEach(function(element) {
                var ncolor = "white";
                if (element['status'] == "started") {
                  ncolor = "lightgreen";
                } else if (element['status'] == "error") {
                  ncolor = "lightred";
                }

                if (element['type'].toLowerCase() == "k8s") {
                  nodes2.add({id: element['id'], label: element['id'] + "    (<i>" + element['ip'] + "</i>)", 
                              image: './vendor/img/K8s.png', shape: 'image',
                              font: {size:11, multi: true, color: ncolor}, level: 1});
                } else if (element['type'].toLowerCase() == "microk8s") {
                  nodes2.add({id: element['id'], label: element['id'] + "    (<i>" + element['ip'] + "</i>)", 
                              image: './vendor/img/microk8s_cluster.png', shape: 'image',
                              font: {size:11, multi: true, color: ncolor}, level: 1});
                } else if (element['type'].toLowerCase() == "docker") {
                  nodes2.add({id: element['id'], label: element['id'] + "    (<i>" + element['ip'] + "</i>)", 
                              image: './vendor/img/docker-logo.png', shape: 'image',
                              font: {size:11, multi: true, color: ncolor}, level: 1});
                } else {
                  nodes2.add({id: element['id'], label: element['id'], image: './vendor/img/apps_started_mini.png', shape: 'image',
                              font: {size:11, multi: true, color: ncolor}, level: 1});
                }

                if (element['status'] == "started") {
                  edges2.add({from: 'ag_1', to: element['id'], color:{color:"lightgreen"}, dashes: true});
                } else if (element['status'] == "error") {
                  edges2.add({from: 'ag_1', to: element['id'], color:{color:"lightred"}, dashes: true});
                } else {
                  edges2.add({from: 'ag_1', to: element['id'], color:{color:"white"}, dashes: [2,2,10,10]});
                }
              });

              // create a network2
              var container2 = document.getElementById('mynetwork2');
              var data2 = {
                nodes: nodes2,
                edges: edges2
              };
              var options2 = {
                nodes: {
                  borderWidth: 2,
                  size:20,
                  color: {
                    border: "darkgray",
                    background: "#fffffc",
                  },
                  font: { color: "#eeeeee" }
                },
                edges: {
                  color: 'lightgray'
                }
              };
              var network2 = new vis.Network(container2, data2, options2);

              that.setState({total_service_instances_1: body['list'].length});

              // EVENTS
              network2.on("click", function (params) {
                  console.log('params returns: ' + params.nodes);
                  if ((params != null) && (params.nodes[0] != "ag_1")) {
                    that.setState({ sel_orch_id_1: params.nodes[0] });
                    if (that.state.sel_orch_id_1 != "") {
                      console.log('showing orchestrator ...');
                      that.displayCluster();
                      that.setState({notCreatingNew: true});
                    } else {
                      console.log('setting orchestrator to default values ...');
                      that.setState({orch_id: "",  orch_ip: "", orch_type: "", orch_description: "", orch_name: "", orch_so: "",
                                     orch_restAPIEndPoint: "", orch_defaultnamespace: "", orch_connectionToken: "", orch_location: "",
                                     img_icon_service_type: "vendor/img/apps_mini.png", orch_json: "{}", notSelected: true});
                    }
                  } else {
                    that.setState({ sel_orch_id_1: "", notSelected: true});
                    that.clear();
                  }
              });

              that.sleep(1000);
            }
            ////////////////////////////////////////////////////////////////////////////
          }
          else {
            that.setState({ show_alert: true, msg: "GET /api/v1/ime/e2co", msg_content: JSON.stringify(body) + " => " + resp.statusCode });
          }
        }

        that.setState({isLoading: false});
      });
    }
    catch(err) {
      console.error(err);
      this.setState({ show_alert: true, msg: "GET /api/v1/ime/e2co", msg_content: err.toString(), isLoading: false });
    }
  }

  // sleep
  sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > milliseconds){
        break;
      }
    }
  }

  // 
  handleShow(event) {
    this.setState({show: true});
  }

  // alert messages
  onDismiss() {
    this.setState({ show_alert: false });
    this.setState({ show_info: false });
    this.setState({ msg: "" });
    this.setState({ msg_content: "" });
  }

  //////////////////////////
  // FORM 

  // 
  handleChangeId(event) {
    this.setState({orch_id: event.target.value});
  }

  // 
  handleChangeName(event) {
    this.setState({orch_name: event.target.value});
  }

  // 
  handleChangeDescription(event) {
    this.setState({orch_description: event.target.value});
  }

  // 
  handleChangeIP(event) {
    this.setState({orch_ip: event.target.value});
  }

  // 
  handleChangeType(event) {
    this.setState({orch_type: event.target.value});
  }

  // 
  handleChangeSO(event) {
    this.setState({orch_so: event.target.value});
  }

  // 
  handleChangeRestAPIEndPoint(event) {
    this.setState({orch_restAPIEndPoint: event.target.value});
  }

  // 
  handleChangeDefaultnamespace(event) {
    this.setState({orch_defaultnamespace: event.target.value});
  }

  // 
  handleChangeConnectionToken(event) {
    this.setState({orch_connectionToken: event.target.value});
  }

  // 
  handleChangeLocation(event) {
    this.setState({orch_location: event.target.value});
  }

  // 
  handleSelectType(eventKey, event) {
    this.setState({orch_type: eventKey});
    //this.setState({orch_type: event.target.value});
  }


  /**
   * render component
   */
  render() {
    return (
      <div style={{margin: "0px 0px 0px 0px"}}>

        <Alert variant="danger" toggle={this.onDismiss} show={this.state.show_alert}>
            <p><b>{this.state.msg}</b></p>
            <p className="mb-0">{this.state.msg_content}</p>
            <div className="d-flex justify-content-end">
                <Button onClick={() => this.setState({ show_alert: false })} variant="outline-danger">
                Close
                </Button>
            </div>
            </Alert>

            <Alert variant="primary" toggle={this.onDismiss} show={this.state.show_info}>
            <p><b>{this.state.msg}</b></p>
            <p className="mb-0">{this.state.msg_content}</p>
            <div className="d-flex justify-content-end">
                <Button onClick={() => this.setState({ show_info: false })} variant="outline-primary">
                Close
                </Button>
            </div>
        </Alert>


        <form>
          <div className="form-group row">
            <div className="col-sm-7">
              <i class="fa fa-cloud" aria-hidden="true"></i>
              &nbsp;
              <b>Clusters</b> managed by Edge-To-Cloud-Orchestration tool: &nbsp;
              <Badge variant="secondary">{this.state.total_service_instances_1}</Badge>
              {this.state.isLoading ?
              <Spinner animation="border" role="status" variant="primary">
                <span className="sr-only">Loading...</span>
              </Spinner> : ""}
            </div>
            
            <div className="col-sm-5">
              Cluster info:
              &nbsp;&nbsp;
              <Badge variant="primary" onClick={this.handleShow}  hidden={!this.state.notCreatingNew}>{this.state.sel_orch_id_1}</Badge>
            </div>
          </div>

          <div className="form-group row">
            <div className="col-sm-7">
              <div id="mynetwork2"></div>

              <Button variant="outline-primary" onClick={this.handleView2} hidden={!this.state.notCreatingNew || this.state.isLoading} size="sm">
                <i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;Refresh
              </Button>
              &nbsp;
              <Button variant="outline-primary" onClick={this.newCluster} hidden={!this.state.notCreatingNew || this.state.isLoading} size="sm">
                <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Add new cluster
              </Button>
              &nbsp;

              <Button variant="primary" onClick={this.handleView2} disabled={this.state.isLoading} hidden>
                <i class="fa fa-search" aria-hidden="true"></i>&nbsp;
                {this.state.isLoading ? "Loading ..." : "View"}
              </Button>
              &nbsp;
              <Button variant="danger" onClick={this.handleDelete} disabled={!this.state.delete_si_button && !this.state.isDeleting} hidden>
                <i class="fa fa-trash" aria-hidden="true"></i>&nbsp;
                {this.state.isDeleting ? "Removing ..." : "Remove"}
              </Button>
            </div>

            <div className="col-sm-5">

              <Form style={{margin: "10px 0px 0px 15px"}}>
                <OverlayTrigger placement="bottom" delay={{ show: 250, hide: 400 }} overlay={<Tooltip>Cluster / Orchestrator unique identifier</Tooltip>}>
                  <Form.Group as={Row}>
                    <Form.Text size="sm" column  className="col-sm-2">Id</Form.Text>
                    <Col sm={6}>
                      <Form.Control size="sm" placeholder="identifier" value={this.state.orch_id}
                      style={{ backgroundColor: "#FFFFFC" }} onChange={this.handleChangeId}/>
                    </Col>
                  </Form.Group>
                </OverlayTrigger>

                <Form.Group as={Row}>
                  <Form.Text size="sm" column  className="col-sm-2">Name</Form.Text>
                  <Col sm={6}>
                    <Form.Control size="sm" placeholder="name" value={this.state.orch_name}
                     style={{ backgroundColor: "#FFFFFC" }} onChange={this.handleChangeName}/>
                  </Col>
                </Form.Group>
                <Form.Group as={Row}>
                  <Form.Text size="sm" column  className="col-sm-2">Description</Form.Text>
                  <Col sm={8}>
                    <Form.Control size="sm" placeholder="description" value={this.state.orch_description}
                     style={{ backgroundColor: "#EEEEEE" }} onChange={this.handleChangeDescription}/>
                  </Col>
                </Form.Group>
                <Form.Group as={Row}>
                  <Form.Text size="sm" column  className="col-sm-2">Location</Form.Text>
                  <Col sm={8}>
                    <Form.Control size="sm" placeholder="Location" value={this.state.orch_location}
                     style={{ backgroundColor: "#EEEEEE" }} onChange={this.handleChangeLocation}/>
                  </Col>
                </Form.Group>
                <Form.Group as={Row}>
                  <Form.Text size="sm" column  className="col-sm-2">IP Address</Form.Text>
                  <Col sm={5}>
                    <Form.Control size="sm" placeholder="ip address" value={this.state.orch_ip}
                     style={{ backgroundColor: "#EEEEEE"}} onChange={this.handleChangeIP}/>
                  </Col>
                </Form.Group>

                <Form.Group as={Row}>
                  <Form.Text size="sm" column  className="col-sm-2">Type</Form.Text>
                  <Col sm={5}>
                    <Dropdown size="sm" onSelect={this.handleSelectType} value={this.state.orch_type}>
                      <Dropdown.Toggle variant="light" id="dropdown-basic" size="sm">{this.state.orch_type}</Dropdown.Toggle>
                      <Dropdown.Menu>
                        <Dropdown.Item eventKey="k8s">Kubernetes</Dropdown.Item>
                        <Dropdown.Item eventKey="microk8s">MicroK8s</Dropdown.Item>
                        <Dropdown.Item eventKey="docker">Docker</Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </Col>
                </Form.Group>

                <Form.Group as={Row}>
                  <Form.Text size="sm" column className="col-sm-2">O.S.</Form.Text>
                  <Col sm={5}>
                    <Form.Control size="sm" placeholder="Operating System" value={this.state.orch_so}
                     style={{ backgroundColor: "#FFFFFC" }} onChange={this.handleChangeSO}/>
                  </Col>
                </Form.Group>
                <Form.Group as={Row}>
                  <Form.Text size="sm" column  className="col-sm-2">EndPoint</Form.Text>
                  <Col sm={8}>
                    <Form.Control size="sm" placeholder="REST API Endpoint" value={this.state.orch_restAPIEndPoint}
                     style={{ backgroundColor: "#EEEEEE" }} onChange={this.handleChangeRestAPIEndPoint}/>
                  </Col>
                </Form.Group>
                <Form.Group as={Row}>
                  <Form.Text size="sm" column  className="col-sm-2">Default Namespace</Form.Text>
                  <Col sm={5}>
                    <Form.Control size="sm" placeholder="Default Namespace" value={this.state.orch_defaultnamespace}
                     style={{ backgroundColor: "#EEEEEE" }} onChange={this.handleChangeDefaultnamespace}/>
                  </Col>
                </Form.Group>
                <Form.Group as={Row}>
                  <Form.Text size="sm" column  className="col-sm-2">Connection Token</Form.Text>
                  <Col sm={8}>
                    <Form.Control size="sm" placeholder="Connection Token" value={this.state.orch_connectionToken}
                     style={{ backgroundColor: "#EEEEEE" }} onChange={this.handleChangeConnectionToken}/>
                  </Col>
                </Form.Group>

                <Button variant="outline-success" onClick={this.update} hidden={this.state.notSelected || this.state.isLoading} size="sm">
                  <i class="fa fa-save" aria-hidden="true"></i>&nbsp;Update
                </Button>&nbsp;
                <Button disabled variant="outline-danger" onClick={this.handleDelete} hidden={this.state.notSelected || this.state.isLoading} size="sm">
                  <i class="fa fa-eraser" aria-hidden="true"></i>&nbsp;Delete
                </Button>&nbsp;
                <Button variant="outline-danger" onClick={this.cancel2} hidden={this.state.notSelected || this.state.isLoading} size="sm">
                  <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancel
                </Button>&nbsp;
                <Button variant="outline-success" onClick={this.save} hidden={this.state.notCreatingNew || this.state.isLoading} size="sm">
                  <i class="fa fa-save" aria-hidden="true"></i>&nbsp;Save
                </Button>&nbsp;
                <Button variant="outline-warning" onClick={this.clear} hidden={this.state.notCreatingNew || this.state.isLoading} size="sm">
                  <i class="fa fa-eraser" aria-hidden="true"></i>&nbsp;Clear
                </Button>&nbsp;
                <Button variant="outline-danger" onClick={this.cancel} hidden={this.state.notCreatingNew || this.state.isLoading} size="sm">
                  <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancel
                </Button>
              </Form>
            </div>
          </div>
        </form>

        <Modal show={this.state.show} onHide={this.handleClose} size="lg">
          <Modal.Header closeButton>
            <Modal.Title>Orchestrator (JSON)</Modal.Title>
          </Modal.Header>
          <Modal.Body>{this.state.orch_json}</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default Orchestrators;
