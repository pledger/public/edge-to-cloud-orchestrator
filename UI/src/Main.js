//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 03 Mar 2021
//
// @author: ATOS
//
import React from 'react';
import { Navbar, Nav, OverlayTrigger, Tooltip } from 'react-bootstrap';
import Switch from 'react-switch';
import Home from "./views/Home";
import Orchestrators from "./views/Orchestrators";
import Apps from "./views/Apps";
import AppEdit from "./views/AppEdit";
import Config from "./views/Config";
import Init from "./views/Init";
import {
  Route,
  HashRouter
} from "react-router-dom";


/*
* Main
*/
const Main = ({
  collapsed,
  image,
  handleCollapsedChange,
  handleImageChange,
}) => {

  var swagger = "http://localhost:8333/swaggerui/";
  if (global.rest_api_lm != null) {
    global.rest_api_lm.replace("api/v1", "swaggerui");
  }

  return (
    <div className="container-fluid">
      <Navbar expand="lg" variant="dark" style={{margin: "0px -10px 10px -10px", background: "#23242D"}}>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto" as="ul">
            <Nav.Item as="li">
              <Switch
                height={16}
                width={30}
                checkedIcon={false}
                uncheckedIcon={false}
                onChange={handleCollapsedChange}
                checked={collapsed}
                onColor="#219de9"
                offColor="#bbbbbb"
              />
            </Nav.Item>

            <Nav.Item as="li" hidden>
              <Switch
                height={16}
                width={30}
                checkedIcon={false}
                uncheckedIcon={false}
                onChange={handleImageChange}
                checked={image}
                onColor="#219de9"
                offColor="#bbbbbb"
              />
            </Nav.Item>

            <div class="vl"></div>

            <Nav className="mr-auto" defaultActiveKey="#/" as="ul" className="ml-auto">
              <Nav.Item as="li">
                <OverlayTrigger placement="bottom" delay={{ show: 250, hide: 400 }} overlay={<Tooltip>Swagger REST API</Tooltip>}>
                  <Nav.Link exact href={swagger} target="_blank">
                    <b>Swagger</b>
                  </Nav.Link>
                </OverlayTrigger>
              </Nav.Item>
            </Nav>

          </Nav>
        </Navbar.Collapse>

        <Navbar.Brand href="http://www.pledger-project.eu/" target="_blank">
          <img
            src="img/logo122.png"
            width="140"
            height="35"
            className="d-inline-block align-top"
            alt="Pledger EU Project"
          />
        </Navbar.Brand>
      </Navbar>

      <div  style={{overflow: 'auto', height:'92%'}}>
        <HashRouter>
            <Route exact path="/" component={Init}/>
            <Route path="/home" component={Init}/>
            <Route path="/dashboard" component={Home}/>
            <Route path="/clusters" component={Orchestrators}/>
            <Route path="/apps" component={Apps}/>
            <Route path="/launch" component={AppEdit}/>
            <Route path="/config" component={Config}/>
        </HashRouter>
      </div>

    </div>
  );
};

export default Main;
