#!/usr/bin/bash
#export SLALiteEndPoint=http://localhost:32000
#export KafkaEndPoint=localhost:9093
#export KafkaTopic=sla_violation
export E2CO_SLA_ENDPOINT=http://localhost:32000
export E2CO_WARMING_TIME=120
#export E2CO_CONFIGSVC_ENDPOINT="http://confservice:80/api/"
export E2CO_CONFIGSVC_ENDPOINT="http://localhost:8000/api/"
export E2CO_CONFIGSVC_CREDENTIALS="url:http://localhost:8000/api/authenticate,username:api,password:apiH2020"
export E2CO_KAFKA_CLIENT=default
#export E2CO_KAFKA_ENDPOINT=localhost:9093
export E2CO_KAFKA_ENDPOINT=192.168.1.23:9095
export E2CO_KAFKA_VIOLATION_TOPIC=sla_violation
export E2CO_KAFKA_CONFIG_TOPIC=configuration
export E2CO_KAFKA_DEPLOY_TOPIC=deployment
export E2CO_KAFKA_SOE_TOPIC=soe_provisioning
export E2CO_KAFKA_KEYSTORE_PASSWORD=password
export E2CO_KAFKA_KEY_PASSWORD=password
export E2CO_KAFKA_TRUSTSTORE_PASSWORD=password
export E2CO_KAFKA_KEYSTORE_LOCATION=$HOME/tmp/kafka/keystore/localdev/kafka.client.keystore.p12
export E2CO_KAFKA_TRUSTSTORE_LOCATION=$HOME/tmp/kafka/truststore/localdev/kafka.client.truststore.pem
#./E2CO ./resources/config/config_tests.json
echo "go module tidy..."
#go mod tidy
echo "go build..."
CGO_ENABLED=0 GOOS=linux go build -a -o E2CO .
echo "go run..."
ulimit -n 4096
#go run main.go
./E2CO
