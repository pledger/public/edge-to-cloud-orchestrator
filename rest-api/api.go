//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 26 Mar 2021
//
// @author: ATOS
//
package restapi

import (
	"encoding/json"

	"atos.pledger/e2c-orchestrator/common/cfg"
	"atos.pledger/e2c-orchestrator/data/db"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
	"atos.pledger/e2c-orchestrator/orchestrator"
	"atos.pledger/e2c-orchestrator/data/parsers"

	"context"
	"flag"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

// path used in logs
const pathLOG string = "E2CO > Rest-API > api "

/*
NotImplementedFunc Default Function for not implemented calls
*/
func NotImplementedFunc(w http.ResponseWriter, r *http.Request) {
	log.Info(pathLOG + "[NotImplementedFunc] -Not implemented-")

	json.NewEncoder(w).Encode(structs.ResponseE2CO{
		Resp:        "ok",
		Method:      "NotImplementedFunc",
		Message:     "Function not implemented",
		E2COVersion: cfg.Config.E2COVersion})
}

/*
errorResponse response function
*/
func errorResponse(w http.ResponseWriter, method string, message string) {
	log.Error(pathLOG + "[" + method + "] " + message)

	json.NewEncoder(w).Encode(structs.ResponseE2CO{
		Resp:        "error",
		Method:      method,
		Message:     message,
		E2COVersion: cfg.Config.E2COVersion})
}

/*
enableCors
*/
func enableCors(w *http.ResponseWriter, r *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

/*
InitializeRESTAPI initialization function
*/
func InitializeRESTAPI() {
	log.Info(pathLOG + "[InitializeRESTAPI] Initializing Server ...")

	// initialize adapter after getting 'Mode' value
	//caas.InitializeAdapter()

	// set paths / routes
	router := mux.NewRouter()

	// routes:
	// default
	router.HandleFunc("/", NotImplementedFunc).Methods("GET")

	// configuration - status
	router.HandleFunc("/api/", GetVersionv1).Methods("GET")
	router.HandleFunc("/api/v1/", GetVersionv1).Methods("GET")        // status
	router.HandleFunc("/api/v1/version", GetVersionv1).Methods("GET") // version
	router.HandleFunc("/api/v1/config", GetConfigv1).Methods("GET") // version

	/************************************
	 Path: /api/v1/tasks...
	************************************/
	// E2coApplication:

	// get all tasks
	router.HandleFunc("/api/v1/apps", GetAllAppsv1).Methods("GET")
	// deploy task
	router.HandleFunc("/api/v1/apps", DeployTaskv1).Methods("POST", "OPTIONS")

	// get task
	router.HandleFunc("/api/v1/apps/{id}", GetE2coApplicationv1).Methods("GET")
	// migrate / scale up task
	router.HandleFunc("/api/v1/apps/{id}", NotImplementedFunc).Methods("PUT")
	// get task
	router.HandleFunc("/api/v1/apps/{id}/all", NotImplementedFunc).Methods("GET")
	// remove task
	router.HandleFunc("/api/v1/apps/{id}", RemoveTaskv1).Methods("DELETE")

	// E2coSubApp:

	// get sub app
	router.HandleFunc("/api/v1/apps/{id}/subapps/{idapp}", GetE2coAppv1).Methods("GET")
	// migrate / scale up sub app
	router.HandleFunc("/api/v1/apps/{id}/subapps/{idapp}", UpdateE2coAppv1).Methods("PUT")
	// remove sub app
	router.HandleFunc("/api/v1/apps/{id}/subapps/{idapp}", NotImplementedFunc).Methods("DELETE")
	// add new sub app
	router.HandleFunc("/api/v1/apps/{id}/subapps", NotImplementedFunc).Methods("POST", "OPTIONS")
	// get all sub apps
	router.HandleFunc("/api/v1/subapps", GetAllE2coAppsv1).Methods("GET")

	/************************************
	 ime functions
	 Path: /api/v1/ime/e2co...
	************************************/
	// ORCHESTRATORS:
	// get all available and running orchestrators
	router.HandleFunc("/api/v1/ime/e2co", GetAllOrchestratorsv1).Methods("GET")
	// get all available and running orchestrators with their apps
	router.HandleFunc("/api/v1/ime/e2co-apps", GetAllOrchestratorsWithAppsv1).Methods("GET")
	// add new existing orchestrator to managed orchestrators
	router.HandleFunc("/api/v1/ime/e2co", AddNewOrchestratorv1).Methods("POST", "OPTIONS")
	// update existing orchestrator
	router.HandleFunc("/api/v1/ime/e2co/{id}", UpdateOrchestratorv1).Methods("PUT")
	// get orchestrator info
	router.HandleFunc("/api/v1/ime/e2co/{id}", GetOrchestratorv1).Methods("GET")
	// remove orchestrator from managed orchestrators
	router.HandleFunc("/api/v1/ime/e2co/{id}", RemoveOrchestratorv1).Methods("DELETE")

	/************************************
	 ime functions
	 Path: /api/v1/ime/install... /uninstall...
	************************************/
	// ORCHESTRATORS INSTALLATION:
	// install a new orchestrator in a remote device add add this orchestrator to managed orchestrators
	router.HandleFunc("/api/v1/ime/install/{id}", NotImplementedFunc).Methods("POST")
	// get orchestrator installation info
	router.HandleFunc("/api/v1/ime/install/{id}", NotImplementedFunc).Methods("GET")
	// delete orchestrator from remote device
	router.HandleFunc("/api/v1/ime/uninstall/{id}", NotImplementedFunc).Methods("DELETE")

	/************************************
	 SLA integration
	 Path: /api/v1/sla...
	************************************/
	// sla - violation
	//router.HandleFunc("/api/v1/sla/{id}/guarantee/{guarantee}/violation", ProcessSLAViolationv1).Methods("POST")

	// swagger api
	sh := http.StripPrefix("/swaggerui/", http.FileServer(http.Dir("./rest-api/swaggerui/")))
	router.PathPrefix("/swaggerui/").Handler(sh)

	/////////////////////////////////////////////////////////////////
	// start server: 8333
	server := &http.Server{Addr: ":"+ strconv.Itoa(cfg.Config.E2COPort), Handler: router}

	stop := make(chan os.Signal)
	signal.Notify(stop, os.Interrupt)

	go func() {
		// init server
		log.Info(pathLOG + "[InitializeRESTAPI] Server listening on port " + strconv.Itoa(cfg.Config.E2COPort) + " ...")
		log.Info("................................................................................")
		if err := server.ListenAndServe(); err != nil {
			if err != http.ErrServerClosed {
				log.Fatal(err)
			}
		}
	}()

	<-stop

	/////////////////////////////////////////////////////////////////
	// after stopping server:
	log.Info("................................................................................")
	// close DB
	db.CloseConnection()

	// shutdown server
	log.Info(pathLOG + "[InitializeRESTAPI] Shutting down server ...")

	var shutdownTimeout = flag.Duration("shutdown-timeout", 10*time.Second, "shutdown timeout (5s,5m,5h) before connections are cancelled")
	ctx, cancel := context.WithTimeout(context.Background(), *shutdownTimeout)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		log.Fatal(err)
	}

	log.Info(pathLOG + "[InitializeRESTAPI] Terminated")
	log.Info("................................................................................")
}

///////////////////////////////////////////////////////////////////////////////
// FUNCTIONS:

/*
AddNewOrchestrator adds a new orchestrator (to be managed) to the system
*/
func AddNewOrchestratorv1(w http.ResponseWriter, r *http.Request) {
	enableCors(&w, r)

	log.Info("E2CO > Rest-API ######## [AddNewOrchestrator] Adding new orchestrator ... ########")

	orch, err := parsers.ParseE2cOrchestratorStruct(r) // parse json
	if err != nil {
		errorResponse(w, "AddNewOrchestrator", "Error parsing JSON to E2cOrchestrator struct: "+err.Error())
	} else {
		orchRes, err := orchestrator.AddNewOrchestrator(*orch) // response: (structs.E2cOrchestrator, error)
		if err != nil {
			errorResponse(w, "AddNewOrchestrator", err.Error())
		} else {
			// response
			json.NewEncoder(w).Encode(structs.ResponseE2CO{
				Resp:        "ok",
				Method:      "AddNewOrchestrator",
				Message:     "Orchestrator added",
				E2COVersion: cfg.Config.E2COVersion,
				Object:      orchRes})
		}
	}
}

/*
UpdateOrchestrator Updates an orchestrator
*/
func UpdateOrchestratorv1(w http.ResponseWriter, r *http.Request) {
	enableCors(&w, r)

	log.Info("E2CO > Rest-API ######## [UpdateOrchestrator] Updating orchestrator ... ########")

	params := mux.Vars(r)
	log.Debug(pathLOG + "[UpdateOrchestrator] id=" + params["id"])

	orch, err := parsers.ParseE2cOrchestratorStruct(r) // parse json / body
	if err != nil {
		errorResponse(w, "UpdateOrchestrator", "Error parsing JSON to E2cOrchestrator struct: "+err.Error())
	}

	orchRes, err := orchestrator.UpdateOrchestrator(params["id"], *orch) // response: (structs.E2cOrchestrator, error)
	if err != nil {
		errorResponse(w, "UpdateOrchestrator", err.Error())
	} else {
		json.NewEncoder(w).Encode(structs.ResponseE2CO{
			Resp:        "ok",
			Method:      "UpdateOrchestrator",
			Message:     "Orchestrator updated",
			E2COVersion: cfg.Config.E2COVersion,
			Object:      orchRes})
	}
}

/*
RemoveOrchestrator Removes a orchestrator
*/
func RemoveOrchestratorv1(w http.ResponseWriter, r *http.Request) {
	enableCors(&w, r)

	log.Info("E2CO > Rest-API ######## [RemoveOrchestrator] Deleting orchestrator ... ########")

	params := mux.Vars(r)
	log.Debug(pathLOG + "[RemoveOrchestrator] id=" + params["id"])

	err := orchestrator.RemoveOrchestrator(params["id"]) // response: (error)
	if err != nil {
		errorResponse(w, "RemoveOrchestrator", err.Error())
	} else {
		json.NewEncoder(w).Encode(structs.ResponseE2CO{
			Resp:        "ok",
			Method:      "RemoveOrchestrator",
			Message:     "Orchestrator deleted",
			E2COVersion: cfg.Config.E2COVersion})
	}
}

/*
GetConfig Get all config parameters
*/
func GetConfigv1(w http.ResponseWriter, r *http.Request) {
	enableCors(&w, r)

	log.Info("E2CO > Rest-API ######## [GetConfig] Getting E2CO configuratoin ... ########")

	respConfig := cfg.Config
	respConfig.Orchestrators = make([]cfg.Orchestrator, 0)

	// response
	json.NewEncoder(w).Encode(structs.ResponseE2CO{
			Resp:        "ok",
			Method:      "GetConfig",
			Message:     "Config found",
			E2COVersion: cfg.Config.E2COVersion,
			Object:      respConfig})
}

/*
GetVersion Get version
*/
func GetVersionv1(w http.ResponseWriter, r *http.Request) {
	enableCors(&w, r)

	log.Info("E2CO > Rest-API ######## [GetVersion] Getting E2CO version ... ########")

	// response
	json.NewEncoder(w).Encode(structs.ResponseE2CO{
			Resp:        "ok",
			Method:      "GetVersion",
			E2COVersion: cfg.Config.E2COVersion})
}

/*
GetAllOrchestrators Get all orchestrators
*/
func GetAllOrchestratorsv1(w http.ResponseWriter, r *http.Request) {
	enableCors(&w, r)

	log.Info("E2CO > Rest-API ######## [GetAllOrchestrators] Getting orchestrators managed by E2CO ... ########")

	// get orchestrators
	orchs, err := db.ReadAllOrchestrators() // ([]structs.E2cOrchestrator, error)

	// response
	if err != nil {
		errorResponse(w, "GetAllOrchestrators", "Error getting list of orchestrators: "+err.Error())
	} else {
		json.NewEncoder(w).Encode(structs.ResponseE2CO{
			Resp:        "ok",
			Method:      "GetAllOrchestrators",
			Message:     "Orchestrators found: " + strconv.Itoa(len(orchs)),
			E2COVersion: cfg.Config.E2COVersion,
			List:        orchs})
	}
}

/*
GetAllOrchestratorsWithApps Get all orchestrators and apps
*/
func GetAllOrchestratorsWithAppsv1(w http.ResponseWriter, r *http.Request) {
	enableCors(&w, r)

	log.Info("E2CO > Rest-API ######## [GetAllOrchestratorsWithApps] Getting orchestrators (apps) managed by E2CO ... ########")

	// get orchestrators
	orchs, err := db.ReadAllOrchestratorsApps() // ([]structs.E2cOrchestratorApps, error)

	// response
	if err != nil {
		errorResponse(w, "GetAllOrchestratorsWithApps", "Error getting list of orchestrators: "+err.Error())
	} else {
		// get apps
		_, err = db.ReadAllApplications() // ([]structs.E2coTaskExtended, error)
		if err != nil {
			errorResponse(w, "GetAllOrchestratorsWithApps", "Error getting list of apps: "+err.Error())
		} else {
			var orchsList []structs.E2cOrchestratorApps

			// TODO fix
			for _, orch := range orchs {
				/*for _, app := range apps {
					if app.IDE2cOrchestrator == orch.ID {
						log.Info(pathLOG + "[GetAllOrchestratorsWithApps] App [" + app.ID + "] added ...")
						orch.Apps = append(orch.Apps, app)
					}
				}*/

				log.Info(pathLOG + "[GetAllOrchestratorsWithApps] Orch [" + orch.ID + "] added ...")
				orchsList = append(orchsList, orch)
			}

			json.NewEncoder(w).Encode(structs.ResponseE2CO{
				Resp:        "ok",
				Method:      "GetAllOrchestratorsWithApps",
				Message:     "Orchestrators found: " + strconv.Itoa(len(orchs)),
				E2COVersion: cfg.Config.E2COVersion,
				List:        orchsList})
		}
	}
}

/*
GetOrchestrator Get an orchestrators
*/
func GetOrchestratorv1(w http.ResponseWriter, r *http.Request) {
	enableCors(&w, r)

	log.Info("E2CO > Rest-API ######## [GetOrchestrator] Getting orchestrator ... ########")

	params := mux.Vars(r)
	log.Debug(pathLOG + "[GetOrchestrator] id=" + params["id"])

	// get orchestrator
	orch, err := db.ReadOrchestrator(params["id"]) // ([]structs.E2cOrchestrator, error)

	// response
	if err != nil {
		errorResponse(w, "GetOrchestrator", "Error getting orchestrator: "+err.Error())
	} else {
		json.NewEncoder(w).Encode(structs.ResponseE2CO{
			Resp:        "ok",
			Method:      "GetOrchestrator",
			Message:     "Orchestrator found:",
			E2COVersion: cfg.Config.E2COVersion,
			Object:      orch})
	}
}

/*
GetE2coApplication Get an application
*/
func GetE2coApplicationv1(w http.ResponseWriter, r *http.Request) {
	enableCors(&w, r)

	log.Info("E2CO > Rest-API ######## [GetE2coApplication] Getting application [E2coApplication] ... ########")

	params := mux.Vars(r)
	log.Debug(pathLOG + "[GetE2coApplication] id=" + params["id"])

	// get app
	app, err := db.ReadE2coApplication(params["id"]) // db.ReadApplicationValue(params["id"]) // ([]structs.E2coTaskExtended, error)

	// response
	if err != nil {
		errorResponse(w, "GetE2coApplication", "Error getting application: "+err.Error())
	} else {
		respGetTask, _ := orchestrator.GetE2coApplication(params["id"])
		json.NewEncoder(w).Encode(structs.ResponseE2CO{
			Resp:        	"ok",
			Method:      	"GetE2coApplication",
			Message:     	"Application found",
			E2COVersion: 	cfg.Config.E2COVersion,
			Object:      	app,
			ExtraContent: 	respGetTask})
	}
}

/*
GetE2coApp Get an application
*/
func GetE2coAppv1(w http.ResponseWriter, r *http.Request) {
	enableCors(&w, r)

	log.Info("E2CO > Rest-API ######## [GetE2coApp] Getting sub application [E2coApp] ... ########")

	params := mux.Vars(r)
	log.Debug(pathLOG + "[GetE2coApp] id=" + params["id"] + ", idapp=" + params["idapp"])

	// get app
	app, err := db.ReadE2coSubApp(params["idapp"])

	// response
	if err != nil {
		errorResponse(w, "GetE2coApp", "Error getting sub application: "+err.Error())
	} else {
		respGetTask, _ := orchestrator.GetE2coApp(params["idapp"])
		json.NewEncoder(w).Encode(structs.ResponseE2CO{
			Resp:        	"ok",
			Method:      	"GetE2coApp",
			Message:     	"Sub application found",
			E2COVersion: 	cfg.Config.E2COVersion,
			Object:      	app,
			ExtraContent: 	respGetTask})
	}
}

/*
GetAllApps Get all applicatoins
*/
func GetAllAppsv1(w http.ResponseWriter, r *http.Request) {
	enableCors(&w, r)

	log.Info("E2CO > Rest-API ######## [GetAllApps] Getting applications managed by E2CO ... ########")

	// get apps
	apps, err := db.ReadAllApplications()

	// response
	if err != nil {
		errorResponse(w, "GetAllApps", "Error getting list of applications: "+err.Error())
	} else {
		json.NewEncoder(w).Encode(structs.ResponseE2CO{
			Resp:        "ok",
			Method:      "GetAllApps",
			Message:     "Apps found: " + strconv.Itoa(len(apps)),
			E2COVersion: cfg.Config.E2COVersion,
			List:        apps})
	}
}

/*
GetAllE2coApps Get all sub applicatoins
*/
func GetAllE2coAppsv1(w http.ResponseWriter, r *http.Request) {
	enableCors(&w, r)

	log.Info("E2CO > Rest-API ######## [GetAllE2coApps] Getting sub applications managed by E2CO ... ########")

	// get apps
	apps, err := db.ReadAllSubApplications()

	// response
	if err != nil {
		errorResponse(w, "GetAllE2coApps", "Error getting list of sub applications: "+err.Error())
	} else {
		json.NewEncoder(w).Encode(structs.ResponseE2CO{
			Resp:        "ok",
			Method:      "GetAllE2coApps",
			Message:     "Sub apps found: " + strconv.Itoa(len(apps)),
			E2COVersion: cfg.Config.E2COVersion,
			List:        apps})
	}
}

/*
DeployTask Deploy a task
*/
func DeployTaskv1(w http.ResponseWriter, r *http.Request) {
	enableCors(&w, r)

	log.Info("E2CO > Rest-API ######## [DeployTask] Deploying new application ... ########")

	// parse json
	e2coApp, err := parsers.ParseE2coApplicationStruct(r)
	if err != nil {
		errorResponse(w, "DeployTask", err.Error())
	} else {
		err := orchestrator.DeployE2coApplication(&e2coApp, true) // response: (structs.E2cOrchestrator, error)
		if err != nil {
			errorResponse(w, "DeployTask", err.Error())
		} else {
			// response
			json.NewEncoder(w).Encode(structs.ResponseE2CO{
				Resp:        "ok",
				Method:      "DeployTask",
				Message:     "application deployed",
				E2COVersion: cfg.Config.E2COVersion,
				Object:      e2coApp})
		}
	}
}

/*
RemoveTask Deploy a task
*/
func RemoveTaskv1(w http.ResponseWriter, r *http.Request) {
	enableCors(&w, r)

	log.Info("E2CO > Rest-API ######## [RemoveTask] Removing task ... ########")

	params := mux.Vars(r)
	log.Debug(pathLOG + "[RemoveTask] id=" + params["id"])

	err := orchestrator.RemoveE2coApplication(params["id"]) // response: (error)
	if err != nil {
		errorResponse(w, "RemoveTask", err.Error())
	} else {
		// return response
		json.NewEncoder(w).Encode(structs.ResponseE2CO{
			Resp:        "ok",
			Method:      "RemoveTask",
			Message:     "application removed",
			E2COVersion: cfg.Config.E2COVersion})
	}
}

/*
UpdateE2coApp Update E2coApp
*/
func UpdateE2coAppv1(w http.ResponseWriter, r *http.Request) {
	enableCors(&w, r)
	
	log.Debug("E2CO > Rest-API ######## [UpdateE2coApp] Updating task ... ########")
	params := mux.Vars(r)
	log.Debug(pathLOG + "[UpdateE2coApp] id=" + params["id"] + ", idapp=" + params["idapp"])

	log.Info(pathLOG + "[UpdateE2coApp] Updating application [" + params["id"] + ", " + params["idapp"] +  "] ...")

	// parse json
	taskOperation, err := parsers.ParseTaskOperationStruct(r)
	if err != nil {
		errorResponse(w, "UpdateE2coApp", err.Error())
	} else {
		// TODO
		// check app ==> 'id'

		// get app
		app, err := db.ReadE2coSubApp(params["idapp"]) // (*structs.E2coTaskExtended, error)
		// response
		if err != nil {
			errorResponse(w, "GetApp", "Error getting application: "+err.Error())
		} else {
			err = orchestrator.UpdateE2coSubApp(&app, *taskOperation)
			if err != nil {
				errorResponse(w, "UpdateE2coApp", err.Error())
			} else {
				// response
				json.NewEncoder(w).Encode(structs.ResponseE2CO{
					Resp:        "ok",
					Method:      "UpdateE2coApp",
					Message:     "application updated",
					E2COVersion: cfg.Config.E2COVersion,
					Object:      app})
			}
		}
	}
}

/*
ProcessSLAViolation processes SLA violations
*/
/*
func ProcessSLAViolationv1(w http.ResponseWriter, r *http.Request) {
	enableCors(&w, r)

	log.Info("E2CO > Rest-API ######## [ProcessSLAViolation] Processing violation ... ########")

	params := mux.Vars(r)
	log.Debug(pathLOG + "[UpdateOrchestrator] id=" + params["id"])
	log.Debug(pathLOG + "[UpdateOrchestrator] guarantee=" + params["guarantee"])

	// parse json
	v, err := parseViolationInfoStruct(r)
	if err != nil {
		errorResponse(w, "ProcessSLAViolation", "Error parsing violation info: "+err.Error())
	} else {
		resStr, err := dss.ProcessSLAViolations(*v)
		if err != nil {
			errorResponse(w, "ProcessSLAViolation", err.Error())
		} else {
			// response
			json.NewEncoder(w).Encode(structs.ResponseE2CO{
				Resp:        "ok",
				Method:      "ProcessSLAViolation",
				Message:     resStr,
				E2COVersion: cfg.Config.E2COVersion,
				Object:      v})
		}
	}
}
*/