//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 26 Mar 2021
//
// @author: ATOS
//
package restapi

import (
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/rest-api/sec/controllers"
	"atos.pledger/e2c-orchestrator/rest-api/sec/middlewares"
	"atos.pledger/e2c-orchestrator/data/db"
	"atos.pledger/e2c-orchestrator/common/cfg"
	"atos.pledger/e2c-orchestrator/orchestrator"
	"atos.pledger/e2c-orchestrator/data/structs"
	"atos.pledger/e2c-orchestrator/data/parsers"

	"github.com/gin-gonic/gin"
	"github.com/gin-contrib/cors"

	"strconv"
	"os/signal"
	"os"
	"context"
	"net/http"
	"syscall"
	"time"
	"errors"
)

/*
ResponseNotImplementedFunc Default Function for not implemented calls
*/
func ResponseNotImplementedFuncV2(c *gin.Context) {
	log.Warn(pathLOG + "[ResponseNotImplementedFunc] -Not implemented-")

	c.JSON(200, gin.H{
		"Resp":        "ok",
		"Method":      "ResponseNotImplementedFunc",
		"Message":     "Function not implemented",
		"E2COVersion": cfg.Config.E2COVersion})
}

/*
ResponseError response function
*/
func ResponseError(c *gin.Context, method string, message string) {
	log.Error(pathLOG + "[" + method + "] " + message)

	c.JSON(500, gin.H{
		"Resp":        "error",
		"Method":      method,
		"Message":     message,
		"E2COVersion": cfg.Config.E2COVersion})
}

/*
InitializeRESTAPIv2 initialization function
*/
func InitializeRESTAPIv2() {
	// database initialization
	/*log.Info(pathLOG + "[InitializeRESTAPIv2] Initializing REST API database ...")
	err := database.InitDatabase()
	if err != nil {
		log.Error(err)
	} else {
		database.GlobalDB.AutoMigrate(&models.User{})
		// TODO merge this db with the one used in E2CO
	}*/

	/////////////////////////////////////////////////////////////////
	// SERVER initialization
	log.Info(pathLOG + "[InitializeRESTAPIv2] Initializing Server (v2) ...")
	log.Info("..............................................................................")
	r := gin.Default()

	// CORS https://github.com/gin-contrib/cors
	// - PUT and PATCH methods
	// - Origin header
	// - Credentials share
	// - Preflight requests cached for 12 hours
	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "POST", "DELETE", "PATCH", "OPTIONS", "GET"}, // "POST, GET, DELETE, PUT, OPTIONS"
		AllowHeaders:     []string{"Origin"}, // "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization"
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		/*AllowOriginFunc: func(origin string) bool {
			return origin == "https://github.com"
		},*/
		MaxAge: 12 * time.Hour,
	}))

	// ping
	r.GET("/api/v1/public/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})

	api := r.Group("/api") 
	{
		// public methods
		public := api.Group("/v1/public")
		{
			// security:
			public.POST("/login", controllers.Login)
			public.POST("/signup", controllers.Signup)

			// default - configuration - status
			public.GET("/", ResponseNotImplementedFuncV2) 
			public.GET("/version", GetVersionV2)
			public.GET("/config", GetConfigV2) 

			// sla - violation 
			// TODO implement JWT / User connection in SLA ==> protected method
			//public.POST("/api/v1/sla/:id/guarantee/:guarantee/violation", ProcessSLAViolationV2) 
		}

		// protected metods; auth required
		protected := api.Group("/v1").Use(middlewares.Authz())
		{
			// security:
			protected.GET("/profile/:id", GetUser)
			protected.GET("/users", GetUsers)

			// applications:
			protected.GET("/apps", GetAllAppsV2)							// get all tasks
			protected.POST("/apps", DeployTaskV2)							// deploy task OPTIONS!!!
			protected.GET("/apps/:id", GetE2coApplicationV2)				// get task
			protected.PUT("/apps/:id", UpdateE2coApplication) 				// migrate / scale up task
			protected.GET("/apps/:id/all", ResponseNotImplementedFuncV2) 	// get task
			protected.DELETE("/apps/:id", RemoveTaskV2) 					// remove task

			// subapplications: E2coSubApp
			protected.GET("/apps/:id/subapps/:idapp", GetE2coAppV2) 					// get sub app
			protected.PUT("/apps/:id/subapps/:idapp", UpdateE2coSubApp) 				// migrate / scale up sub app
			protected.DELETE("/apps/:id/subapps/:idapp", ResponseNotImplementedFuncV2) 	// remove sub app
			protected.POST("/apps/:id/subapps", ResponseNotImplementedFuncV2) 			// add new sub app
			protected.GET("/subapps", GetAllE2coAppsV2) 								// get all sub apps

			// orchestrators:
			protected.GET("/ime/e2co", GetAllOrchestratorsV2)					// get all available and running orchestrators
			protected.GET("/ime/e2co-apps", GetAllOrchestratorsWithAppsV2)	// get all available and running orchestrators with their apps
			protected.POST("/ime/e2co", AddNewOrchestratorV2)					// add new existing orchestrator to managed orchestrators
			protected.PUT("/ime/e2co/:id", UpdateOrchestratorV2)				// update existing orchestrator
			protected.GET("/ime/e2co/:id", GetOrchestratorV2)					// get orchestrator info
			protected.DELETE("/ime/e2co/:id", RemoveOrchestratorV2) 			// remove orchestrator from managed orchestrators

			// orchestrators installation:
			protected.POST("/ime/install/:id", ResponseNotImplementedFuncV2) 		// install a new orchestrator in a remote device
			protected.GET("/ime/install/:id", ResponseNotImplementedFuncV2) 		// get orchestrator installation info
			protected.DELETE("/ime/uninstall/:id", ResponseNotImplementedFuncV2) 	// delete orchestrator from remote device
		}
	}

	// swagger
	r.Static("/swaggerui", "./rest-api/swaggerui/")

	//r.RunTLS(":443", "resources/sec/server.crt", "resources/sec/server.key")

	/////////////////////////////////////////////////////////////////
	// start server: 8333 (default)

	srv := &http.Server{
		Addr:    ":" + strconv.Itoa(cfg.Config.E2COPort), //":8080",
		Handler: r,
	}

	// Initializing the server in a goroutine so that it won't block the graceful shutdown handling below
	log.Info("..............................................................................")
	go func() {
		// HTTPS
		/*
		if err := srv.ListenAndServeTLS("resources/sec/server.crt", "resources/sec/server.key"); err != nil && errors.Is(err, http.ErrServerClosed) {
			log.Error(pathLOG + "[InitializeRESTAPIv2] ListenAndServeTLS Error: ", err)
		}
		*/
		///*
		if err := srv.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
			log.Error(pathLOG + "[InitializeRESTAPIv2] ListenAndServe Error: ", err)
		}
		//*/
	}()

	/////////////////////////////////////////////////////////////////
	// stop server:

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 5 seconds.
	quit := make(chan os.Signal)
	// kill (no param) default send syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-quit
	
	log.Info("................................................................................")

	// close DB
	db.CloseConnection()
	
	log.Info(pathLOG + "[InitializeRESTAPIv2] Shutting down server  (v2) ...")

	// The context is used to inform the server it has 5 seconds to finish the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal(err)
	}

	log.Info(pathLOG + "[InitializeRESTAPIv2] Terminated")
	log.Info("................................................................................")
}

///////////////////////////////////////////////////////////////////////////////
// API METHODS:

/*
GetVersion Get version
*/
func GetVersionV2(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [GetVersion] Getting E2CO version ... ########")

	c.JSON(200, gin.H{
		"Resp":        "ok",
		"Method":      "GetVersion",
		"E2COVersion": cfg.Config.E2COVersion})
}

/*
GetConfig Get all config parameters
*/
func GetConfigV2(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [GetConfig] Getting E2CO configuratoin ... ########")

	respConfig := cfg.Config
	respConfig.Orchestrators = make([]cfg.Orchestrator, 0)

	c.JSON(200, gin.H{
		"Resp":        "ok",
		"Method":      "GetConfig",
		"Message":     "Config found",
		"E2COVersion": cfg.Config.E2COVersion,
		"Object":      respConfig})
}

// Orchestrators:

/*
GetAllOrchestrators Get all orchestrators
*/
func GetAllOrchestratorsV2(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [GetAllOrchestrators] Getting orchestrators managed by E2CO ... ########")

	orchs, err := db.ReadAllOrchestrators() // get orchestrators: ([]structs.E2cOrchestrator, error)
	if err != nil {
		ResponseError(c, "GetAllOrchestrators", "Error getting list of orchestrators: "+err.Error())
	} else {
		c.JSON(200, gin.H{
			"Resp":        "ok",
			"Method":      "GetAllOrchestrators",
			"Message":     "Orchestrators found: " + strconv.Itoa(len(orchs)),
			"E2COVersion": cfg.Config.E2COVersion,
			"List":        orchs})
	}
}

/*
GetAllOrchestratorsWithApps Get all orchestrators and apps
*/
func GetAllOrchestratorsWithAppsV2(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [GetAllOrchestratorsWithApps] Getting orchestrators (apps) managed by E2CO ... ########")

	orchs, err := db.ReadAllOrchestratorsApps() // get orchestrators: ([]structs.E2cOrchestratorApps, error)
	if err != nil {
		ResponseError(c, "GetAllOrchestratorsWithApps", "Error getting list of orchestrators: "+err.Error())
	} else {
		_, err = db.ReadAllApplications() // get apps: ([]structs.E2coTaskExtended, error)
		if err != nil {
			ResponseError(c, "GetAllOrchestratorsWithApps", "Error getting list of apps: "+err.Error())
		} else {
			var orchsList []structs.E2cOrchestratorApps

			// TODO fix
			for _, orch := range orchs {
				/*for _, app := range apps {
					if app.IDE2cOrchestrator == orch.ID {
						log.Info(pathLOG + "[GetAllOrchestratorsWithApps] App [" + app.ID + "] added ...")
						orch.Apps = append(orch.Apps, app)
					}
				}*/

				log.Info(pathLOG + "[GetAllOrchestratorsWithApps] Orch [" + orch.ID + "] added ...")
				orchsList = append(orchsList, orch)
			}

			c.JSON(200, gin.H{
				"Resp":        "ok",
				"Method":      "GetAllOrchestratorsWithApps",
				"Message":     "Orchestrators found: " + strconv.Itoa(len(orchs)),
				"E2COVersion": cfg.Config.E2COVersion,
				"List":        orchsList})
		}
	}
}

/*
AddNewOrchestrator adds a new orchestrator (to be managed) to the system
*/
func AddNewOrchestratorV2(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [AddNewOrchestrator] Adding new orchestrator ... ########")

	orch, err := parsers.ParseE2cOrchestratorStructV2(c) // parse json
	if err != nil {
		ResponseError(c, "AddNewOrchestrator", "Error parsing JSON to E2cOrchestrator struct: "+err.Error())
	} else {
		orchRes, err := orchestrator.AddNewOrchestrator(orch) // response: (structs.E2cOrchestrator, error)
		if err != nil {
			ResponseError(c, "AddNewOrchestrator", err.Error())
		} else {
			c.JSON(200, gin.H{
				"Resp":        "ok",
				"Method":      "AddNewOrchestrator",
				"Message":     "Orchestrator added",
				"E2COVersion": cfg.Config.E2COVersion,
				"Object":        orchRes})
		}
	}
}

/*
UpdateOrchestrator Updates an orchestrator
*/
func UpdateOrchestratorV2(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [UpdateOrchestrator] Updating orchestrator ... ########")

	id := c.Param("id")
	log.Debug(pathLOG + "[UpdateOrchestrator] id=" + id)

	orch, err := parsers.ParseE2cOrchestratorStructV2(c) // parse json / body
	if err != nil {
		ResponseError(c, "UpdateOrchestrator", "Error parsing JSON to E2cOrchestrator struct: "+err.Error())
	}

	orchRes, err := orchestrator.UpdateOrchestrator(id, orch) // response: (structs.E2cOrchestrator, error)
	if err != nil {
		ResponseError(c, "UpdateOrchestrator", err.Error())
	} else {
		c.JSON(200, gin.H{
			"Resp":        	"ok",
			"Method":      	"UpdateOrchestrator",
			"Message":      "Orchestrator updated",
			"E2COVersion": 	cfg.Config.E2COVersion,
			"Object":       orchRes})
	}
}

/*
RemoveOrchestrator Removes a orchestrator
*/
func RemoveOrchestratorV2(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [RemoveOrchestrator] Deleting orchestrator ... ########")

	id := c.Param("id")
	log.Debug(pathLOG + "[RemoveOrchestrator] id=" + id)

	err := orchestrator.RemoveOrchestrator(id) // response: (error)
	if err != nil {
		ResponseError(c, "RemoveOrchestrator", err.Error())
	} else {
		c.JSON(200, gin.H{
			"Resp":        	"ok",
			"Method":      	"RemoveOrchestrator",
			"Message":      "Orchestrator deleted",
			"E2COVersion": 	cfg.Config.E2COVersion})
	}
}

/*
GetOrchestrator Get an orchestrators
*/
func GetOrchestratorV2(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [GetOrchestrator] Getting orchestrator ... ########")

	id := c.Param("id")
	log.Debug(pathLOG + "[GetOrchestrator] id=" + id)

	orch, err := db.ReadOrchestrator(id) // get orchestrator: ([]structs.E2cOrchestrator, error)

	if err != nil {
		ResponseError(c, "GetOrchestrator", "Error getting orchestrator: "+err.Error())
	} else {
		c.JSON(200, gin.H{
			"Resp":        	"ok",
			"Method":      	"GetOrchestrator",
			"Message":      "Orchestrator found:",
			"E2COVersion": 	cfg.Config.E2COVersion,
			"Object":       orch})
	}
}

// Users:

/*
GetUsers Get all users
*/
func GetUsers(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [GetUsers] Getting E2CO users ... ########")

	users, err := db.GetUsers() // get users
	if err != nil {
		ResponseError(c, "GetAllApps", "Error getting list of users: "+err.Error())
	} else {
		c.JSON(200, gin.H{
			"Resp":        "ok",
			"Method":      "GetUsers",
			"Message":     "Users found: " + strconv.Itoa(len(users)),
			"E2COVersion": cfg.Config.E2COVersion,
			"List":      users})
	}
}

/*
GetUser Get a user
*/
func GetUser(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [GetUser] Getting E2CO user ... ########")

	id := c.Param("id")
	log.Debug(pathLOG + "[GetUser] id=" + id)

	user, err := db.GetUser(id) // get users
	if err != nil {
		ResponseError(c, "GetUser", "Error getting requested user: "+err.Error())
	} else {
		c.JSON(200, gin.H{
			"Resp":        "ok",
			"Method":      "GetUser",
			"Message":     "User found",
			"E2COVersion": cfg.Config.E2COVersion,
			"Object":      user})
	}
}

// Applications:

/*
GetAllApps Get all applicatoins
*/
func GetAllAppsV2(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [GetAllApps] Getting applications managed by E2CO ... ########")

	apps, err := db.ReadAllApplications() // get apps
	if err != nil {
		ResponseError(c, "GetAllApps", "Error getting list of applications: "+err.Error())
	} else {
		c.JSON(200, gin.H{
			"Resp":        "ok",
			"Method":      "GetAllApps",
			"Message":     "Apps found: " + strconv.Itoa(len(apps)),
			"E2COVersion": cfg.Config.E2COVersion,
			"List":      apps})
	}
}

/*
DeployTask Deploy a task
*/
func DeployTaskV2(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [DeployTask] Deploying new application ... ########")

	e2coApp, err := parsers.ParseE2coApplicationStructV2(c)	// parse json
	if err != nil {
		ResponseError(c, "DeployTask", err.Error())
	} else {
		err := orchestrator.DeployE2coApplication(&e2coApp, true) // response: (structs.E2cOrchestrator, error)
		if err != nil {
			ResponseError(c, "DeployTask", err.Error())
		} else {
			c.JSON(200, gin.H{
				"Resp":        "ok",
				"Method":      "DeployTask",
				"Message":     "Application is being deployed. ID: " + e2coApp.ID,
				"Application": "GET /api/v1/apps/" + e2coApp.ID,
				"E2COVersion": cfg.Config.E2COVersion,
				"ID": 		   e2coApp.ID})
		}
	}
}

/*
GetE2coApplication Get an application
*/
func GetE2coApplicationV2(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [GetE2coApplication] Getting application [E2coApplication] ... ########")

	id := c.Param("id")
	log.Debug(pathLOG + "[GetE2coApplication] id=" + id)

	app, err := db.ReadE2coApplication(id) // get app: db.ReadApplicationValue(params["id"]) // ([]structs.E2coTaskExtended, error)

	if err != nil {
		ResponseError(c, "GetE2coApplication", "Error getting application: "+err.Error())
	} else {
		respGetTask, _ := orchestrator.GetE2coApplication(id)
		c.JSON(200, gin.H{
			"Resp":        	"ok",
			"Method":      	"GetE2coApplication",
			"Message":     	"Application found",
			"E2COVersion": 	cfg.Config.E2COVersion,
			"Object":      	app,
			"ExtraContent": respGetTask})
	}
}

/*
RemoveTask Deploy a task
*/
func RemoveTaskV2(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [RemoveTask] Removing task ... ########")

	id := c.Param("id")
	log.Debug(pathLOG + "[RemoveTask] id=" + id)

	err := orchestrator.RemoveE2coApplication(id) // response: (error)
	if err != nil {
		ResponseError(c, "RemoveTask", err.Error())
	} else {
		c.JSON(200, gin.H{
			"Resp":        	"ok",
			"Method":      	"RemoveTask",
			"Message":     	"Application removed",
			"E2COVersion": 	cfg.Config.E2COVersion})
	}
}

/*
UpdateE2coApplication Update E2coApp
*/
func UpdateE2coApplication(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [UpdateE2coApplication] Updating application ... ########")

	id := c.Param("id")
	log.Debug(pathLOG + "[UpdateE2coApplication] Updating application [" + id + "] ...")

	taskOperation, err := parsers.ParseTaskOperationStructV2(c)	// parse json
	if err != nil {
		ResponseError(c, "UpdateE2coApplication", err.Error())
	} else {
		// TODO check app ==> 'id'

		app, err := db.ReadE2coApplication(id) // get app: (*structs.E2coTaskExtended, error)
		if err != nil {
			ResponseError(c, "UpdateE2coApplication", "Error getting application: "+err.Error())
		} else {
			err = orchestrator.UpdateE2coApplication(&app, taskOperation) // UpdateE2coApplication(app *structs.E2coApplication, operation structs.TaskOperation) error
			if err != nil {
				ResponseError(c, "UpdateE2coApplication", err.Error())
			} else {
				c.JSON(200, gin.H{
					"Resp":        	"ok",
					"Method":      	"UpdateE2coApplication",
					"Message":     	"Application updated",
					"E2COVersion": 	cfg.Config.E2COVersion,
					"Object":       app})
			}
		}
	}
}

/*
UpdateE2coSubApp Update E2coApp
*/
func UpdateE2coSubApp(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [UpdateE2coSubApp] Updating subapplication ... ########")

	id := c.Param("id")
	idapp := c.Param("idapp")
	log.Debug(pathLOG + "[UpdateE2coSubApp] Updating subapplication [" + id + ", " + idapp +  "] ...")

	taskOperation, err := parsers.ParseTaskOperationStructV2(c)	// parse json
	if err != nil {
		ResponseError(c, "UpdateE2coSubApp", err.Error())
	} else {
		// TODO check app ==> 'id'

		app, err := db.ReadE2coSubApp(idapp) // get app: (*structs.E2coTaskExtended, error)
		if err != nil {
			ResponseError(c, "UpdateE2coSubApp", "Error getting subapplication: "+err.Error())
		} else {
			err = orchestrator.UpdateE2coSubApp(&app, taskOperation) // (task *structs.E2coTaskExtended, operation structs.TaskOperation) => (structs.E2coTaskExtended, error)
			if err != nil {
				ResponseError(c, "UpdateE2coSubApp", err.Error())
			} else {
				c.JSON(200, gin.H{
					"Resp":        	"ok",
					"Method":      	"UpdateE2coSubApp",
					"Message":     	"Subapplication updated",
					"E2COVersion": 	cfg.Config.E2COVersion,
					"Object":       app})
			}
		}
	}
}

/*
GetE2coApp Get an application
*/
func GetE2coAppV2(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [GetE2coApp] Getting subapplication [E2coApp] ... ########")

	id := c.Param("id")
	idapp := c.Param("idapp")
	log.Debug(pathLOG + "[GetE2coApp] Getting subapplication [" + id + ", " + idapp +  "] ...")

	app, err := db.ReadE2coSubApp(idapp) // get app

	if err != nil {
		ResponseError(c, "GetE2coApp", "Error getting subapplication: "+err.Error())
	} else {
		respGetTask, _ := orchestrator.GetE2coApp(idapp)
		c.JSON(200, gin.H{
			"Resp":        	"ok",
			"Method":      	"GetE2coApp",
			"Message":     	"Subapplication found",
			"E2COVersion": 	cfg.Config.E2COVersion,
			"Object":       app,
			"ExtraContent": respGetTask})
	}
}

/*
GetAllE2coApps Get all sub applicatoins
*/
func GetAllE2coAppsV2(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [GetAllE2coApps] Getting sub applications managed by E2CO ... ########")

	// get apps
	apps, err := db.ReadAllSubApplications()

	// response
	if err != nil {
		ResponseError(c, "GetAllE2coApps", "Error getting list of sub applications: "+err.Error())
	} else {
		c.JSON(200, gin.H{
			"Resp":        	"ok",
			"Method":      	"GetAllE2coApps",
			"Message":     	"Sub apps found: " + strconv.Itoa(len(apps)),
			"E2COVersion": 	cfg.Config.E2COVersion,
			"List":       	apps})
	}
}

// Connectors: connections with external applications, i.e. SLALite

/*
ProcessSLAViolation processes SLA violations
func ProcessSLAViolation(c *gin.Context) {
	log.Info("E2CO > Rest-API ######## [ProcessSLAViolation] Processing violation ... ########")

	id := c.Param("id")
	guarantee := c.Param("guarantee")
	log.Debug(pathLOG + "[ProcessSLAViolation] id=" + id + ", guarantee=" + guarantee)

	v, err := parseViolationInfoStructV2(c)	// parse json
	if err != nil {
		ResponseError(c, "ProcessSLAViolation", "Error parsing violation info: "+err.Error())
	} else {
		resStr, err := dss.ProcessSLAViolations(v)
		if err != nil {
			ResponseError(c, "ProcessSLAViolation", err.Error())
		} else {
			c.JSON(200, gin.H{
				"Resp":        	"ok",
				"Method":      	"ProcessSLAViolation",
				"Message":     	resStr,
				"E2COVersion": 	cfg.Config.E2COVersion,
				"Object":      	v})
		}
	}
}
*/