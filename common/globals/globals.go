//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 26 Mar 2021
//
// @author: ATOS
//
package globals


// APPLICATION STATUS 
const NOT_DEFINED string = "NOT_DEFINED"
const NOT_DEFINED_INT int = -1


// APPLICATION STATUS 
const DEPLOYMENT_UNDEFINED string = "UNDEFINED"
const DEPLOYMENT_ERROR string = "ERROR"
const DEPLOYMENT_FAILED string = "FAILED"
const DEPLOYMENT_LAUNCHING string = "LAUNCHING"
const DEPLOYMENT_DEPLOYING string = "DEPLOYING"
const DEPLOYMENT_DEPLOYED string = "DEPLOYED"
const DEPLOYMENT_NOT_READY string = "NOT_READY"
const DEPLOYMENT_TERMINATING string = "TERMINATING"
const DEPLOYMENT_SCALING string = "SCALING"
const DEPLOYMENT_STORED_PAUSED string = "STORED/PAUSED"


// OPERATIONS WITH APPLICATIONS
const MIGRATION string = "migration"
const SCALE_OUT string = "scale_out"
const SCALE_IN string = "scale_in"
const SCALE_UP string = "scale_up"
const SCALE_DOWN string = "scale_down"


// SUPPORTED ORCHESTRATORS
const MICROK8S string = "microk8s"
const K3S string = "k3s"
const KUBERNETES string = "k8s"
const OPENSHIFT string = "openshift"
const DOCKER string = "docker"
const DOCKERSWARM string = "swarm"
const DOCKERCOMPOSE string = "compose"
const VM string = "custom"
const SOE string = "soe"
const CUSTOM string = "custom"


// ORCHESTRATORS
const ORCHESTRATOR_DEFAULT_NAMESPACE string = "default"


// APPLICATION TYPES
const APP_DEFAULT string = "default"
const APP_PERSISTANCE string = "persistance"
const APP_JOB string = "job"
const APP_SOE string = "soe"
const APP_CUSTOM string = "custom"


// LOCATION / REPLICAS TYPES
const V_GLOBAL string = "global"
const V_SPECIFIC string = "specific"


// PORTS MANAGEMENT
const PORT_INI int = 32500


// APPLICATIONS PLACEHOLDERS / VALUES
const PLACEHOLDER_PREFIX string = "PLACEHOLDER_"
const PLACEHOLDER_INT_VALUE int = -10
const PLACEHOLDER_STRING_VALUE string = "-10"


// KAFKA
const KAFKA_DEFAULT string = "default"
const KAFKA_SSL string = "ssl"


// DB
const DB_DEFAULT string = "default"
const DB_MONGO string = "mongodb"


// CONFIGURATION DEFAULT VALUES
const E2CO_SLALiteEndPoint string = "http://localhost:8090"
const E2CO_PrometheusPushgatewayEndPoint string = "http://localhost:9091"
const E2CO_PrometheusEndPoint string = "http://localhost:9090"

const E2CO_DB string = DB_DEFAULT
const E2CO_DB_CONNECTION string = ""
const E2CO_DB_REPOSITORY string = ""

const E2CO_WARMING_TIME	int = 120
const E2CO_CONFIGSVC_ENDPOINT string = "http://localhost:8000/api/"
const E2CO_CONFIGSVC_CREDENTIALS string = "url:http://localhost:8000/api/authenticate,username:api,password:apiH2020"
const E2CO_KAFKA_CLIENT	string = KAFKA_DEFAULT
const E2CO_KAFKA_ENDPOINT string = "192.168.1.23:9095"
const E2CO_KAFKA_VIOLATION_TOPIC string = "sla_violation"
const E2CO_KAFKA_CONFIG_TOPIC string = "configuration"
const E2CO_KAFKA_DEPLOY_TOPIC string = "deployment"
const E2CO_KAFKA_SOE_TOPIC string = "soe_provisioning"
const E2CO_KAFKA_RESPONSE_CONFIG_TOPIC string = "configuration_feedback"
const E2CO_KAFKA_RESPONSE_DEPLOY_TOPIC string = "deployment_feedback"
const E2CO_KAFKA_KEYSTORE_PASSWORD string = "password"
const E2CO_KAFKA_KEY_PASSWORD string = "password"
const E2CO_KAFKA_TRUSTSTORE_PASSWORD string = "password"
const E2CO_KAFKA_KEYSTORE_LOCATION string = "$HOME/tmp/kafka/keystore/localdev/kafka.client.keystore.p12"
const E2CO_KAFKA_TRUSTSTORE_LOCATION string = "$HOME/tmp/kafka/truststore/localdev/kafka.client.truststore.pem"