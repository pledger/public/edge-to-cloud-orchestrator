//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package cfg

import (
	"os"
	"strconv"

	"atos.pledger/e2c-orchestrator/common/globals"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
)

// path used in logs
const pathLOG string = "E2CO > Common > Cfg "

/*
Config global configuration
*/
var Config Configuration

/*
Orchestrators global variable // TODO store in internal database
*/
var Orchestrators []Orchestrator

/*
Configuration default configuration elements
configuration struct: is filled with values from json file and environement values
*/
type Configuration struct {
	E2COVersion   						string
	E2COPort      						int			// Port where REST API services are exposed
	E2COSLALiteEndPoint              	string 
	E2COPrometheusPushgatewayEndPoint 	string 
	E2COPrometheusEndPoint            	string 
	DefaultRESTAPIEndPoint 				string 
	// DB
	Database							string
	DatabaseConnection					string
	DatabaseRepository					string
	// Orchestrators, clusters
	Orchestrators 						[]Orchestrator
	// properties values used when no environment variables found
	E2CO_WARMING_TIME					int 	// 120
	E2CO_CONFIGSVC_ENDPOINT				string 	// "http://localhost:8000/api/"
	E2CO_CONFIGSVC_CREDENTIALS			string 	// "url:http://localhost:8000/api/authenticate,username:api,password:apiH2020"
	// Kafka
	E2CO_KAFKA_CLIENT					string 	// kafka2
	E2CO_KAFKA_ENDPOINT					string 	// 192.168.1.23:9095
	E2CO_KAFKA_VIOLATION_TOPIC			string 	// sla_violation
	E2CO_KAFKA_CONFIG_TOPIC				string 	// configuration
	E2CO_KAFKA_DEPLOY_TOPIC				string 	// deployment
	E2CO_KAFKA_SOE_TOPIC 				string 	// soe provisioning
	E2CO_KAFKA_RESPONSE_CONFIG_TOPIC    string  // responses
	E2CO_KAFKA_RESPONSE_DEPLOY_TOPIC    string  // responses
	E2CO_KAFKA_KEYSTORE_PASSWORD		string 	// password
	E2CO_KAFKA_KEY_PASSWORD				string 	// password
	E2CO_KAFKA_TRUSTSTORE_PASSWORD		string 	// password
	E2CO_KAFKA_KEYSTORE_LOCATION		string 	// $HOME/tmp/kafka/keystore/localdev/kafka.client.keystore.p12
	E2CO_KAFKA_TRUSTSTORE_LOCATION		string 	// $HOME/tmp/kafka/truststore/localdev/kafka.client.truststore.pem
}

// CONSTANTS


/*
Orchestrator cluster element
*/
type Orchestrator struct {
	ID              string
	E2cOrchestrator structs.E2cOrchestrator
}

/*
ResponseConfig Config Response
*/
type ResponseConfig struct {
	Resp    string        `json:"resp,omitempty"`
	Method  string        `json:"method,omitempty"`
	Message string        `json:"message,omitempty"`
	Content Configuration `json:"content,omitempty"`
}

// setEnvValue set string environment value
func setEnvValue(val *string, name string, global string) {
	if os.Getenv(name) != "" {
		log.Debug(pathLOG + "Updating '" + name + "' value [from environment variables] => '" + os.Getenv(name) + "'")
		*val = os.Getenv(name)
	} else if len(*val) == 0 {
		log.Debug(pathLOG + "Updating '" + name + "' value [from default variables / constants] => '" + global + "'")
		*val = global
	} else {
		log.Debug(pathLOG + "Using '" + name + "' value [from configuration file] => '" + *val + "'")
	}
}

// setEnvValueInt set int environment value
func setEnvValueInt(val *int, name string, global string) {
	if os.Getenv(name) != "" {
		log.Debug(pathLOG + "Setting " + name + " value [from environment variables] => '" + os.Getenv(name) + "'")
		*val, _ = strconv.Atoi(os.Getenv(name))
	} else if *val == 0 {
		log.Debug(pathLOG + "Updating '" + name + "' value [from default variables / constants] => '" + global + "'")
		*val, _ = strconv.Atoi(global)
	} else {
		log.Debug(pathLOG + "Using '" + name + "' value [from configuration file] => '" + strconv.Itoa(*val) + "'")
	}
}

/*
InitConfig Initialize configuration values
*/
func InitConfig(cfg *Configuration) {
	log.Debug(pathLOG + "Checking configuration values from defined environment variables ...")

	// 
	setEnvValue(&cfg.E2COSLALiteEndPoint, "E2CO_SLA_ENDPOINT", globals.E2CO_SLALiteEndPoint)
	setEnvValueInt(&cfg.E2COPort, "E2CO_Port", strconv.Itoa(globals.PORT_INI))
	setEnvValue(&cfg.E2COPrometheusPushgatewayEndPoint, "E2CO_PROMETHEUS_PUSHGATEWAY", globals.E2CO_PrometheusPushgatewayEndPoint)
	setEnvValue(&cfg.E2COPrometheusEndPoint, "E2CO_PROMETHEUS", globals.E2CO_PrometheusEndPoint)
	
	// Database
	setEnvValue(&cfg.Database, "E2CO_DB", globals.E2CO_DB)
	setEnvValue(&cfg.DatabaseConnection, "E2CO_DB_CONNECTION", globals.E2CO_DB_CONNECTION)
	setEnvValue(&cfg.DatabaseRepository, "E2CO_DB_REPOSITORY", globals.E2CO_DB_REPOSITORY)

	// CONFIG SERVICE - DSS
	setEnvValueInt(&cfg.E2CO_WARMING_TIME, "E2CO_WARMING_TIME", strconv.Itoa(globals.E2CO_WARMING_TIME))
	setEnvValue(&cfg.E2CO_CONFIGSVC_ENDPOINT, "E2CO_CONFIGSVC_ENDPOINT", globals.E2CO_CONFIGSVC_ENDPOINT)
	setEnvValue(&cfg.E2CO_CONFIGSVC_CREDENTIALS, "E2CO_CONFIGSVC_CREDENTIALS", globals.E2CO_CONFIGSVC_CREDENTIALS)

	// KAFKA
	setEnvValue(&cfg.E2CO_KAFKA_CLIENT, "E2CO_KAFKA_CLIENT", globals.E2CO_KAFKA_CLIENT)
	setEnvValue(&cfg.E2CO_KAFKA_ENDPOINT, "E2CO_KAFKA_ENDPOINT", globals.E2CO_KAFKA_ENDPOINT)
	setEnvValue(&cfg.E2CO_KAFKA_VIOLATION_TOPIC, "E2CO_KAFKA_VIOLATION_TOPIC", globals.E2CO_KAFKA_VIOLATION_TOPIC)
	setEnvValue(&cfg.E2CO_KAFKA_CONFIG_TOPIC, "E2CO_KAFKA_CONFIG_TOPIC", globals.E2CO_KAFKA_CONFIG_TOPIC)
	setEnvValue(&cfg.E2CO_KAFKA_DEPLOY_TOPIC, "E2CO_KAFKA_DEPLOY_TOPIC", globals.E2CO_KAFKA_DEPLOY_TOPIC)
	setEnvValue(&cfg.E2CO_KAFKA_SOE_TOPIC, "E2CO_KAFKA_SOE_TOPIC", globals.E2CO_KAFKA_SOE_TOPIC)
	setEnvValue(&cfg.E2CO_KAFKA_RESPONSE_CONFIG_TOPIC, "E2CO_KAFKA_RESPONSE_CONFIG_TOPIC", globals.E2CO_KAFKA_RESPONSE_CONFIG_TOPIC)
	setEnvValue(&cfg.E2CO_KAFKA_RESPONSE_DEPLOY_TOPIC, "E2CO_KAFKA_RESPONSE_DEPLOY_TOPIC", globals.E2CO_KAFKA_RESPONSE_DEPLOY_TOPIC)
	setEnvValue(&cfg.E2CO_KAFKA_KEYSTORE_PASSWORD, "E2CO_KAFKA_KEYSTORE_PASSWORD", globals.E2CO_KAFKA_KEYSTORE_PASSWORD)
	setEnvValue(&cfg.E2CO_KAFKA_KEY_PASSWORD, "E2CO_KAFKA_KEY_PASSWORD", globals.E2CO_KAFKA_KEY_PASSWORD)
	setEnvValue(&cfg.E2CO_KAFKA_TRUSTSTORE_PASSWORD, "E2CO_KAFKA_TRUSTSTORE_PASSWORD", globals.E2CO_KAFKA_TRUSTSTORE_PASSWORD)
	setEnvValue(&cfg.E2CO_KAFKA_KEYSTORE_LOCATION, "E2CO_KAFKA_KEYSTORE_LOCATION", globals.E2CO_KAFKA_KEYSTORE_LOCATION)
	setEnvValue(&cfg.E2CO_KAFKA_TRUSTSTORE_LOCATION, "E2CO_KAFKA_TRUSTSTORE_LOCATION", globals.E2CO_KAFKA_TRUSTSTORE_LOCATION)
}
