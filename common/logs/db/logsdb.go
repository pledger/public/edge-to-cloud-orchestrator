//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package logs

import (
	"fmt"

	"atos.pledger/e2c-orchestrator/data/db"
	log "atos.pledger/e2c-orchestrator/common/logs"
)

// save to DB
var STORE_LOGS bool

// init
func init() {
	STORE_LOGS = false
}

// storeToDB: store logs to DB
func storeToDB(a ...interface{}) {
	if STORE_LOGS {
		s := fmt.Sprint(a...)
		// store in DB
		db.SetLog(s)
	}
}

///////////////////////////////////////////////////////////////////////////////

/*

 */
func Fatal(e error) {
	log.Fatal(e)
}

/*

 */
func Trace(args ...interface{}) {
	log.Trace(args...)
}

/*

 */
func Debug(args ...interface{}) {
	log.Debug(args...)
}

/*

 */
func Info(args ...interface{}) {
	log.Info(args...)
	//storeToDB(args...)
}

/*

 */
func Warn(args ...interface{}) {
	log.Warn(args...)
	storeToDB(args...)
}

/*

 */
func Error(args ...interface{}) {
	log.Error(args...)
	storeToDB(args...)
}

/*

 */
func Panic(m string) {
	log.Panic(m)
	storeToDB(m)
}