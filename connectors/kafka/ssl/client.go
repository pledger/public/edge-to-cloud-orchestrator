//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 12 Jan 2021
//
// @author: ATOS
//
package ssl

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"strings"
	"time"
	"net"
	"strconv"

	log "atos.pledger/e2c-orchestrator/common/logs"
	cfg "atos.pledger/e2c-orchestrator/common/cfg"

	"golang.org/x/crypto/pkcs12"
	"github.com/segmentio/kafka-go"
)

// path used in logs
const pathLOG string = "E2CO > Connectors > Kafka > SSL "

/*
Adapter Adapter
*/
type Adapter struct{}

// Kafka endpoint server
var KAFKA_ENDPOINT = "localhost:9093"

// Kafka SSL config
var KAFKA_KEYSTORE_LOCATION = "/var/kafka_keystore/kafka.client.keystore.p12"
var KAFKA_KEYSTORE_PASSWORD = ""
var KAFKA_TRUSTSTORE_LOCATION = "/var/kafka_truststore/kafka.client.truststore.pem"

// Connection vars
var dialer *kafka.Dialer = nil

var reader map[string]*kafka.Reader
var writer map[string]*kafka.Writer

var wcounter int

/*
NewClient 
*/
func (a Adapter) NewClient() {
	log.Info(pathLOG + "[NewClient] Initializating Kafka client ...")

	KAFKA_ENDPOINT = cfg.Config.E2CO_KAFKA_ENDPOINT 
	KAFKA_KEYSTORE_LOCATION = cfg.Config.E2CO_KAFKA_KEYSTORE_LOCATION 
	KAFKA_KEYSTORE_PASSWORD = cfg.Config.E2CO_KAFKA_KEYSTORE_PASSWORD 
	KAFKA_TRUSTSTORE_LOCATION = cfg.Config.E2CO_KAFKA_TRUSTSTORE_LOCATION 

	log.Info(pathLOG + "[NewClient] Ping to Kafka ...")
	timeout := 1 * time.Second

	if strings.Index(KAFKA_ENDPOINT, ",") > 0 {
		endpoints := strings.Split(KAFKA_ENDPOINT, ",")

		for _, e := range endpoints {
			_, err := net.DialTimeout("tcp",e, timeout)
			if err != nil {
				log.Error(pathLOG + "[NewClient] Site [" + e + "] unreachable, error: ", err)
			} else {
				log.Info(pathLOG + "[NewClient] Service listening in " + e)
			}
		}
	} else {
		_, err := net.DialTimeout("tcp",KAFKA_ENDPOINT, timeout)
		if err != nil {
			log.Error(pathLOG + "[NewClient] Site [" + KAFKA_ENDPOINT + "] unreachable, error: ", err)
		} else {
			log.Info(pathLOG + "[NewClient] Service listening in " + KAFKA_ENDPOINT)
		}
	}

	dialer = getSSLDialer()
	log.Info(pathLOG + "[NewClient] Kafka Endpoint: " + KAFKA_ENDPOINT)

	reader = make(map[string]*kafka.Reader)
	writer = make(map[string]*kafka.Writer)

	wcounter = 0
}

/*
CheckForMessages
*/
func (a Adapter) CheckForMessages(topic string) (string, error) {
	return consumeSSL(topic)
}

/*
WriteMessage
*/
func (a Adapter) WriteMessage(topic string, message string) (string, error) {
	return produceSSL(topic, message)
}

/*
consume Read from Kafka server
*/
func consumeSSL(topic string) (string, error) {
	log.Info(pathLOG + "[consumeSSL] Consume [topic=" + topic + "] ...")
	var msg = ""
	if dialer == nil {
		dialer = getSSLDialer()
	}
	KafkaEndpointList := strings.Split(KAFKA_ENDPOINT, ",")
	if reader[topic] == nil {
		myreader := kafka.NewReader(kafka.ReaderConfig{
			Brokers:   KafkaEndpointList,
			Topic:     topic,
			Partition: 0,
			MinBytes:  10e3, // 10KB
			MaxBytes:  10e6, // 10MB
			Dialer:    dialer,
		})
		reader[topic] = myreader
		reader[topic].SetOffsetAt(context.Background(), time.Now())
	}

	m, err := reader[topic].ReadMessage(context.Background())
	if err != nil {
		log.Error(pathLOG+"[consumeSSL] Error reading message: ", err)
	}

	msg = string(m.Value)

	return msg, err
}

/*
produce Write to Kafka sever
*/
func produceSSL(topic string, message string) (string, error) {
	wcounter++
	log.Info(pathLOG + "[produceSSL] Produce [topic=" + topic + "] [" + strconv.Itoa(wcounter) + "] ...")

	if dialer == nil {
		dialer = getSSLDialer()
	}
	KafkaEndpointList := strings.Split(KAFKA_ENDPOINT, ",")
	if writer[topic] == nil {
		mywriter := kafka.NewWriter(kafka.WriterConfig{
			Brokers:  KafkaEndpointList,
			Topic:    topic,
			Balancer: &kafka.Hash{},
			Dialer:   dialer,
			WriteTimeout: 30 * time.Second,
			ReadTimeout: 30 * time.Second,
			MaxAttempts: 15,
		})
		writer[topic] = mywriter
	}

	errMsg := ""
	err := writer[topic].WriteMessages(context.Background(),
		kafka.Message{
			Key: []byte(strconv.Itoa(wcounter)),
			Value: []byte(message),
		},
	)
	if err != nil {
		log.Error(pathLOG+"[produceSSL] Failed to write message: ", err)
		errMsg = "send-message-error;"
	}

	if err2 := writer[topic].Close(); err2 != nil {
		log.Error(pathLOG+"[produceSSL] Failed to close writer: ", err2)
		errMsg = errMsg + "close-writer-error;"
		err = err2
	} else {
		writer[topic] = nil
	}
	return errMsg, err
}

//
func getSSLDialer() *kafka.Dialer {
	log.Info(pathLOG + "[getSSLDialer] Get SSLDialer...")
	cert := getCertFromKeyStore(KAFKA_KEYSTORE_LOCATION, KAFKA_KEYSTORE_PASSWORD)
	caCert, err := ioutil.ReadFile(KAFKA_TRUSTSTORE_LOCATION)
	if err != nil {
		log.Error(pathLOG+"[getSSLDialer] CA cert load:", err)
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)
	dialer := &kafka.Dialer{
		Timeout:   45 * time.Second,
		DualStack: true,
		TLS: &tls.Config{
			Certificates: []tls.Certificate{cert},
			RootCAs:      caCertPool,
			InsecureSkipVerify: true,
		},
	}
	return dialer
}

//
func getCertFromKeyStore(keystorage string, password string) tls.Certificate {
	log.Info(pathLOG+"[getCertFromKeyStore] Reading cert... %s\n", keystorage)
	pfxdata, err1 := ioutil.ReadFile(keystorage)
	if err1 != nil {
		log.Error(pathLOG+"[getCertFromKeyStore] Error ReadFile: ", err1)
	}
	blocks, err := pkcs12.ToPEM(pfxdata, password)

	if err != nil {
		log.Error(pathLOG+"[getCertFromKeyStore] Error pkcs12.ToPEM: ", err)
	}
	var pemData []byte
	for _, b := range blocks {
		pemData = append(pemData, pem.EncodeToMemory(b)...)
	}
	// then use PEM data for tls to construct tls certificate:
	cert, err2 := tls.X509KeyPair(pemData, pemData)
	if err2 != nil {
		log.Error(pathLOG+"[getCertFromKeyStore] Error tls.X509KeyPair: ", err2)
	}
	return cert
}
