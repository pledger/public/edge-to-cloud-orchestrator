//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 10 Sep 2021
// Updated on 10 Sep 2021
//
// @author: ATOS
//
package nosec

import (
	"context"
	"strings"
	"time"
	"net"
	"strconv"
	"errors"

	log "atos.pledger/e2c-orchestrator/common/logs"
	cfg "atos.pledger/e2c-orchestrator/common/cfg"

	"github.com/segmentio/kafka-go"
)

// path used in logs
const pathLOG string = "E2CO > Connectors > Kafka > Default "

/*
Adapter Adapter
*/
type Adapter struct{}

// Kafka endpoint server
var KAFKA_ENDPOINT = "localhost:9093"

// Connection vars
var dialer *kafka.Dialer = nil

var reader map[string]*kafka.Reader
var writer map[string]*kafka.Writer

var wcounter int

/*
NewClient 
*/
func (a Adapter) NewClient() {
	log.Info(pathLOG + "[NewClient] Initializating Kafka client ...")

	KAFKA_ENDPOINT = cfg.Config.E2CO_KAFKA_ENDPOINT //config.GetString("KAFKA_ENDPOINT")

	log.Info(pathLOG + "[NewClient] Ping to Kafka ...")
	timeout := 1 * time.Second
	_, err := net.DialTimeout("tcp",KAFKA_ENDPOINT, timeout)
	if err != nil {
		log.Error("[NewClient] Site unreachable, error: ", err)
	} else {
		log.Info(pathLOG + "[NewClient] Service listening in " + KAFKA_ENDPOINT)
	}

	dialer = getDialer()
	log.Info(pathLOG + "[NewClient] Kafka Endpoint: " + KAFKA_ENDPOINT)

	reader = make(map[string]*kafka.Reader)
	writer = make(map[string]*kafka.Writer)

	wcounter = 0
}

/*
CheckForMessages
*/
func (a Adapter) CheckForMessages(topic string) (string, error) {
	return consumeDefault(topic)
}

/*
WriteMessage
*/
func (a Adapter) WriteMessage(topic string, message string) (string, error) {
	return produceDefault(topic, message)
}

/*
consumeDefault Read from Kafka server
*/
func consumeDefault(topic string) (string, error) {
	log.Info(pathLOG + "[consumeDefault] Consume [topic=" + topic + "] ...")
	var msg = ""
	if dialer == nil {
		dialer = getDialer()
	}
	KafkaEndpointList := strings.Split(KAFKA_ENDPOINT, ",")
	if reader[topic] == nil {
		myreader := kafka.NewReader(kafka.ReaderConfig{
			Brokers:   KafkaEndpointList,
			Topic:     topic,
			Partition: 0,
			MinBytes:  10e3, // 10KB
			MaxBytes:  10e6, // 10MB
			Dialer:    dialer,
		})
		reader[topic] = myreader
		reader[topic].SetOffsetAt(context.Background(), time.Now())
	}

	m, err := reader[topic].ReadMessage(context.Background())
	if err != nil {
		log.Error(pathLOG+"[consumeDefault] Error reading message: ", err)
	}

	msg = string(m.Value)

	return msg, err
}

/*
produceDefault Write to Kafka sever
*/
func produceDefault(topic string, message string) (string, error) {
	wcounter++
	log.Info(pathLOG + "[produceDefault] Produce [topic=" + topic + "] [" + strconv.Itoa(wcounter) + "] ...")

	if dialer == nil {
		dialer = getDialer()
	}
	KafkaEndpointList := strings.Split(KAFKA_ENDPOINT, ",")

	if writer[topic] == nil {
		mywriter := kafka.NewWriter(kafka.WriterConfig{
			Brokers:  KafkaEndpointList,
			Topic:    topic,
			Balancer: &kafka.Hash{},
			Dialer:   dialer,
			WriteTimeout: 30 * time.Second,
			ReadTimeout: 30 * time.Second,
			MaxAttempts: 15,
		})

		if mywriter == nil {
			return "error", errors.New("'mywriter' is nil")
		}

		writer[topic] = mywriter
	}

	err := writer[topic].WriteMessages(context.Background(),
		kafka.Message{
			Key: []byte(strconv.Itoa(wcounter)),
			//Topic: topic,
			Value: []byte(message),
		},
	)
	if err != nil {
		log.Error(pathLOG+"[produceDefault] Failed to write messages: ", err)
	}

	if err := writer[topic].Close(); err != nil {
		log.Error(pathLOG+"[produceDefault] Failed to close writer: ", err)
	} else {
		writer[topic] = nil
	}
	return "", err
}

// getDialer
func getDialer() *kafka.Dialer {
	log.Info(pathLOG + "[getDialer] Getting Dialer...")

	dialer := &kafka.Dialer{
		Timeout:   45 * time.Second,
		DualStack: true,
	}
	return dialer
}
