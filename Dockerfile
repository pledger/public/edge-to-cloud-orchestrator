##########################################
## e2co
##########################################
FROM golang:alpine as builder

ARG BUILD_ID
LABEL stage=builder
LABEL build=$BUILD_ID

RUN apk add --no-cache git curl
RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

WORKDIR /go/src/atos.pledger/e2c-orchestrator

COPY . .

RUN go get -d -v ./...
RUN CGO_ENABLED=0 GOOS=linux go build -a -o E2CO .

##########################################
FROM alpine:3.6

WORKDIR /opt/e2co

COPY --from=builder /go/src/atos.pledger/e2c-orchestrator/E2CO .
COPY run_e2co.sh run_e2co.sh
COPY ./resources/config/config.json ./resources/config/config.json
COPY ./resources/config/config_testbed.json ./resources/config/config_testbed.json
COPY ./rest-api ./rest-api

RUN apk add --no-cache curl
RUN chmod 775 /opt/e2co/run_e2co.sh

EXPOSE 8333

ENTRYPOINT ["/opt/e2co/run_e2co.sh"]