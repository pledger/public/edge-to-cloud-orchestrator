//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 05 Aug 2021
//
// @author: ATOS
//
package eventhandler

import (
	"fmt"
	"strings"
	"time"
	"errors"

	myhttp "atos.pledger/e2c-orchestrator/common/http"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
	"atos.pledger/e2c-orchestrator/data/db"
	orchestrator "atos.pledger/e2c-orchestrator/orchestrator/apps"
	parsers "atos.pledger/e2c-orchestrator/data/parsers/kafka-dss"
	"atos.pledger/e2c-orchestrator/common/globals"
	"atos.pledger/e2c-orchestrator/common"
)

// Time from the last token generated for ConfigService API
var tokenTimeStamp time.Time
var tokenServerUrl string
var tokenServerUser string
var tokenServerPass string
var tokenCache string

// kafkaConfigEventsLoop Kafka events handler for Configuration changes (Infrastructure and apps)
func kafkaConfigEventsLoop() {
	log.Info(pathLOG + "[kafkaConfigEventsLoop] Starting to listen topic " + KAFKA_CONFIG_TOPIC + " ...")

	for {
		message, err := KafkaAdapter.CheckForMessages(KAFKA_CONFIG_TOPIC)
		if err != nil {
			log.Error(pathLOG+"[kafkaConfigEventsLoop] Error reading kafka event ...", err)
			sendError("kafkaConfigEventsLoop", "CheckForMessages", err)
			continue
		}

		// Message processing ...
		log.Info(pathLOG+">>>>>> [kafkaConfigEventsLoop] Event received from kafka <<<<<< \n", message)
		
		// Message is a JSON with information of infrastructure or app config
		// We expect something like this: {'id': 1, 'entity': 'infrastructure or app', 'operation': 'update'}
		// And we need to call ConfService REST API /api/infrastructure/{id} or (/apps/{id} and /api/service/{id})
		var eventObj interface{}

		if err = common.ParseJSONObj(message, &eventObj); err != nil {
			sendConfigError("kafkaConfigEventsLoop", "Unmarshal", err)
			continue
		}

		eventInfoMap := eventObj.(map[string]interface{})
		if eventInfoMap["id"] == nil || eventInfoMap["entity"] == nil || eventInfoMap["operation"] == nil {
			log.Error(pathLOG + "'id' / 'entity' / 'operation' not included in JSON object")
			continue
		}
		
		entityIDn := eventInfoMap["id"].(float64)
		if entityIDn == 0 {
			continue
		}
		entityID := fmt.Sprintf("%d", uint64(entityIDn))
		if entityID == "" {
			continue
		}
		entity := eventInfoMap["entity"].(string)
		if entity == "" || (entity != "infrastructure" && entity != "service") {
			log.Warn(pathLOG+"[kafkaConfigEventsLoop] Event (entity=" + entity + ") won't be processed. ")
			continue
		}
		operation := eventInfoMap["operation"].(string)
		if operation == "" {
			continue
		}

		switch entity {
			case "infrastructure": // cluster / infrastructure
				switch operation {
					case "update":
						if _, err = processInfraUpdate(entityID); err != nil {
							sendConfigError("kafkaConfigEventsLoop", "processInfraUpdate", err)
						}
					case "delete":
						if err = orchestrator.RemoveOrchestrator(entityID); err != nil {
							sendConfigError("kafkaConfigEventsLoop", "processInfraDelete", err)
						}
					default:
						continue
					}
			case "service": // app or microservice
				switch operation {
					case "update":
						if err = processAppUpdate(entityID); err != nil {
							sendConfigError("kafkaConfigEventsLoop", "processAppUpdate", err)
						}
					case "delete":
						if err = orchestrator.RemoveE2coApplication(entityID); err != nil {
							sendConfigError("kafkaConfigEventsLoop", "processAppDelete", err)
						}
					default:
						continue
					}
			default:
				continue
		}
	} // endless for
} // kafkaConfigEventsLoop()

// processAppUpdate Process App config update
func processAppUpdate(id string) error {
	log.Info(pathLOG + "[processAppUpdate] Creating / updating application ...")

	if id == "" {
		log.Warn(pathLOG + "[processAppUpdate] 'id' is empty")
		return errors.New("application 'id' is empty")
	}

	var hasAuth bool = false
	var token string
	if CONFIGSVC_CREDENTIALS != "" {
		token = checkTokenConfigSvc(CONFIGSVC_CREDENTIALS)
		hasAuth = true
	}
	
	// 1. Call ConfigService API for app info => appInfo
	log.Debug(pathLOG + "[processAppUpdate] Calling ConfigService API to get application info ...")
	code, body, err := myhttp.GetString(CONFIGSVC_ENDPOINT+"services/"+id, hasAuth, token)
	if err != nil || code != 200 {
		log.Error(pathLOG+"[processAppUpdate] Error calling ConfigService API for app info: ", err)
		return errors.New("Error calling ConfigService")
	}

	log.Debug(pathLOG + "[processAppUpdate] Update app info: " + body)

	// appInfoMap
	log.Debug(pathLOG + "[processAppUpdate] Unmarshalling JSON app info ...")
	var appInfo interface{}

	if err = common.ParseJSONObj(body, &appInfo); err != nil || appInfo == nil {
		return errors.New("Error unmarshalling JSON app info")
	}

	appInfoMap := appInfo.(map[string]interface{})

	// 2. Set E2coApplication values <= appInfo
	log.Debug(pathLOG + "[processAppUpdate] Setting E2coApplication values from app info ...")
	e2coApp, err := parsers.ToE2coApplication(id, appInfoMap)
	if err == nil {
		// 3. Call E2CO API for app info
		log.Debug(pathLOG + "[processAppUpdate] Calling E2CO API services to get app / task info ...")
		e2coAppDB, err := orchestrator.GetE2coApplication(id) // GetE2coApplication(idTask string) (structs.E2coApplication, error)

		e2coApp.DryRun = true // Store the app in DB but no deploy it!
		log.Trace(pathLOG+"[processAppUpdate] E2coApplication: \n", e2coApp)

		if err != nil { // error or app not found
			log.Info(pathLOG + "[processAppUpdate] Task / app not found. Creating new app ...")

			// New app
			if e2coApp.Locations[0].IDE2cOrchestrator == "" {
				e2coApp.Locations[0].IDE2cOrchestrator, e2coApp.Locations[0].NameSpace = getDefaultInfra()
			}
			e2coApp.Status = globals.DEPLOYMENT_STORED_PAUSED
			e2coApp.Info.Status = globals.DEPLOYMENT_STORED_PAUSED
			e2coApp.Info.Message = "Application stored/paused. ID: " + e2coApp.ID

			db.SetE2coApplication(e2coApp.ID, e2coApp) 

		} else { // found
			log.Info(pathLOG + "[processAppUpdate] Task / app found. Updating existing app ...")

			// Update app
			e2coApp.Status = e2coAppDB.Status
			e2coApp.Info.Status = e2coAppDB.Info.Status
			e2coApp.Info.Message = e2coAppDB.Info.Message

			db.UpdateE2coApplication(e2coApp.ID, e2coApp) 

		}
	} else {
		log.Error(pathLOG + "[processAppUpdate] Application / service not created: ", err)
	}

	return nil
} // processAppUpdate func

// processInfraUpdate Process Infrastructure config update
func processInfraUpdate(id string) (structs.E2cOrchestrator, error) {
	log.Info(pathLOG + "[processInfraUpdate] Creating / updating infrastructure ...")

	if id == "" {
		log.Warn(pathLOG + "[processInfraUpdate] 'id' is empty")
		return structs.E2cOrchestrator{}, errors.New("Orchestrator / Infra 'id' is empty")
	}

	var hasAuth bool = false
	var token string
	if CONFIGSVC_CREDENTIALS != "" {
		token = checkTokenConfigSvc(CONFIGSVC_CREDENTIALS)
		hasAuth = true
	}

	// Call ConfigService API for infrastructure info => infraInfo
	code, body, err := myhttp.GetString(CONFIGSVC_ENDPOINT+"infrastructures/"+id, hasAuth, token)
	if err != nil || code != 200 {
		log.Error(pathLOG+"[processInfraUpdate] Error calling ConfigService API for infrastructure info: ", err)
		return structs.E2cOrchestrator{}, errors.New("Error calling ConfigService API for infrastructure info: " + err.Error())
	}
	log.Trace(pathLOG + "[processInfraUpdate] Update Infrastructure info: " + body)

	var infraInfo interface{}

	if err = common.ParseJSONObj(body, &infraInfo); err != nil || infraInfo == nil {
		return structs.E2cOrchestrator{}, errors.New("Error unmarshalling JSON infrastructure info: " + err.Error())
	}

	infraInfoMap := infraInfo.(map[string]interface{})

	// Set Orchestrator values <= infraInfo
	e2cOrchestrator, err := parsers.ToE2cOrchestrator(id, infraInfoMap)
	if err == nil {
		// create / update infra / orchestrator
		_, err = db.ReadOrchestrator(id) // get orchestrator: ([]structs.E2cOrchestrator, error)
		if err != nil {
			// error or orchestrator not found
			return orchestrator.AddNewOrchestrator(e2cOrchestrator)
		} else {
			// found
			return orchestrator.UpdateOrchestrator(id, e2cOrchestrator)
		}
	}

	return structs.E2cOrchestrator{}, err
}

/*
checkTokenConfigSvc Get a valid token for ConfigService (token expires every 24 hours)
	CONFIGSVC_CREDENTIALS="url:http://ip:port/api/authenticate,username:xxxxx,password:xxxxx"
*/
func checkTokenConfigSvc(credentials string) string {
	var token string = ""
	if tokenServerUrl == "" {
		params := strings.SplitN(credentials, ",", 3)
		for idx := range params {
			param := strings.SplitN(params[idx], ":", 2)
			switch param[0] {
			case "url":
				tokenServerUrl = param[1]
			case "username":
				tokenServerUser = param[1]
			case "password":
				tokenServerPass = param[1]
			case "token": // Non expiration token
				token = param[1]
			}
		} // for params
	}
	if token != "" { // Non expiration token
		return token
	}
	if time.Since(tokenTimeStamp).Hours() >= 24 {
		payload := fmt.Sprintf("{\"username\":\"%s\", \"password\":\"%s\"}", tokenServerUser, tokenServerPass)
		code, body, err := myhttp.PostRawData(tokenServerUrl, false, "", payload)
		if err != nil || code != 200 {
			log.Error(pathLOG+"Error calling ConfigService API for new API token: ", err, payload)
			return token
		}
		var tokenJsonMsg map[string]string

		if err = common.ParseBytesToJSONObj(body, &tokenJsonMsg); err != nil {
			return token
		}

		token = tokenJsonMsg["id_token"]
		if token != "" {
			tokenTimeStamp = time.Now()
			tokenCache = token
		} else {
			log.Error(pathLOG+"Error getting ConfigService API new token: ", body)
		}
	} else {
		token = tokenCache
	}
	return token
}

// getDefaultInfra Get default infra for the first deployment because Config Svc does not setup it.
func getDefaultInfra() (string, string) {
	orchs, err := db.ReadAllOrchestrators() // get orchestrators: ([]structs.E2cOrchestrator, error)
	if err != nil || len(orchs) == 0 {
		log.Error(pathLOG+"[getDefaultInfra] Error getting list of orchestrators: ", err)
		return "", ""
	}

	log.Info(pathLOG+"[getDefaultInfra] Default infra: %s", orchs[0].ID)
	return orchs[0].ID, orchs[0].DefaultNamespace
}
