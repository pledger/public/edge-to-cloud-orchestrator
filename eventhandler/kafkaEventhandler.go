//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 05 Aug 2021
//
// @author: ATOS
//
package eventhandler

import (
	"strings"
	"strconv"
	"time"

	kafka "atos.pledger/e2c-orchestrator/connectors/kafka"
	kafkassl "atos.pledger/e2c-orchestrator/connectors/kafka/ssl"
	kafkadef "atos.pledger/e2c-orchestrator/connectors/kafka/nosec"
	log "atos.pledger/e2c-orchestrator/common/logs"
	cfg "atos.pledger/e2c-orchestrator/common/cfg"
	//"atos.pledger/e2c-orchestrator/common"
	"atos.pledger/e2c-orchestrator/common/globals"
)

// path used in logs
const pathLOG string = "E2CO > Eventhandler "

// Kafka Adapter
var KafkaAdapter kafka.Adapter

var KAFKA_VIOLATION_TOPIC string
var KAFKA_CONFIG_TOPIC string
var KAFKA_DEPLOY_TOPIC string
var KAFKA_SOE_TOPIC string
var KAFKA_RESPONSE_CONFIG_TOPIC string
var KAFKA_RESPONSE_DEPLOY_TOPIC string
var KAFKA_CLIENT string
var CONFIGSVC_ENDPOINT string
var CONFIGSVC_CREDENTIALS string

// KAFKA Messages Actions
var ACTION_START string = "start"
var ACTION_STOP string = "stop"
var ACTION_MIGRATE string = "placement"
var ACTION_SCALE_OUT string = "scaleout"
var ACTION_SCALE_IN string = "scalein"
var ACTION_SCALE_UP string = "scaleup"
var ACTION_SCALE_DOWN string = "scaledown"
var ACTION_REDEPLOY string = "redeploy"
var ACTION_PROVISION string = "provision"

/*
NewEventHandler new handler
*/
func NewEventHandler() {
	log.Info(pathLOG + "[NewEventHandler] Creating new event handler ...")

	KAFKA_VIOLATION_TOPIC = cfg.Config.E2CO_KAFKA_VIOLATION_TOPIC
	KAFKA_CONFIG_TOPIC = cfg.Config.E2CO_KAFKA_CONFIG_TOPIC
	KAFKA_DEPLOY_TOPIC = cfg.Config.E2CO_KAFKA_DEPLOY_TOPIC
	KAFKA_SOE_TOPIC = cfg.Config.E2CO_KAFKA_SOE_TOPIC
	KAFKA_RESPONSE_CONFIG_TOPIC = cfg.Config.E2CO_KAFKA_RESPONSE_CONFIG_TOPIC
	KAFKA_RESPONSE_DEPLOY_TOPIC = cfg.Config.E2CO_KAFKA_RESPONSE_DEPLOY_TOPIC
	KAFKA_CLIENT = strings.ToLower(cfg.Config.E2CO_KAFKA_CLIENT)
	CONFIGSVC_ENDPOINT = cfg.Config.E2CO_CONFIGSVC_ENDPOINT
	CONFIGSVC_CREDENTIALS = cfg.Config.E2CO_CONFIGSVC_CREDENTIALS

	switch KAFKA_CLIENT {
		case globals.KAFKA_SSL:
			log.Info(pathLOG + "[NewEventHandler] Using KAFKA_CLIENT = '" + globals.KAFKA_SSL + "' ...")
			KafkaAdapter = kafkassl.Adapter{}
		case globals.KAFKA_DEFAULT:
			log.Info(pathLOG + "[NewEventHandler] Using KAFKA_CLIENT = '" + globals.KAFKA_DEFAULT + "' ...")
			KafkaAdapter = kafkadef.Adapter{}
		default:
			return
		}

	KafkaAdapter.NewClient()

	// readers
	if KAFKA_VIOLATION_TOPIC != "" && KAFKA_CLIENT != "" {
		go kafkaViolationEventsLoop()
	}
	if KAFKA_CONFIG_TOPIC != "" && KAFKA_CLIENT != "" {
		go kafkaConfigEventsLoop()
	}
	if KAFKA_DEPLOY_TOPIC != "" && KAFKA_CLIENT != "" {
		go kafkaDeployEventsLoop()
	}
	if KAFKA_SOE_TOPIC != "" && KAFKA_CLIENT != "" {
		//go soeConfigEventsLoop()
	}
}

func fSendMessage(topic string, content string) {
	time.Sleep(10 * time.Second)
	for i := 0; i < 3; i++ {
    	str, err := KafkaAdapter.WriteMessage(topic, content)
		if err != nil && strings.Index(str, "send-message-error") > -1 {
			log.Info(pathLOG+"[fSendMessage] Trying to send message again [" + strconv.Itoa(i) + "] in 30 seconds ...")
			time.Sleep(30 * time.Second)
		} else {
			break
		}
	}
}

// sendError
func sendError(f string, t string, e error) {
	//go fSendMessage(KAFKA_RESPONSE_DEPLOY_TOPIC, "[" + f + "][" + t + "][" + e.Error()+"]") 
	_, err := KafkaAdapter.WriteMessage(KAFKA_RESPONSE_DEPLOY_TOPIC, "[" + f + "][" + t + "][" + e.Error()+"]")
	if err != nil {
		log.Error(pathLOG+"[sendError] Error (1) sending message to Kafka: ", err)
		log.Debug(pathLOG + "[sendError] Sending again message to Kafka (15 seconds) ...")
		
		time.Sleep(15 * time.Second)
		_, err = KafkaAdapter.WriteMessage(KAFKA_RESPONSE_DEPLOY_TOPIC, "[" + f + "][" + t + "][" + e.Error()+"]")
		if err != nil {
			log.Error(pathLOG+"[sendError] Error (2) sending message to Kafka: ", err)
		}
	}
}

// sendConfigError
func sendConfigError(f string, t string, e error) {
	//go fSendMessage(KAFKA_RESPONSE_CONFIG_TOPIC, "[" + f + "][" + t + "][" + e.Error()+"]") 
	_, err := KafkaAdapter.WriteMessage(KAFKA_RESPONSE_CONFIG_TOPIC, "[" + f + "][" + t + "][" + e.Error()+"]")
	if err != nil {
		log.Error(pathLOG+"[sendConfigError] Error (1) sending message to Kafka: ", err)
		log.Debug(pathLOG + "[sendConfigError] Sending again message to Kafka (15 seconds) ...")
		
		time.Sleep(15 * time.Second)
		_, err = KafkaAdapter.WriteMessage(KAFKA_RESPONSE_CONFIG_TOPIC, "[" + f + "][" + t + "][" + e.Error()+"]")
		if err != nil {
			log.Error(pathLOG+"[sendConfigError] Error (2) sending message to Kafka: ", err)
		}
	}
}

// sendMessage sends a message to Kafka
func sendMessage(topic string, content string) {
	if KafkaAdapter == nil {
		log.Warn(pathLOG+"[sendMessage] No Kafka adapter defined.")
	}

	//go fSendMessage(topic, content)
	log.Debug(pathLOG + "[createSLA] Sending message to Kafka ...")
	_, err := KafkaAdapter.WriteMessage(topic, content)
	if err != nil {
		log.Error(pathLOG+"[createSLA] Error (1) sending message to Kafka: ", err)
		log.Debug(pathLOG + "[createSLA] Sending again message to Kafka (15 seconds) ...")

		time.Sleep(15 * time.Second)
		_, err = KafkaAdapter.WriteMessage(topic, content)
		if err != nil {
			log.Error(pathLOG+"[createSLA] Error (2) sending message to Kafka: ", err)
		}
	}
}