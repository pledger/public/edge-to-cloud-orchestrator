//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 05 Aug 2021
//
// @author: ATOS
//
package eventhandler

import (
	"encoding/json"
	"fmt"
	"strconv"

	myhttp "atos.pledger/e2c-orchestrator/common/http"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
)

/*
	Kafka events handler for SLA violation alerts
*/
func kafkaViolationEventsLoop() {
	log.Info(pathLOG + "[kafkaViolationEventsLoop] Starting to listen topic " + KAFKA_VIOLATION_TOPIC + " ...")
	for {
		message, err := KafkaAdapter.CheckForMessages(KAFKA_VIOLATION_TOPIC)
		if err != nil {
			log.Error(pathLOG+"[kafkaViolationEventsLoop] Error reading kafka event ...", err)
			continue
		}
		// Message processing ...
		log.Info(pathLOG+">>>>>> [kafkaViolationEventsLoop] Event received from kafka <<<<<< /n", message)
		// Message is a JSON with information of agreement violation
		// Read app id from message
		var alertInfo interface{}
		err = json.Unmarshal([]byte(message), &alertInfo)
		if err != nil {
			log.Error(pathLOG+"[kafkaViolationEventsLoop] Error unmarshalling JSON alert: ", err)
			continue
		}
		alertInfoMap := alertInfo.(map[string]interface{})
		var appID string
		if alertInfoMap["appID"] != nil {
			appID = alertInfoMap["appID"].(string)
		} else if alertInfoMap["app_id"] != nil {
			appID = alertInfoMap["app_id"].(string)
		} else if alertInfoMap["service_id"] != nil {
			appID = alertInfoMap["service_id"].(string)
		} else if alertInfoMap["id"] != nil {
			appID = alertInfoMap["id"].(string)
		} else {
			continue
		}

		log.Info(pathLOG + "[kafkaViolationEventsLoop] Processing alert from app ID: " + appID)
		if appID == "" {
			continue
		}
		var guarantee string
		if alertInfoMap["guarantee"] != nil {
			guarantee = alertInfoMap["guarantee"].(string)
		}
		if alertInfoMap["guarantee_id"] != nil {
			guarantee = alertInfoMap["guarantee_id"].(string)
		}
		if guarantee == "" {
			continue
		}

		// TODO call to DB or internal methods !!!!!!!!!!!!!
		
		// Check if violation exists or ended
		//intervalName := alertInfoMap["IntervalName"].(string)
		//if intervalName == "No violation detected" {
		//	continue
		//}
		// Call E2CO API for app info
		code, body, err := myhttp.GetString("http://localhost:8333/api/v1/apps/"+appID, false, "")
		if err != nil || code != 200 {
			log.Error(pathLOG+"[kafkaViolationEventsLoop] Error calling E2CO API for app info: ", err)
			continue
		}
		log.Info(pathLOG + "App info: " + body)
		var appInfo interface{}
		err = json.Unmarshal([]byte(body), &appInfo)
		if err != nil || appInfo == nil {
			log.Error(pathLOG+"[kafkaViolationEventsLoop] Error unmarshalling JSON app info: ", err)
			continue
		}
		appInfoMap := appInfo.(map[string]interface{})
		if appInfoMap == nil || appInfoMap["object"] == nil {
			log.Error(pathLOG + "[kafkaViolationEventsLoop] Error reading app info: object is empty")
			continue
		}
		object := appInfoMap["object"].(map[string]interface{})
		if object == nil || object["qos"] == nil {
			log.Error(pathLOG + "[kafkaViolationEventsLoop] Error reading app info: qos is empty")
			continue
		}
		qos := object["qos"].([]interface{})
		if len(qos) == 0 {
			log.Error(pathLOG + "[kafkaViolationEventsLoop] Error reading app info: qos[] metrics is empty")
			continue
		}
		var actions map[string]interface{}
		for key := range qos {
			value := qos[key].(map[string]interface{})
			if value["name"] != nil && value["name"] == guarantee {
				actions = value
				break
			}
		}
		//actions = qos[0].(map[string]interface{})
		if actions == nil || actions["actions"] == nil {
			log.Error(pathLOG + "[kafkaViolationEventsLoop] Error reading app info: qos[0] actions is empty")
			continue
		}
		action := actions["actions"].([]interface{})
		if len(action) == 0 {
			log.Error(pathLOG + "[kafkaViolationEventsLoop] Error reading app info: qos[0] actions action[] is empty")
			continue
		}
		tipo := action[0].(map[string]interface{})
		if tipo == nil || tipo["type"] == nil {
			log.Error(pathLOG + "[kafkaViolationEventsLoop] Error reading app info: qos[0] actions action[0] type is empty")
			continue
		}
		if tipo["type"] == "SCALE OUT" {
			//appInfoMap := appInfo.(map[string]interface{})
			object = appInfoMap["object"].(map[string]interface{})
			replicas := object["replicas"]
			if replicas == nil {
				object["replicas"] = 1
				replicas = object["replicas"]
			}
			n, err := strconv.Atoi(fmt.Sprintf("%v", replicas))
			if err != nil {
				n = 1
			}
			var inc = 1
			if tipo["value"] != nil {
				inc, err = strconv.Atoi(tipo["value"].(string))
				if err != nil {
					inc = 1
				}
			}
			n = n + inc
			log.Info(pathLOG+"[kafkaViolationEventsLoop] Scaling app from %v to %v\n", object["replicas"], n)
			object["replicas"] = n
			// Call E2CO API for scaling
			var operacion structs.TaskOperation
			operacion.Operation = "scale_out"
			operacion.Replicas = n
			if operacion.Replicas == 0 {
				continue
			}

			// TODO call to DB or internal methods !!!!!!!!!!!!!

			code, body, err := myhttp.Put("http://localhost:8333/api/v1/apps/"+appID, false, "", operacion)
			if err != nil || code != 200 {
				log.Error(pathLOG+"[kafkaViolationEventsLoop] Error calling E2CO API for app scaling: ", err)
				continue
			}
			log.Info(pathLOG + "[kafkaViolationEventsLoop] App info scaled: " + string(body))
		} // SCALE OUT
		if tipo["type"] == "MIGRATE" {
			//appInfoMap := appInfo.(map[string]interface{})
			object = appInfoMap["object"].(map[string]interface{})
			var target string
			var source string
			if object["idE2cOrchestrator"] != nil {
				source = object["idE2cOrchestrator"].(string)
			}
			if tipo["value"] != nil {
				target = tipo["value"].(string)
			}
			log.Info(pathLOG+"[kafkaViolationEventsLoop] Migrating app %v from %v to %v cluster\n", appID, source, target)

			// TODO call to DB or internal methods !!!!!!!!!!!!!

			// Call E2CO API for scaling
			var operacion structs.TaskOperation
			operacion.Operation = "migration"
			operacion.Target = target
			if target == "" || source == target {
				continue
			}
			code, body, err := myhttp.Put("http://localhost:8333/api/v1/apps/"+appID, false, "", operacion)
			if err != nil || code != 200 {
				log.Error(pathLOG+"[kafkaViolationEventsLoop] Error calling E2CO API for app migrating: ", err)
				continue
			}
			log.Info(pathLOG + "[kafkaViolationEventsLoop] App info migrated: " + string(body))
		} // MIGRATE
	} // endless for
} // kafkaViolationEventsLoop()
