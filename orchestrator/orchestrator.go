//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 23 Mar 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package orchestrator

import (
	"atos.pledger/e2c-orchestrator/data/structs"
	"atos.pledger/e2c-orchestrator/orchestrator/apps"
)

// path used in logs
const pathLOG string = "E2CO > Orchestrator > Applications "

///////////////////////////////////////////////////////////////////////////////
// ORCHESTRATORS

/*
AddNewOrchestrator Adds a new orchestrator
*/
func AddNewOrchestrator(orch structs.E2cOrchestrator) (structs.E2cOrchestrator, error) {
	return apps.AddNewOrchestrator(orch)
}

/*
UpdateOrchestrator Updates an orchestrator
*/
func UpdateOrchestrator(idOrch string, orch structs.E2cOrchestrator) (structs.E2cOrchestrator, error) {
	return apps.UpdateOrchestrator(idOrch, orch)
}

/*
RemoveOrchestrator Removes a orchestrator
*/
func RemoveOrchestrator(idOrch string) error {
	return apps.RemoveOrchestrator(idOrch)
}

///////////////////////////////////////////////////////////////////////////////
// APPLICATIONS

/*
DeployE2coApplication Deploy a E2coApplication
*/
func DeployE2coApplication(application *structs.E2coApplication, isNew bool) error {
	return apps.DeployE2coApplication(application, isNew)
}

/*
RemoveE2coApplication Deploy a E2coApplication
*/
func RemoveE2coApplication(id string) error {
	return apps.RemoveE2coApplication(id)
}

/*
GetE2coApplication get cluster's E2coApplication information
*/
func GetE2coApplication(id string) (structs.E2coApplication, error) {
	return apps.GetE2coApplication(id)
}

/*
GetE2coApp get cluster's E2coApplication information
*/
func GetE2coApp(id string) (structs.E2coSubApp, error) {
	return apps.GetE2coApp(id)
}

/*
UpdateE2coApplication Updates a E2coApplication
*/
func UpdateE2coApplication(app *structs.E2coApplication, operation structs.TaskOperation) error {
	return apps.UpdateE2coApplication(app, operation)
}


/*
UpdateE2coSubApp Updates a E2coSubApp
*/
func UpdateE2coSubApp(app *structs.E2coSubApp, operation structs.TaskOperation) error {
	return apps.UpdateE2coSubApp(app, operation)
}


/*
 */
 func ResetDryRun(app *structs.E2coApplication) {
	apps.ResetDryRun(app)
}
