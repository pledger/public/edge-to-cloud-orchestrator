//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 18 Mar 2021
// Updated on 18 Mar 2021
//
// @author: ATOS
//
package migrationengine

import (
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/db"
	"atos.pledger/e2c-orchestrator/data/structs"
	"atos.pledger/e2c-orchestrator/orchestrator/adapters"
)

// path used in logs
const pathLOG string = "E2CO > Orchestrator > Migration-Engine "

// remove application from location
func removeAppFromLocation(app *structs.E2coSubApp, location structs.E2coLocation, operation structs.TaskOperation, index int) error {
	log.Debug(pathLOG + "[removeAppFromLocation] Removing application from 'old' location [" + location.IDE2cOrchestrator + "] ...")

	// get orchestrator to get adapter and endpoint
	orch, err := db.ReadOrchestrator(location.IDE2cOrchestrator) // orchFrom
	if err != nil {
		log.Error(pathLOG+"[removeAppFromLocation] Error reading orchestrator from database: ", err)
		return err
	}

	// get adapter
	a := adapters.GetDeploymentengineAdapter(orch.Type)

	log.Debug(pathLOG + "[removeAppFromLocation] Location [" + orch.ID + "], Type [" + orch.Type + "]")

	// Remove task: call to adapter
	_, err = a.RemoveTask(app, orch)
	if err != nil {
		log.Error(pathLOG+"[removeAppFromLocation] Error removing application: ", err)
		app.Status = "errorTerminating"
		return err
	}

	log.Debug(pathLOG + "[removeAppFromLocation] Application removed from [" + location.IDE2cOrchestrator + "].")
	return nil
}

// deploy application to location
func deployAppToLocation(app *structs.E2coSubApp, orchTarget structs.E2cOrchestrator, operation structs.TaskOperation, index int) error {
	log.Debug(pathLOG + "[deployAppToLocation] Deploying application to location [" + orchTarget.ID + "] ...")

	// get adapter (from orchestrator)
	a := adapters.GetDeploymentengineAdapter(orchTarget.Type) 

	// set new values
	app.Locations[index].IDE2cOrchestrator = orchTarget.ID
	if operation.TargetNamespace == "" {
		app.Locations[index].NameSpace = orchTarget.DefaultNamespace
	} else {
		app.Locations[index].NameSpace = operation.TargetNamespace
	}
	
	app.Type = orchTarget.Type

	log.Debug(pathLOG + "[deployAppToLocation] Location [" + orchTarget.ID + "], NameSpace [" + app.Locations[index].NameSpace + "], Type [" + orchTarget.Type + "]")

	// deploy the application / task
	_, err := a.DeployTask(app, orchTarget)
	if err != nil {
		return err
	}

	log.Info(pathLOG + "[deployAppToLocation] Application is being deployed to [Location [" + orchTarget.ID + "], NameSpace [" + app.Locations[index].NameSpace + "]]. Storing information in database ...")
	return nil
}

/*
MigrateApp Migrate application From 'orchFrom' To 'operation.Target'

 TODO handle SLA ??
*/
func MigrateApp(app *structs.E2coSubApp, orchFrom structs.E2cOrchestrator, operation structs.TaskOperation) (*structs.E2coSubApp, error) {
	log.Info(pathLOG + "[MigrateApp] Migrating application / Redeploying application [" + app.ID + "] ...")

	// TARGET INFRASTRUCTURE / ORCHESTRATOR => orchTarget
	if operation.Target == "" {
		log.Warn(pathLOG + "[MigrateApp] Missing target orchestrator!")
		return app, nil
	}
	orchTarget, err := db.ReadOrchestrator(operation.Target)
	if err != nil {
		log.Error(pathLOG + "[MigrateApp] Target orchestrator not found / error: " + err.Error())
		return app, err
	}
	log.Info(pathLOG + "[MigrateApp] Migrating application [" + app.ID + "] to orchestrator with id '" + orchTarget.ID + "' ...")

	// check app is not deployed on target
	for _, location := range app.Locations {
		if location.IDE2cOrchestrator == operation.Target {
			log.Warn(pathLOG+"[MigrateApp] App already deployed in / migrated to this orchestrator ", operation.Target)
			return app, nil
		}
	}

	log.Info(pathLOG + "[MigrateApp] ---------------------------------------------")
	log.Info(pathLOG + "[MigrateApp] Migration: ")
	log.Info(pathLOG + "[MigrateApp] Application [" + app.ID + "]")
	log.Info(pathLOG + "[MigrateApp] From [" + orchFrom.ID + "]")
	log.Info(pathLOG + "[MigrateApp] To [" + orchTarget.ID + "]")
	
	// migration
	for index, location := range app.Locations {
		if location.IDE2cOrchestrator == orchFrom.ID {

			// 1. Remove FROM 'orchFrom'
			log.Info(pathLOG + "[MigrateApp] ---------------------------------------------")
			log.Info(pathLOG + "[MigrateApp] [1.] Removing application from 'old' location ...")

			err = removeAppFromLocation(app, location, operation, index)
			if err != nil {
				log.Error(pathLOG + "[MigrateApp] Error removing application: " + err.Error())
				return app, err
			}

			log.Info(pathLOG + "[MigrateApp] Application removed.")

			// 2. Deploy TO
			log.Info(pathLOG + "[MigrateApp] ---------------------------------------------")
			log.Info(pathLOG + "[MigrateApp] [2.] Deploying application to 'new' location ...")

			err = deployAppToLocation(app, orchTarget, operation, index)
			if err != nil {
				log.Error(pathLOG + "[MigrateApp] Error deploying application: " + err.Error())
				return app, err
			}

			log.Info(pathLOG + "[MigrateApp] Application is being deployed. Storing information in database ...")

			///////////////////////////////////
			/*
			// 1. Deploy TO
			log.Info(pathLOG + "[MigrateApp] [1.] Deploying application in 'new' location ...")

			err = deployAppToLocation(app, orchTarget, operation, index)
			if err != nil {
				log.Error(pathLOG + "[MigrateApp] Error deploying application: " + err.Error())
				return app, err
			}
			*/

			/*
			// get adapter (from orchestrator) - uncomment
			a := adapters.GetDeploymentengineAdapter(orchTarget.Type) 

			// set new values
			app.Locations[index].IDE2cOrchestrator = orchTarget.ID
			if operation.TargetNamespace == "" {
				app.Locations[index].NameSpace = orchTarget.DefaultNamespace
			} else {
				app.Locations[index].NameSpace = operation.TargetNamespace
			}
			
			app.Type = orchTarget.Type

			// deploy the application / task - uncomment
			_, err = a.DeployTask(app, orchTarget)
			if err != nil {
				return app, err
			}
			*/
			//log.Info(pathLOG + "[MigrateApp] Application is being deployed. Storing information in database ...")

			///////////////////////////////////
			/*
			// 2. Remove FROM
			log.Info(pathLOG + "[MigrateApp] [2.] Removing application from 'old' location ...")

			err = removeAppFromLocation(app, location, operation, index)
			if err != nil {
				log.Error(pathLOG + "[MigrateApp] Error removing application: " + err.Error())
				return app, err
			}
			*/

			/*
			// get orchestrator to get adapter and endpoint
			orch, err := db.ReadOrchestrator(location.IDE2cOrchestrator) // orchFrom
			if err != nil {
				log.Error(pathLOG+"[MigrateApp] Error reading orchestrator from database: ", err)
				return app, err
			}
			
			// get adapter
			a = adapters.GetDeploymentengineAdapter(orch.Type)

			// Remove task: call to adapter
			_, err = a.RemoveTask(app, orch)
			if err != nil {
				log.Error(pathLOG+"[MigrateApp] Error deleting application: ", err)
				app.Status = "errorTerminating"
				return app, err
			}
			*/
			//log.Info(pathLOG + "[MigrateApp] Application removed.")

			///////////////////////////////////
			// 3. Save task information in DB
			log.Trace(pathLOG + "[MigrateApp] Updating application status ...")
			app.Status = "deployed"
			err = db.SetE2coSubApp(app.ID, *app) // (id string, dbtask structs.E2coTaskExtended) returns (error)
			if err != nil {
				log.Error("[MigrateApp] Error storing value in database: ", err)
				return app, err
			}

			// get value
			dbApp, err := db.ReadE2coSubApp(app.ID) // returns (*structs.E2coTaskExtended, error)
			if err != nil {
				log.Error("[MigrateApp] Error retrieving value from database: ", err)
				return app, nil
			}

			// TODO 
			///////////////////////////////////
			// 4. SLA ???
			log.Warn(pathLOG + "[MigrateApp] SLA was not updated")

			///////////////////////////////////
			log.Info(pathLOG + "[MigrateApp] Migration process finalized")
			return &dbApp, nil
		}
	}
	
	log.Error(pathLOG + "[MigrateApp] Migration not processed. No [location.IDE2cOrchestrator == orchFrom.ID] found.")
	return app, nil
}
