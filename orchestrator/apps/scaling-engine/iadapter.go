//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 18 Mar 2021
// Updated on 18 Mar 2021
//
// @author: ATOS
//
package scalingengine

import "atos.pledger/e2c-orchestrator/data/structs"

// path used in logs
const pathLOG string = "E2CO > Orchestrator > Scaling-Engine "

/*
Adapter generic adapter
*/
type Adapter interface {
	// scale_out (replicas)
	ScaleOut(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error)

	// scale_in (replicas)
	ScaleIn(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error)

	// scale_up (RAM, CPU ...)
	ScaleUp(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error)

	// scale_down (RAM, CPU ...)
	ScaleDown(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error)
}
