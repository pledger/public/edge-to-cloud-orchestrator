//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 23 Mar 2021
// Updated on 18 Mar 2021
//
// @author: ATOS
//
package apps

import (
	"errors"
	"time"
	"strconv"

	connectors "atos.pledger/e2c-orchestrator/connectors"
	deploymentengine "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine"
	globals "atos.pledger/e2c-orchestrator/common/globals"
	log "atos.pledger/e2c-orchestrator/common/logs"
	db "atos.pledger/e2c-orchestrator/data/db"
	structs "atos.pledger/e2c-orchestrator/data/structs"
)

// subAppDeploymentError
func subAppDeploymentError(app *structs.E2coSubApp, location structs.E2coLocation) {
	// save task in DB; save status to DEPLOYMENT_FAILED
	app.Status = globals.DEPLOYMENT_FAILED
	err := db.SetE2coSubApp(app.ID, *app)
	if err != nil {
		log.Error(pathLOG+"[deploymentError] Error storing value in database: ", err)
	}
}

// deploySubApp deploy sub application
func deploySubApp(app *structs.E2coSubApp, location structs.E2coLocation) error {
	// get adapter and orchestrator
	_, adapter, orchestrator, err := getDeploymentInfo(location.IDE2cOrchestrator)
	if err != nil {
		log.Error(pathLOG+"[deploySubApp] Error getting application information (cluster, adapter): ", err)
		return err
	}

	// save subapplication in DB; save status to DEPLOYMENT_DEPLOYING
	app.Status = globals.DEPLOYMENT_DEPLOYING
	err = db.SetE2coSubApp(app.ID, *app)
	if err != nil {
		log.Error(pathLOG+"[deploySubApp] Error storing value in database: ", err)
		return errors.New("Error updating SubApp value in database: " + err.Error())
	}

	// location namespace
	if len(location.NameSpace) > 0 {
		orchestrator.DefaultNamespace = location.NameSpace 
	}

	// background deployment process
	deploymentProcess(app, *orchestrator, adapter)

	return nil
}

// deploymentProcess Deployment background processes
func deploymentProcess(app *structs.E2coSubApp, orch structs.E2cOrchestrator, a deploymentengine.Adapter) {
	log.Info(pathLOG + "[deploymentProcess] Starting deployment background process of sub app [" + app.Name + "] ...")

	// deploy the application / task
	_, err := a.DeployTask(app, orch)
	if err != nil {
		app.Status = globals.DEPLOYMENT_FAILED
		log.Error("[deploymentProcess] Error deploying sub application: ", err)
	} else {
		log.Info(pathLOG + "[deploymentProcess] Sub Application is being deployed ...")
		// wait still deployment is finished
		for i := 0; i < 10; i++ {
			// get app information from cluster
			log.Info(pathLOG + "[deploymentProcess] Checking sub application status [" + strconv.Itoa(i) + "] ...")
			appStatus, err := a.GetTask(*app, orch)
			if err != nil {
				log.Error(pathLOG+"[deploymentProcess] Error getting sub application: ", err)
			} else {
				if appStatus.Status == globals.DEPLOYMENT_DEPLOYED {
					app.Status = globals.DEPLOYMENT_DEPLOYED
					db.UpdateE2coSubApp(app.ID, *app)
					log.Info(pathLOG + "[deploymentProcess] Sub Application status is " + app.Status + ". Proceeding with next steps ...")
					break
				} 
				
				log.Debug(pathLOG + "[deploymentProcess] Sub Application is not ready ...")
				app.Status = appStatus.Status
				db.UpdateE2coSubApp(app.ID, *app)
			}

			time.Sleep(10 * time.Second)	// TODO loop should last more: image pull can take a lot of time
		}

		// call to CONNECTORS: SLA creation
		if len(app.Qos) > 0 {
			log.Info(pathLOG+"[deploymentProcess] Calling the SLA-Manager to create a new SLA ...")
			connectors.CreateSLA(*app)
		} else {
			log.Warn(pathLOG+"[deploymentProcess] No QoS definition found.")
		}
	}
	
	// save app in DB
	err = db.UpdateE2coSubApp(app.ID, *app) // (id string, dbtask structs.E2coTaskExtended) returns (error)
	if err != nil {
		log.Error("[deploymentProcess] Error storing value in database: ", err)
	}

	log.Info(pathLOG + "[deploymentProcess] Finalizing background process [" + app.Name + "] ...")
}