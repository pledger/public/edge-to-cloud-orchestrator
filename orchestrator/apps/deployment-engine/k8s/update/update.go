//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 04 Aug 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package update

import (
	"strconv"

	"atos.pledger/e2c-orchestrator/common"
	"atos.pledger/e2c-orchestrator/common/cfg"
	"atos.pledger/e2c-orchestrator/common/http"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
	appdeploy "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine/k8s/deploy"
)

// path used in logs
const pathLOG string = "E2CO > Orchestrator > Deployment-Engine > K8s > Update "

/*
UpdateK8sDeployment creates the deployment json used by K8s / call to Kubernetes API to create a deployment
*/
func UpdateK8sDeployment(app *structs.E2coSubApp, orch structs.E2cOrchestrator, sec bool) (string, error) {
	log.Info(pathLOG + "[UpdateK8sDeployment] Generating 'deployment' json ...")

	namespace := common.GetAppNamespace(*app, orch)
	k8sDepl := appdeploy.NewDeploymentStruct(*app, namespace, 1) // returns *structs.K8sDeployment

	if k8sDepl.Metadata.Namespace == "" {
		k8sDepl.Metadata.Namespace = orch.DefaultNamespace
	}

	strTxt, _ := common.StructToString(*k8sDepl)
	log.Info(pathLOG + "[UpdateK8sDeployment] [" + strTxt + "]")

	// CALL to Kubernetes API to launch a new deployment
	log.Info(pathLOG + "[UpdateK8sDeployment] Updating deployment in K8s cluster ...")
	status, _, err := http.PutStruct(
		cfg.GetPathKubernetesCreateDeployment(orch, namespace)+"/"+k8sDepl.Metadata.Name,
		sec,
		orch.ConnectionToken,
		k8sDepl)
	if err != nil {
		log.Error(pathLOG+"[UpdateK8sDeployment] ERROR", err)
		return "", err
	}
	log.Info(pathLOG + "[UpdateK8sDeployment] RESPONSE: OK")

	return strconv.Itoa(status), nil
}

/* 
UpdateK8sService re-creates the K8s service json / call to Kubernetes API to re-create a service
*/
func UpdateK8sService(app *structs.E2coSubApp, orch structs.E2cOrchestrator, sec bool) (string, int, string, error) {
	log.Info(pathLOG + "[UpdateK8sService] Generating 'service' json ...")

	namespace := common.GetAppNamespace(*app, orch)
	k8sServ, mainPort, mainPortName := appdeploy.NewServiceStruct(app, namespace, orch.IP) // returns *structs.K8sService

	if k8sServ.Metadata.Namespace == "" {
		k8sServ.Metadata.Namespace = orch.DefaultNamespace
	}

	strTxt, _ := common.StructToString(*k8sServ)
	log.Info(pathLOG + "[UpdateK8sService] [" + strTxt + "]")

	// CALL to Kubernetes API to launch a new service
	log.Info(pathLOG + "[UpdateK8sService] Updating service in K8s cluster ...")
	status, _, err := http.PutStruct(
		cfg.GetPathKubernetesCreateService(orch, namespace+"/"+k8sServ.Metadata.Name),
		sec,
		orch.ConnectionToken,
		k8sServ)
	if err != nil {
		log.Error(pathLOG+"[UpdateK8sService] ERROR", err)
		return "", -1, "", err
	}
	log.Info(pathLOG + "[UpdateK8sService] RESPONSE: OK")

	return strconv.Itoa(status), mainPort, mainPortName, nil
}
