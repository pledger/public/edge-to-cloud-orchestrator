//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 23 Mar 2021
//
// @author: ATOS
//
package terminate

import (
	"strconv"

	"atos.pledger/e2c-orchestrator/common/cfg"
	"atos.pledger/e2c-orchestrator/common/http"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
)

// path used in logs
const pathLOG string = "E2CO > Orchestrator > Deployment-Engine > K8s > Terminate "

/* 
DelK8sDeployment call to Kubernetes API to delete a deployment
*/
func DelK8sDeployment(namespace string, name string, orch structs.E2cOrchestrator, sec bool) (string, error) {
	log.Info(pathLOG + "[DelK8sDeployment] Deleting deployment from K8s orchestrator ...")

	status, _, err := http.DeleteStruct(cfg.GetPathKubernetesDeleteDeployment(orch, namespace, name), sec, orch.ConnectionToken)
	if err != nil {
		log.Error(pathLOG+"[DelK8sDeployment] ERROR", err)
		return strconv.Itoa(status), err
	}
	log.Info(pathLOG + "[DelK8sDeployment] RESPONSE: OK")

	return strconv.Itoa(status), nil
}

/* 
DelK8sService call to Kubernetes API to delete a service
*/
func DelK8sService(namespace string, name string, orch structs.E2cOrchestrator, sec bool) (string, error) {
	log.Info(pathLOG + "[DelK8sService] Deleting service from K8s orchestrator ...")

	status, _, err := http.DeleteStruct(cfg.GetPathKubernetesService(orch, namespace, name), sec, orch.ConnectionToken)
	if err != nil {
		log.Error(pathLOG+"[DelK8sService] ERROR", err)
		return strconv.Itoa(status), err
	}
	log.Info(pathLOG + "[DelK8sService] RESPONSE: OK")

	return strconv.Itoa(status), nil
}
