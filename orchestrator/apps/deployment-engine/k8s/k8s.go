//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 07 Apr 2021
// Updated on 07 Apr 2021
//
// @author: ATOS
//
package k8s

import (
	"encoding/json"
	"errors"

	"atos.pledger/e2c-orchestrator/common"
	"atos.pledger/e2c-orchestrator/common/cfg"
	"atos.pledger/e2c-orchestrator/common/globals"
	"atos.pledger/e2c-orchestrator/common/http"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
	appdeploy "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine/k8s/deploy"
	appterminate "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine/k8s/terminate"
	appupdate "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine/k8s/update"
)

// path used in logs
const pathLOG string = "E2CO > Orchestrator > Deployment-Engine > K8s "

/*
Adapter K8s Adapter struct
*/
type Adapter struct{}

/*
GetDeploymentErrorResponse Error Response from call to K8s REST API
*/
type DeploymentErrorResponse struct {
	Code    int    `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}

/*
DeploymentResponse Response from call to K8s REST API
*/
type DeploymentResponse struct {
	Status  DeploymentStatusResponse `json:"status,omitempty"`
	Code    int                      `json:"code,omitempty"`
	Message string                   `json:"message,omitempty"`
}

/*
DeploymentStatusResponse Status struct
*/
type DeploymentStatusResponse struct {
	Replicas          int `json:"replicas,omitempty"`
	ReadyReplicas     int `json:"readyReplicas,omitempty"`
	AvailableReplicas int `json:"availableReplicas,omitempty"`
}

// parseResponse: parses incomming JSON to an GetDeploymentResponse struct
func parseResponse(r map[string]interface{}) (DeploymentResponse, error) {
	log.Debug(pathLOG + "[parseResponse] Parsing default json definition [DeploymentResponse] ...")
	jsonString, _ := json.Marshal(r)

	// DeploymentResponse?
	s := DeploymentResponse{}
	err := json.Unmarshal(jsonString, &s)
	if err != nil {
		log.Warn(pathLOG+"[parseResponse] JSON is not a DeploymentResponse object: ", err)

		// DeploymentErrorResponse?
		se := DeploymentErrorResponse{}
		err := json.Unmarshal(jsonString, &se)
		if err != nil {
			log.Error(pathLOG+"[parseResponse] JSON is not a DeploymentResponse object: ", err)
			return DeploymentResponse{}, err
		}

		s.Code = se.Code
		s.Message = se.Message
	} else {
		s.Code = 200
		s.Message = "ok"
	}

	return s, nil
}

///////////////////////////////////////////////////////////////////////////////

/*
GetApp get full deployment information; CALL to Kubernetes API to get deployment
*/
func (a Adapter) GetTask(app structs.E2coSubApp, orch structs.E2cOrchestrator) (structs.E2coTaskStatusInfo, error) {
	log.Info(pathLOG + "[GetApp] Getting deployment from K8s cluster ...")

	// Auth Token needed?
	sec := common.AuthTokenNeeded(orch)

	// structs.E2coTaskStatusInfo
	status := structs.E2coTaskStatusInfo{}
	status.Status = globals.DEPLOYMENT_UNDEFINED
	status.Type = "k8s_deployment"

	// call to REST API
	_, obj, err := http.GetStruct(cfg.GetPathKubernetesGetDeployment(orch, orch.DefaultNamespace, app.ID), sec, orch.ConnectionToken)
	if err != nil {
		log.Error(pathLOG+"[GetApp] ERROR ", err)
		return status, err
	} else {
		// DEBUG Container INFO
		strObj, err := common.StructToString(obj)
		if err == nil {
			log.Debug(pathLOG + "[GetApp] Deployment INFO: " + strObj)
		}

		deplResp, err := parseResponse(obj)
		if err != nil {
			log.Error(pathLOG+"[GetApp] ERROR parsing response: ", err)
			return status, err
		}

		if deplResp.Message == "ok" {
			if deplResp.Status.Replicas == deplResp.Status.ReadyReplicas {
				status.Status = globals.DEPLOYMENT_DEPLOYED
				status.Message = "application deployed "
				// call to REST API - Containers inf
				_, obj, err := http.GetStruct(cfg.GetPathKubernetesPodsApp(orch, orch.DefaultNamespace, app.ID), sec, orch.ConnectionToken)
				if err != nil {
					log.Error(pathLOG+"[GetApp] ERROR getting k8s application pods info ", err)
				} else {
					status.Obj = obj
				}
			} else {
				status.Status = globals.DEPLOYMENT_NOT_READY
				status.Message = "application not ready "
			}
		} else {
			status.Status = globals.DEPLOYMENT_UNDEFINED
			status.Message = "application in undefined status"
		}

		return status, nil
	}
}

/*
DeployApp Deploy a subapplication / service (k8s: deployment & service & volumes ...)
*/
func (a Adapter) DeployTask(app *structs.E2coSubApp, orch structs.E2cOrchestrator) (string, error) {
	log.Debug(pathLOG + "[DeployApp] Deploying K8s components (deployment, service) for the new application ...")
	log.Debug(pathLOG + "[DeployApp] Orchestrator id = " + orch.ID + ", namespace = " + common.GetAppNamespace(*app, orch) + ", Endpoint = " + orch.RESTAPIEndPoint + "")

	sec := common.AuthTokenNeeded(orch)	// Auth Token needed?
	status, err := appdeploy.CreateK8sDeployment(app, orch, sec)	// 1. DEPLOYMENT /////
	if err != nil {
		log.Error(pathLOG+"[DeployApp] ERROR (1)", err)
		return "", err
	} else if (status == "200" || status == "201") { // create service if Exposed == true
		if app.Service.Expose {
			status, _, _, err := appdeploy.CreateK8sService(app, orch, sec)	// 2. SERVICE /////
			if err != nil {
				log.Error(pathLOG+"[DeployApp] ERROR (2)", err)
				return "", err
			} else if status == "200" || status == "201" {
				log.Info(pathLOG + "[DeployApp] Application components (deployment, service) deployed with success")
				return "ok", nil
			}
		} else {
			log.Info(pathLOG + "[DeployApp] Application components (deployment) deployed with success. No service was created. ")
			return "ok", nil
		}
	}

	err = errors.New("Components creation failed. status = [" + status + "]")
	log.Error(pathLOG+"[DeployApp] ERROR (4)", err)
	return "", err
}

/*
RemoveApp Removes a subapplication from orchestrator / service
*/
func (a Adapter) RemoveTask(app *structs.E2coSubApp, orch structs.E2cOrchestrator) (string, error) {
	log.Info(pathLOG + "[RemoveApp] Deleting application ...")

	namespace := common.GetAppNamespace(*app, orch)
	name := app.ID
	log.Info(pathLOG + "[RemoveApp] Deleting task [" + name + "] from [" + namespace + "] ...")

	sec := common.AuthTokenNeeded(orch)	// Auth Token needed?
	status, err := appterminate.DelK8sDeployment(namespace, name, orch, sec)	// 1. DEPLOYMENT /////
	if err != nil {
		log.Error(pathLOG+"[RemoveApp] ERROR (1)", err)
		if status == "404" { // Not Found
			log.Info(pathLOG + "[RemoveApp] Task removed with success but not found")
			return "ok", nil
		}
	} else if status == "200" {
		if app.Service.Expose {
			status, err := appterminate.DelK8sService(namespace, name, orch, sec)	// 2. SERVICE /////
			if err != nil {
				log.Error(pathLOG+"[RemoveApp] ERROR (2)", err)
				if status == "404" { // Not Found
					log.Info(pathLOG + "[RemoveApp] Task removed with success but not found")
					return "ok", nil
				}
			} else if status == "200" {
				log.Info(pathLOG + "[RemoveApp] Task removed with success")
				return "ok", nil
			}
		} else {
			return "ok", nil
		}
	}

	err = errors.New("Task termination failed. status = [" + status + "]")
	log.Error(pathLOG+"[RemoveApp] ERROR (4)", err)
	return "", err
}


/*
UpdateTask RE-Deploy a task (k8s: re-deployment & service & volumes ...)
*/
func (a Adapter) UpdateTask(app *structs.E2coSubApp, orch structs.E2cOrchestrator) (string, error) {
	log.Info(pathLOG + "[UpdateTask] Re-Deploying K8s components (deployment, service) for the new application ...")

	clusterID := orch.ID
	clusterHost := orch.IP
	namespace := common.GetAppNamespace(*app, orch)
	log.Info(pathLOG + "[UpdateTask] Orchestrator id = " + clusterID + ", namespace = " + namespace + ", host = " + clusterHost + "")

	// Auth Token needed?
	sec := common.AuthTokenNeeded(orch)

	// 1. DEPLOYMENT /////
	status, err := appupdate.UpdateK8sDeployment(app, orch, sec)
	if err != nil {
		log.Error(pathLOG+"[UpdateTask] ERROR (1)", err)
		return "", err
	} else if status == "200" || status == "201" {
		// 2. SERVICE /////
		var hasService bool = false
		for key := range app.Containers {
			if len(app.Containers[key].Ports) > 0 {
				hasService = true
				break
			}
		}
		if !hasService {
			return "ok", nil
		}
		status, _, _, err := appupdate.UpdateK8sService(app, orch, sec)
		if err != nil {
			log.Error(pathLOG+"[UpdateTask] ERROR (2)", err)
			return "", err
		} else if status == "200" || status == "201" {
			log.Info(pathLOG + "[UpdateTask] Application components (deployment, service) deployed with success")
			return "ok", nil
		}
	}

	err = errors.New("Components creation failed. status = [" + status + "]")
	log.Error(pathLOG+"[UpdateTask] ERROR (4)", err)
	return "", err
}
