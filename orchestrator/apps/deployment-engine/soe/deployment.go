//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 07 Apr 2021
//
// @author: ATOS
//
package soe

import (
	"strconv"
	"strings"

	"atos.pledger/e2c-orchestrator/common"
	"atos.pledger/e2c-orchestrator/common/cfg"
	"atos.pledger/e2c-orchestrator/common/http"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
	"atos.pledger/e2c-orchestrator/data/db"
)

/* SOE DEPLOYMENT example:

	{   
		"user_id": "5b63089158f568073093f70d",   
		"slic3_id": "5b63089158f568073093f70d",   
		"network_service_id": "5b63089158f568073093f70d",   
		"name": "pledger-uc3-app",   
		"description": "Instance example",   
		"requirements": {     
			"request_cpu": "100m",     
			"request_memory": "100Mi",     
			"limit_cpu": "200m",     
			"limit_memory": "200Mi"     
		} 
	}
*/

type SOEDeployment struct {
    UserID 				string 		`json:"user_id,omitempty"`
	Slic3ID 			string  	`json:"slic3_id,omitempty"`
	NetworkServiceID 	string  	`json:"network_service_id,omitempty"`
	Name 				string  	`json:"name,omitempty"`
	Description 		string  	`json:"description,omitempty"`
	Requirements  	struct {
		RequestCPU 		string 		`json:"request_cpu,omitempty"`
		RequestMemory 	string 		`json:"request_memory,omitempty"`
		LimitCPU 		string 		`json:"limit_cpu,omitempty"`
		LimitMemory 	string 		`json:"limit_memory,omitempty"`
	} `json:"requirements,omitempty"`
}

/* 
NewDeploymentStruct creates a new K8s Deployment json
*/
func NewDeploymentStruct(app structs.E2coSubApp, soeObj structs.E2coSoeObject) SOEDeployment {
	log.Debug(pathLOG + "[NewDeploymentStruct] Creatin SOE json deployment struct ...")

	var jsonDeployment SOEDeployment

	// from soe object
	jsonDeployment.UserID = soeObj.UserID
	jsonDeployment.Slic3ID = soeObj.Slic3ID
	//jsonDeployment.Name = soeObj.Name
	jsonDeployment.Description = soeObj.Description

	// from app
	if len(app.Containers) > 0 {
		jsonDeployment.Name = app.Containers[0].Name

		jsonDeployment.Requirements.RequestCPU = app.Containers[0].Hw.Cpu
		jsonDeployment.Requirements.RequestMemory = app.Containers[0].Hw.Memory
		jsonDeployment.Requirements.LimitCPU = app.Containers[0].Hw.Cpu
		jsonDeployment.Requirements.LimitMemory = app.Containers[0].Hw.Memory
	} else {
		log.Error(pathLOG + "[NewDeploymentStruct] No containers defined")
	}

	// get network_service_id from service_descriptor in an ENV_VAR ("network_service_id")
	if len(app.Containers) > 0 && len(app.Containers[0].Environment) > 0 {
		for _, env := range app.Containers[0].Environment {
			if strings.ToLower(env.Name) == "network_service_id" {
				log.Debug(pathLOG + "[NewDeploymentStruct] 'network_service_id' env variable found: " + env.Value)
				jsonDeployment.NetworkServiceID = env.Value
				break
			} else {

			}
		}
	} else {
		log.Error(pathLOG + "[NewDeploymentStruct] No environment variables defined")
	}

	log.Debug(pathLOG + "[NewDeploymentStruct] Returning jsonDeployment struct ...")

	return jsonDeployment
}

/* 
createSOEDeployment creates the deployment json used by SOE; call to SOE API to create a deployment
*/
func createSOEDeployment(app *structs.E2coSubApp, orch structs.E2cOrchestrator, sec bool) (string, error) {
	log.Info(pathLOG + "[CreateSOEDeployment] Generating 'network service instance' json [app=" + app.ID + ", orch=" + orch.ID + "] ...")

	var soeDepl SOEDeployment

	// TODO 
	log.Info(pathLOG + "[CreateSOEDeployment] Getting SOE object from database ...")
	sOEObj, err := db.ReadSOEObj(orch.ID)
	if err == nil { // found
		soeDepl = NewDeploymentStruct(*app, sOEObj) // returns SOEDeployment
	} else {
		return "SOEObj not found", err
	}

	strTxt, _ := common.StructToString(soeDepl)
	log.Info(pathLOG + "[CreateSOEDeployment] [" + strTxt + "]")

	// CALL to SOE API to launch a new deployment
	log.Info(pathLOG + "[CreateSOEDeployment] Creating a  network service instance in SOE ...")
	status, _, err := http.PostStruct(
		cfg.GetPathSOENetworkInstance(orch),
		sec,
		orch.ConnectionToken,
		soeDepl)
	if err != nil {
		log.Error(pathLOG+"[CreateSOEDeployment] ERROR", err)
		return "", err
	}
	log.Info(pathLOG + "[CreateSOEDeployment] RESPONSE: OK")

	return strconv.Itoa(status), nil
}