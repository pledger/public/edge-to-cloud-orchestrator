# Deployment Engine

&copy; Atos Spain S.A. 2020


### Docker CONFIGURATION

#### Enable TCP port 2375 for external connection to Docker

1. Create daemon.json file in /etc/docker:

  ```json
  {"hosts": ["tcp://0.0.0.0:2375", "unix:///var/run/docker.sock"]}
  ```

2. Add */etc/systemd/system/docker.service.d/override.conf*

  ```bash
  [Service]
  ExecStart=
  ExecStart=/usr/bin/dockerd
  ```

  Or edit */lib/systemd/system/docker.service*:

  ```bash
  sudo nano /lib/systemd/system/docker.service
  ```

  ```bash
  ExecStart=/usr/bin/dockerd
  ```

3. Reload the systemd daemon:

  ```bash
  sudo systemctl daemon-reload
  ```

4. Restart docker:

  ```bash
  sudo systemctl restart docker.service 
  ```

#### Enable TCP port 2376 for external connection to Docker

#### Docker Compose

#### Docker Swarm

---------------------------

### K8s CONFIGURATION

### K3s CONFIGURATION

### MicroK8s CONFIGURATION

### Openshift CONFIGURATION