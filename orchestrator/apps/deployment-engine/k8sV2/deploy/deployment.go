//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 01 Jul 2021
// Updated on 01 Jul 2021
//
// @author: ATOS
//
package deploy

import (
	"strconv"
	//"context"

	//"k8s.io/client-go/kubernetes"
	//"k8s.io/client-go/rest" 
	appsv1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	resource "k8s.io/apimachinery/pkg/api/resource"

	//"atos.pledger/e2c-orchestrator/common"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
)

// path used in logs
const pathLOG string = "E2CO > Orchestrator > Deployment-Engine > K8sV2 > Deploy "

/******************************************************************************
https://pkg.go.dev/k8s.io/api/apps/v1

DEPLOYMENT

		https://pkg.go.dev/k8s.io/api/apps/v1#Deployment

		type Deployment struct {
			metav1.TypeMeta 			`json:",inline"`
			metav1.ObjectMeta 			`json:"metadata,omitempty" protobuf:"bytes,1,opt,name=metadata"`
			Spec	DeploymentSpec 		`json:"spec,omitempty" protobuf:"bytes,2,opt,name=spec"`
			Status 	DeploymentStatus 	`json:"status,omitempty" protobuf:"bytes,3,opt,name=status"`
		}


		https://pkg.go.dev/k8s.io/api/apps/v1#DeploymentSpec

		type DeploymentSpec struct {
			Replicas 				*int32 					`json:"replicas,omitempty" protobuf:"varint,1,opt,name=replicas"`
			Selector 				*metav1.LabelSelector 	`json:"selector" protobuf:"bytes,2,opt,name=selector"`
			Template 				v1.PodTemplateSpec 		`json:"template" protobuf:"bytes,3,opt,name=template"`
			Strategy 				DeploymentStrategy 		`json:"strategy,omitempty" patchStrategy:"retainKeys" protobuf:"bytes,4,opt,name=strategy"`
			MinReadySeconds 		int32 					`json:"minReadySeconds,omitempty" protobuf:"varint,5,opt,name=minReadySeconds"`
			RevisionHistoryLimit 	*int32 					`json:"revisionHistoryLimit,omitempty" protobuf:"varint,6,opt,name=revisionHistoryLimit"`
			Paused 					bool 					`json:"paused,omitempty" protobuf:"varint,7,opt,name=paused"`
			ProgressDeadlineSeconds *int32 					`json:"progressDeadlineSeconds,omitempty" protobuf:"varint,9,opt,name=progressDeadlineSeconds"`
		}


		https://pkg.go.dev/k8s.io/api@v0.21.2/core/v1#PodTemplateSpec

		type PodTemplateSpec struct {
			metav1.ObjectMeta 	`json:"metadata,omitempty" protobuf:"bytes,1,opt,name=metadata"`
			Spec 	PodSpec 	`json:"spec,omitempty" protobuf:"bytes,2,opt,name=spec"`
		}


		https://pkg.go.dev/k8s.io/api@v0.21.2/core/v1#PodSpec

		type PodSpec struct {
			Volumes 						[]Volume 				`json:"volumes,omitempty" patchStrategy:"merge,retainKeys" patchMergeKey:"name" protobuf:"bytes,1,rep,name=volumes"`
			InitContainers 					[]Container 			`json:"initContainers,omitempty" patchStrategy:"merge" patchMergeKey:"name" protobuf:"bytes,20,rep,name=initContainers"`
			Containers 						[]Container 			`json:"containers" patchStrategy:"merge" patchMergeKey:"name" protobuf:"bytes,2,rep,name=containers"`
			EphemeralContainers 			[]EphemeralContainer 
			RestartPolicy 					RestartPolicy 			`json:"restartPolicy,omitempty" protobuf:"bytes,3,opt,name=restartPolicy,casttype=RestartPolicy"`
			TerminationGracePeriodSeconds 	*int64 					`json:"terminationGracePeriodSeconds,omitempty" protobuf:"varint,4,opt,name=terminationGracePeriodSeconds"`
			ActiveDeadlineSeconds 			*int64 					`json:"activeDeadlineSeconds,omitempty" protobuf:"varint,5,opt,name=activeDeadlineSeconds"`
			DNSPolicy 						DNSPolicy 				`json:"dnsPolicy,omitempty" protobuf:"bytes,6,opt,name=dnsPolicy,casttype=DNSPolicy"`
			NodeSelector 					map[string]string 		`json:"nodeSelector,omitempty" protobuf:"bytes,7,rep,name=nodeSelector"`
			ServiceAccountName 				string 					`json:"serviceAccountName,omitempty" protobuf:"bytes,8,opt,name=serviceAccountName"`
			DeprecatedServiceAccount 		string 					`json:"serviceAccount,omitempty" protobuf:"bytes,9,opt,name=serviceAccount"`
			AutomountServiceAccountToken 	*bool 					`json:"automountServiceAccountToken,omitempty" protobuf:"varint,21,opt,name=automountServiceAccountToken"`
			NodeName 						string 					`json:"nodeName,omitempty" protobuf:"bytes,10,opt,name=nodeName"`
			HostNetwork 					bool 					`json:"hostNetwork,omitempty" protobuf:"varint,11,opt,name=hostNetwork"`
			HostPID 						bool 					`json:"hostPID,omitempty" protobuf:"varint,12,opt,name=hostPID"`
			HostIPC 						bool 					`json:"hostIPC,omitempty" protobuf:"varint,13,opt,name=hostIPC"`
			ShareProcessNamespace 			*bool 					`json:"shareProcessNamespace,omitempty" protobuf:"varint,27,opt,name=shareProcessNamespace"`
			SecurityContext 				*PodSecurityContext 	`json:"securityContext,omitempty" protobuf:"bytes,14,opt,name=securityContext"`
			ImagePullSecrets 				[]LocalObjectReference 	`json:"imagePullSecrets,omitempty" patchStrategy:"merge" patchMergeKey:"name" protobuf:"bytes,15,rep,name=imagePullSecrets"`
			Hostname 						string 					`json:"hostname,omitempty" protobuf:"bytes,16,opt,name=hostname"`
			Subdomain 						string 					`json:"subdomain,omitempty" protobuf:"bytes,17,opt,name=subdomain"`
			Affinity 						*Affinity 				`json:"affinity,omitempty" protobuf:"bytes,18,opt,name=affinity"`
			SchedulerName 					string 					`json:"schedulerName,omitempty" protobuf:"bytes,19,opt,name=schedulerName"`
			Tolerations 					[]Toleration 			`json:"tolerations,omitempty" protobuf:"bytes,22,opt,name=tolerations"`
			HostAliases 					[]HostAlias 			`json:"hostAliases,omitempty" patchStrategy:"merge" patchMergeKey:"ip" protobuf:"bytes,23,rep,name=hostAliases"`
			PriorityClassName 				string 					`json:"priorityClassName,omitempty" protobuf:"bytes,24,opt,name=priorityClassName"`
			Priority 						*int32 					`json:"priority,omitempty" protobuf:"bytes,25,opt,name=priority"`
			DNSConfig 						*PodDNSConfig 			`json:"dnsConfig,omitempty" protobuf:"bytes,26,opt,name=dnsConfig"`
			ReadinessGates 					[]PodReadinessGate 		`json:"readinessGates,omitempty" protobuf:"bytes,28,opt,name=readinessGates"`
			RuntimeClassName 				*string 				`json:"runtimeClassName,omitempty" protobuf:"bytes,29,opt,name=runtimeClassName"`
			EnableServiceLinks 				*bool 					`json:"enableServiceLinks,omitempty" protobuf:"varint,30,opt,name=enableServiceLinks"`
			PreemptionPolicy				*PreemptionPolicy 		`json:"preemptionPolicy,omitempty" protobuf:"bytes,31,opt,name=preemptionPolicy"`
			Overhead 						ResourceList 			`json:"overhead,omitempty" protobuf:"bytes,32,opt,name=overhead"`
			TopologySpreadConstraints 		[]TopologySpreadConstraint `` 
			SetHostnameAsFQDN 				*bool 					`json:"setHostnameAsFQDN,omitempty" protobuf:"varint,35,opt,name=setHostnameAsFQDN"`
		}


		https://pkg.go.dev/k8s.io/api@v0.21.2/core/v1#Container

		type Container struct {
			Name 						string 					`json:"name" protobuf:"bytes,1,opt,name=name"`
			Image 						string 					`json:"image,omitempty" protobuf:"bytes,2,opt,name=image"`
			Command 					[]string 				`json:"command,omitempty" protobuf:"bytes,3,rep,name=command"`
			Args 						[]string 				`json:"args,omitempty" protobuf:"bytes,4,rep,name=args"`
			WorkingDir 					string 					`json:"workingDir,omitempty" protobuf:"bytes,5,opt,name=workingDir"`
			Ports 						[]ContainerPort 		`json:"ports,omitempty" patchStrategy:"merge" patchMergeKey:"containerPort" protobuf:"bytes,6,rep,name=ports"`
			EnvFrom 					[]EnvFromSource 		`json:"envFrom,omitempty" protobuf:"bytes,19,rep,name=envFrom"`
			Env 						[]EnvVar 				`json:"env,omitempty" patchStrategy:"merge" patchMergeKey:"name" protobuf:"bytes,7,rep,name=env"`
			Resources 					ResourceRequirements 	`json:"resources,omitempty" protobuf:"bytes,8,opt,name=resources"`
			VolumeMounts 				[]VolumeMount 			`json:"volumeMounts,omitempty" patchStrategy:"merge" patchMergeKey:"mountPath" protobuf:"bytes,9,rep,name=volumeMounts"`
			VolumeDevices 				[]VolumeDevice 			`json:"volumeDevices,omitempty" patchStrategy:"merge" patchMergeKey:"devicePath" protobuf:"bytes,21,rep,name=volumeDevices"`
			LivenessProbe 				*Probe 					`json:"livenessProbe,omitempty" protobuf:"bytes,10,opt,name=livenessProbe"`
			ReadinessProbe 				*Probe 					`json:"readinessProbe,omitempty" protobuf:"bytes,11,opt,name=readinessProbe"`
			StartupProbe 				*Probe 					`json:"startupProbe,omitempty" protobuf:"bytes,22,opt,name=startupProbe"`
			Lifecycle 					*Lifecycle 				`json:"lifecycle,omitempty" protobuf:"bytes,12,opt,name=lifecycle"`
			TerminationMessagePath 		string 					`json:"terminationMessagePath,omitempty" protobuf:"bytes,13,opt,name=terminationMessagePath"`
			TerminationMessagePolicy 	TerminationMessagePolicy `` 
			ImagePullPolicy 			PullPolicy 				`json:"imagePullPolicy,omitempty" protobuf:"bytes,14,opt,name=imagePullPolicy,casttype=PullPolicy"`
			SecurityContext 			*SecurityContext 		`json:"securityContext,omitempty" protobuf:"bytes,15,opt,name=securityContext"`
			Stdin 						bool 					`json:"stdin,omitempty" protobuf:"varint,16,opt,name=stdin"`
			StdinOnce 					bool 					`json:"stdinOnce,omitempty" protobuf:"varint,17,opt,name=stdinOnce"`
			TTY 						bool 					`json:"tty,omitempty" protobuf:"varint,18,opt,name=tty"`
		}

******************************************************************************/

// int32Ptr int32 to pointer int32
func int32Ptr(i int32) *int32 { return &i }

// newDeploymentStruct: creates a new K8s Deployment json
func newDeploymentStruct(app structs.E2coSubApp, replicas int) *appsv1.Deployment {
	// replicas:
	if app.Replicas > 1 {
		replicas = app.Replicas
	} 

	// containers:
	containers := []apiv1.Container{}

	totalContainers := len(app.Containers)
	for i := 0; i < totalContainers; i++ {
		// ports:
		ports := []apiv1.ContainerPort{}
		totalPorts := len(app.Containers[i].Ports)
		for j := 0; j < totalPorts; j++ {
			p := apiv1.ContainerPort{
				//Name:          "http",
				Protocol:      apiv1.ProtocolTCP,
				ContainerPort: int32(app.Containers[i].Ports[j].ContainerPort),
			}
			ports = append(ports, p)
		}

		// env:
		envs := []apiv1.EnvVar{}
		totalEnvs := len(app.Containers[i].Environment)
		if totalEnvs > 0 {
			for j := 0; j < totalEnvs; j++ {
				e := apiv1.EnvVar {
					Value: app.Containers[i].Environment[j].Value,
					Name: app.Containers[i].Environment[j].Name,
				}
				envs = append(envs, e)
			}
		}

		// resources
		resources := apiv1.ResourceRequirements{}
		if app.Containers[i].Hw != (structs.E2coAppkHwProps{}) {
			requests := apiv1.ResourceList{}

			if len(app.Containers[i].Hw.Cpu) > 0 {
				requests["cpu"] = resource.MustParse(app.Containers[i].Hw.Cpu)
			}

			if len(app.Containers[i].Hw.Memory) > 0 {
				requests["memory"] = resource.MustParse(app.Containers[i].Hw.Memory)
			}

			resources.Requests = requests
		}

		// container:
		containerName := app.Containers[i].Name
		if len(app.Containers[i].Name) == 0 {
			containerName = app.Name + strconv.Itoa(i)
		} 
		c := apiv1.Container{
			Name:  				containerName,
			Image: 				app.Containers[i].Image,
			ImagePullPolicy: 	"Always",
			Ports: 				ports,
			Command: 			app.Containers[i].Command, //command,
			Args: 				app.Containers[i].Args, //args,
			Env: 				envs,
			Resources: 			resources,
		}

		containers = append(containers, c)
	}

	// labels
	labelsObjectMeta := map[string]string{}
	labelsSpecTemplate := map[string]string{}
	labelsSpecSelector := map[string]string{}
	
	labelsObjectMeta["app"] = app.ID
	labelsSpecSelector["app"] = app.ID
	labelsSpecTemplate["app"] = app.ID
	
	totalLabels := len(app.Labels)
	if totalLabels > 0 {
		for k := 0; k < totalLabels; k++ {
			labelsSpecSelector[app.Labels[k].Key] = app.Labels[k].Value
			labelsSpecTemplate[app.Labels[k].Key] = app.Labels[k].Value
		}
	}

	// deployment:
	deployment := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name: app.ID,	// name?
			Labels: labelsObjectMeta,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: int32Ptr(int32(replicas)),
			RevisionHistoryLimit: int32Ptr(int32(10)),
			Selector: &metav1.LabelSelector{
				MatchLabels: labelsSpecSelector,
			},
			Template: apiv1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labelsSpecTemplate,
				},
				Spec: apiv1.PodSpec{
					Containers: containers,
				},
			},
		},
	}

	log.Debug(pathLOG + "[newDeploymentStruct] Returning appsv1.Deployment struct ...")
	return deployment
}

/* 
CreateK8sDeployment creates the deployment json used by K8s; call to Kubernetes API to create a deployment
*/
func CreateK8sDeployment(app *structs.E2coSubApp, orch structs.E2cOrchestrator, sec bool) (string, error) {
	log.Info(pathLOG + "[CreateK8sDeployment] Generating 'deployment' object ...")
/*	
	k8sDepl := newDeploymentStruct(*app, 1) // returns *appsv1.Deployment

	var cfg rest.Config
	// e.g. "https://192.168.1.141:16443"
	cfg.Host = orch.RESTAPIEndPoint
	// e.g. "eyJhbGciOiJSUzI1NiIsImtpZCI6IklxdWt0c01TY1R6TkdEWGsxcFlsT3BWcVlnVlJqT2U0VDNFcHd6UVVaZ1kifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJkZWZhdWx0LXRva2VuLXh0cXR2Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImRlZmF1bHQiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJjY2NjZGE0OS02NGY1LTRkNDQtYWRjZi1kNjliM2E5YzRhMmEiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06ZGVmYXVsdCJ9.PmBQyI-pN5mcSk-e20nnWKQ2jeR72WNjg_UD1mAfpWKBYPbyxs3MLYWVvwuj2zydYZsLkCvHQJSsrI5uMdPAZoc-2OPfV8pbRTGFDplX9us76R0WOv1_jCb0e6GhMI0jX4-OIXdcR2lC0VfQ5MfYcoVG16GMq6LWgw_Uxm4zGVqZ-QWw1CCg-QbkTTaoQ1h07w_2Dtp4QWKJr3evPj28XSn_PgwHzP-rWntOnMaMbRPMCyzri3Dq0ZAX4oZgAUWU87w99DexcX3rMPWhiu2E-hwmKkleJgbcn28xPDW2BHEncWUMUD38EIp3my9XUjd5H2awlmMIdn3NCoo1Jzdrtg"
	cfg.BearerToken = orch.ConnectionToken
	// Insecure = true
	cfg.TLSClientConfig.Insecure = true

	// create the clientset
	clientset, err := kubernetes.NewForConfig(&cfg)
	if err != nil {
		log.Error(pathLOG + "[CreateK8sDeployment] Error creating the client connection: ", err)
		return "error", err
	}

	// Create Deployment
	deploymentsClient := clientset.AppsV1().Deployments(common.GetAppNamespace(*app, orch)) // apiv1.NamespaceDefault

	log.Info(pathLOG + "[CreateK8sDeployment] Creating deployment...")
	result, err := deploymentsClient.Create(context.TODO(), k8sDepl, metav1.CreateOptions{})
	if err != nil {
		log.Error(pathLOG + "[CreateK8sDeployment] Error deploying the application: ", err)
		return "error", err
	}
	log.Info(pathLOG + "[CreateK8sDeployment] Created deployment %q.", result.GetObjectMeta().GetName())
*/
	return "", nil
}